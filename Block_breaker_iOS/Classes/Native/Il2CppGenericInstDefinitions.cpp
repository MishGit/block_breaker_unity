﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"




extern const Il2CppType RuntimeObject_0_0_0;
extern const Il2CppType Int32_t2803531614_0_0_0;
extern const Il2CppType Char_t1334203673_0_0_0;
extern const Il2CppType Int64_t338563217_0_0_0;
extern const Il2CppType UInt32_t413574147_0_0_0;
extern const Il2CppType UInt64_t921683320_0_0_0;
extern const Il2CppType Byte_t2822626181_0_0_0;
extern const Il2CppType SByte_t1604185109_0_0_0;
extern const Il2CppType Int16_t1178792569_0_0_0;
extern const Il2CppType UInt16_t2269804182_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IConvertible_t2316153507_0_0_0;
extern const Il2CppType IComparable_t353233170_0_0_0;
extern const Il2CppType IEnumerable_t863395423_0_0_0;
extern const Il2CppType ICloneable_t3398578658_0_0_0;
extern const Il2CppType IComparable_1_t2439511737_0_0_0;
extern const Il2CppType IEquatable_1_t1780045377_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IReflect_t4232730689_0_0_0;
extern const Il2CppType _Type_t2861529173_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t173947886_0_0_0;
extern const Il2CppType _MemberInfo_t1846860436_0_0_0;
extern const Il2CppType Double_t2276348901_0_0_0;
extern const Il2CppType Single_t473017394_0_0_0;
extern const Il2CppType Decimal_t1717466512_0_0_0;
extern const Il2CppType Boolean_t988759797_0_0_0;
extern const Il2CppType Delegate_t3172540527_0_0_0;
extern const Il2CppType ISerializable_t1943426027_0_0_0;
extern const Il2CppType ParameterInfo_t3977181738_0_0_0;
extern const Il2CppType _ParameterInfo_t4262047638_0_0_0;
extern const Il2CppType ParameterModifier_t2173190739_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType _FieldInfo_t619352020_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType _MethodInfo_t119590885_0_0_0;
extern const Il2CppType MethodBase_t3482457506_0_0_0;
extern const Il2CppType _MethodBase_t3230368487_0_0_0;
extern const Il2CppType ConstructorInfo_t1090520116_0_0_0;
extern const Il2CppType _ConstructorInfo_t2766353297_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType TableRange_t1615225767_0_0_0;
extern const Il2CppType TailoringInfo_t4005436652_0_0_0;
extern const Il2CppType KeyValuePair_2_t3632715362_0_0_0;
extern const Il2CppType Link_t3811480030_0_0_0;
extern const Il2CppType DictionaryEntry_t2929293879_0_0_0;
extern const Il2CppType KeyValuePair_2_t3357489110_0_0_0;
extern const Il2CppType Contraction_t691510939_0_0_0;
extern const Il2CppType Level2Map_t1316326550_0_0_0;
extern const Il2CppType BigInteger_t1041289570_0_0_0;
extern const Il2CppType KeySizes_t2289235637_0_0_0;
extern const Il2CppType KeyValuePair_2_t2818848908_0_0_0;
extern const Il2CppType Slot_t334802244_0_0_0;
extern const Il2CppType Slot_t3383151216_0_0_0;
extern const Il2CppType StackFrame_t1229074178_0_0_0;
extern const Il2CppType Calendar_t1129585511_0_0_0;
extern const Il2CppType ModuleBuilder_t3841773814_0_0_0;
extern const Il2CppType _ModuleBuilder_t1161624706_0_0_0;
extern const Il2CppType Module_t2180181500_0_0_0;
extern const Il2CppType _Module_t2704852488_0_0_0;
extern const Il2CppType ParameterBuilder_t3288171829_0_0_0;
extern const Il2CppType _ParameterBuilder_t134282708_0_0_0;
extern const Il2CppType TypeU5BU5D_t60035734_0_0_0;
extern const Il2CppType RuntimeArray_0_0_0;
extern const Il2CppType ICollection_t2097114642_0_0_0;
extern const Il2CppType IList_t2084693791_0_0_0;
extern const Il2CppType IList_1_t2599697879_0_0_0;
extern const Il2CppType ICollection_1_t1936994217_0_0_0;
extern const Il2CppType IEnumerable_1_t4060944939_0_0_0;
extern const Il2CppType IList_1_t1761661641_0_0_0;
extern const Il2CppType ICollection_1_t1098957979_0_0_0;
extern const Il2CppType IEnumerable_1_t3222908701_0_0_0;
extern const Il2CppType IList_1_t390460125_0_0_0;
extern const Il2CppType ICollection_1_t4022723759_0_0_0;
extern const Il2CppType IEnumerable_1_t1851707185_0_0_0;
extern const Il2CppType IList_1_t3242441985_0_0_0;
extern const Il2CppType ICollection_1_t2579738323_0_0_0;
extern const Il2CppType IEnumerable_1_t408721749_0_0_0;
extern const Il2CppType IList_1_t1997846134_0_0_0;
extern const Il2CppType ICollection_1_t1335142472_0_0_0;
extern const Il2CppType IEnumerable_1_t3459093194_0_0_0;
extern const Il2CppType IList_1_t3670758684_0_0_0;
extern const Il2CppType ICollection_1_t3008055022_0_0_0;
extern const Il2CppType IEnumerable_1_t837038448_0_0_0;
extern const Il2CppType IList_1_t3813563408_0_0_0;
extern const Il2CppType ICollection_1_t3150859746_0_0_0;
extern const Il2CppType IEnumerable_1_t979843172_0_0_0;
extern const Il2CppType ILTokenInfo_t1872913708_0_0_0;
extern const Il2CppType LabelData_t1543367224_0_0_0;
extern const Il2CppType LabelFixup_t340982400_0_0_0;
extern const Il2CppType GenericTypeParameterBuilder_t2722992877_0_0_0;
extern const Il2CppType TypeBuilder_t3421641907_0_0_0;
extern const Il2CppType _TypeBuilder_t2650947519_0_0_0;
extern const Il2CppType MethodBuilder_t473061604_0_0_0;
extern const Il2CppType _MethodBuilder_t1927917387_0_0_0;
extern const Il2CppType ConstructorBuilder_t2542337473_0_0_0;
extern const Il2CppType _ConstructorBuilder_t812717629_0_0_0;
extern const Il2CppType FieldBuilder_t1185537865_0_0_0;
extern const Il2CppType _FieldBuilder_t726804693_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType _PropertyInfo_t3021247535_0_0_0;
extern const Il2CppType CustomAttributeTypedArgument_t722066768_0_0_0;
extern const Il2CppType CustomAttributeNamedArgument_t2815983049_0_0_0;
extern const Il2CppType CustomAttributeData_t2504078644_0_0_0;
extern const Il2CppType ResourceInfo_t3425727170_0_0_0;
extern const Il2CppType ResourceCacheItem_t3158443302_0_0_0;
extern const Il2CppType IContextProperty_t3681610108_0_0_0;
extern const Il2CppType Header_t1031972396_0_0_0;
extern const Il2CppType ITrackingHandler_t3502059095_0_0_0;
extern const Il2CppType IContextAttribute_t1394016211_0_0_0;
extern const Il2CppType DateTime_t173522653_0_0_0;
extern const Il2CppType TimeSpan_t384326017_0_0_0;
extern const Il2CppType TypeTag_t3240722718_0_0_0;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType StrongName_t2336798789_0_0_0;
extern const Il2CppType IBuiltInEvidence_t101773638_0_0_0;
extern const Il2CppType IIdentityPermissionFactory_t2821289363_0_0_0;
extern const Il2CppType DateTimeOffset_t2358022680_0_0_0;
extern const Il2CppType Guid_t_0_0_0;
extern const Il2CppType Version_t618301286_0_0_0;
extern const Il2CppType KeyValuePair_2_t1817943545_0_0_0;
extern const Il2CppType KeyValuePair_2_t1542717293_0_0_0;
extern const Il2CppType X509Certificate_t3436509184_0_0_0;
extern const Il2CppType IDeserializationCallback_t306402524_0_0_0;
extern const Il2CppType X509ChainStatus_t703314169_0_0_0;
extern const Il2CppType Capture_t2938944230_0_0_0;
extern const Il2CppType Group_t2651207689_0_0_0;
extern const Il2CppType Mark_t3695721384_0_0_0;
extern const Il2CppType UriScheme_t2057891215_0_0_0;
extern const Il2CppType BigInteger_t1041289571_0_0_0;
extern const Il2CppType ByteU5BU5D_t4256625864_0_0_0;
extern const Il2CppType IList_1_t351557133_0_0_0;
extern const Il2CppType ICollection_1_t3983820767_0_0_0;
extern const Il2CppType IEnumerable_1_t1812804193_0_0_0;
extern const Il2CppType ClientCertificateType_t1318574781_0_0_0;
extern const Il2CppType Link_t2244904633_0_0_0;
extern const Il2CppType Object_t2399819029_0_0_0;
extern const Il2CppType Camera_t4214422582_0_0_0;
extern const Il2CppType Behaviour_t1869462422_0_0_0;
extern const Il2CppType Component_t1209726582_0_0_0;
extern const Il2CppType Display_t964176615_0_0_0;
extern const Il2CppType Keyframe_t1034491164_0_0_0;
extern const Il2CppType Vector3_t174917947_0_0_0;
extern const Il2CppType Vector4_t181764249_0_0_0;
extern const Il2CppType Vector2_t2867110760_0_0_0;
extern const Il2CppType Color32_t338341170_0_0_0;
extern const Il2CppType Playable_t882738958_0_0_0;
extern const Il2CppType PlayableOutput_t2283729907_0_0_0;
extern const Il2CppType Scene_t1653993680_0_0_0;
extern const Il2CppType LoadSceneMode_t3425002508_0_0_0;
extern const Il2CppType SpriteAtlas_t2865821040_0_0_0;
extern const Il2CppType Rigidbody2D_t1937802487_0_0_0;
extern const Il2CppType RaycastHit2D_t2595119295_0_0_0;
extern const Il2CppType ContactPoint2D_t4070334835_0_0_0;
extern const Il2CppType AudioClipPlayable_t2750948543_0_0_0;
extern const Il2CppType AudioMixerPlayable_t2521349572_0_0_0;
extern const Il2CppType AnimationClipPlayable_t2199148327_0_0_0;
extern const Il2CppType AnimationLayerMixerPlayable_t3950440205_0_0_0;
extern const Il2CppType AnimationMixerPlayable_t635383928_0_0_0;
extern const Il2CppType AnimationOffsetPlayable_t1025003618_0_0_0;
extern const Il2CppType AnimatorControllerPlayable_t421975501_0_0_0;
extern const Il2CppType AnimatorClipInfo_t506398021_0_0_0;
extern const Il2CppType AnimatorControllerParameter_t923037798_0_0_0;
extern const Il2CppType UIVertex_t2554352826_0_0_0;
extern const Il2CppType UICharInfo_t1978200295_0_0_0;
extern const Il2CppType UILineInfo_t202048947_0_0_0;
extern const Il2CppType Font_t3423958016_0_0_0;
extern const Il2CppType GUILayoutOption_t1026689508_0_0_0;
extern const Il2CppType GUILayoutEntry_t3218050312_0_0_0;
extern const Il2CppType LayoutCache_t3250357553_0_0_0;
extern const Il2CppType KeyValuePair_2_t1520347806_0_0_0;
extern const Il2CppType KeyValuePair_2_t2781040199_0_0_0;
extern const Il2CppType GUIStyle_t236231435_0_0_0;
extern const Il2CppType KeyValuePair_2_t790188931_0_0_0;
extern const Il2CppType Exception_t4239825407_0_0_0;
extern const Il2CppType UserProfile_t700863329_0_0_0;
extern const Il2CppType IUserProfile_t2763119316_0_0_0;
extern const Il2CppType AchievementDescription_t1494495164_0_0_0;
extern const Il2CppType IAchievementDescription_t452714252_0_0_0;
extern const Il2CppType GcLeaderboard_t436677311_0_0_0;
extern const Il2CppType IAchievementDescriptionU5BU5D_t2589288261_0_0_0;
extern const Il2CppType IAchievementU5BU5D_t2444041797_0_0_0;
extern const Il2CppType IAchievement_t190526476_0_0_0;
extern const Il2CppType GcAchievementData_t3124305070_0_0_0;
extern const Il2CppType Achievement_t1395733712_0_0_0;
extern const Il2CppType IScoreU5BU5D_t3632149934_0_0_0;
extern const Il2CppType IScore_t1344207895_0_0_0;
extern const Il2CppType GcScoreData_t104171333_0_0_0;
extern const Il2CppType Score_t939050586_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t1403542365_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t2116310945_0_0_0;
extern const Il2CppType Attribute_t3232323375_0_0_0;
extern const Il2CppType _Attribute_t3061038810_0_0_0;
extern const Il2CppType ExecuteInEditMode_t2112836633_0_0_0;
extern const Il2CppType RequireComponent_t709491642_0_0_0;
extern const Il2CppType HitInfo_t808200858_0_0_0;
extern const Il2CppType PersistentCall_t1665637431_0_0_0;
extern const Il2CppType BaseInvokableCall_t185741398_0_0_0;
extern const Il2CppType WorkRequest_t1285289635_0_0_0;
extern const Il2CppType PlayableBinding_t198519643_0_0_0;
extern const Il2CppType MessageTypeSubscribers_t2455387773_0_0_0;
extern const Il2CppType MessageEventArgs_t1676961500_0_0_0;
extern const Il2CppType WeakReference_t3337740345_0_0_0;
extern const Il2CppType KeyValuePair_2_t153286347_0_0_0;
extern const Il2CppType KeyValuePair_2_t1501361532_0_0_0;
extern const Il2CppType BaseInputModule_t3534238441_0_0_0;
extern const Il2CppType RaycastResult_t791495392_0_0_0;
extern const Il2CppType IDeselectHandler_t1821424147_0_0_0;
extern const Il2CppType IEventSystemHandler_t550700969_0_0_0;
extern const Il2CppType List_1_t620318833_0_0_0;
extern const Il2CppType List_1_t2059283024_0_0_0;
extern const Il2CppType List_1_t1279344446_0_0_0;
extern const Il2CppType ISelectHandler_t3646584355_0_0_0;
extern const Il2CppType BaseRaycaster_t2834558618_0_0_0;
extern const Il2CppType Entry_t1721113290_0_0_0;
extern const Il2CppType BaseEventData_t2239799759_0_0_0;
extern const Il2CppType IPointerEnterHandler_t3359589364_0_0_0;
extern const Il2CppType IPointerExitHandler_t1194585081_0_0_0;
extern const Il2CppType IPointerDownHandler_t1934540052_0_0_0;
extern const Il2CppType IPointerUpHandler_t1595114040_0_0_0;
extern const Il2CppType IPointerClickHandler_t3898132831_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t133502056_0_0_0;
extern const Il2CppType IBeginDragHandler_t3066996802_0_0_0;
extern const Il2CppType IDragHandler_t1430837312_0_0_0;
extern const Il2CppType IEndDragHandler_t1502010433_0_0_0;
extern const Il2CppType IDropHandler_t4289830363_0_0_0;
extern const Il2CppType IScrollHandler_t2947048734_0_0_0;
extern const Il2CppType IUpdateSelectedHandler_t2533267811_0_0_0;
extern const Il2CppType IMoveHandler_t2306254927_0_0_0;
extern const Il2CppType ISubmitHandler_t1593443308_0_0_0;
extern const Il2CppType ICancelHandler_t1896074317_0_0_0;
extern const Il2CppType Transform_t2451218446_0_0_0;
extern const Il2CppType GameObject_t2629728357_0_0_0;
extern const Il2CppType BaseInput_t718558484_0_0_0;
extern const Il2CppType UIBehaviour_t4190033142_0_0_0;
extern const Il2CppType MonoBehaviour_t3163037868_0_0_0;
extern const Il2CppType PointerEventData_t2898711789_0_0_0;
extern const Il2CppType KeyValuePair_2_t2429394435_0_0_0;
extern const Il2CppType ButtonState_t4280795307_0_0_0;
extern const Il2CppType RaycastHit_t3326614521_0_0_0;
extern const Il2CppType Color_t1894673040_0_0_0;
extern const Il2CppType ICanvasElement_t3347193002_0_0_0;
extern const Il2CppType ColorBlock_t3790099472_0_0_0;
extern const Il2CppType OptionData_t309155721_0_0_0;
extern const Il2CppType DropdownItem_t1339148254_0_0_0;
extern const Il2CppType FloatTween_t820617968_0_0_0;
extern const Il2CppType Sprite_t3431485579_0_0_0;
extern const Il2CppType Canvas_t2785269944_0_0_0;
extern const Il2CppType List_1_t2854887808_0_0_0;
extern const Il2CppType HashSet_1_t1006781235_0_0_0;
extern const Il2CppType Text_t4164675009_0_0_0;
extern const Il2CppType KeyValuePair_2_t800773855_0_0_0;
extern const Il2CppType ColorTween_t3542451946_0_0_0;
extern const Il2CppType Graphic_t3593681171_0_0_0;
extern const Il2CppType IndexedSet_1_t3015234795_0_0_0;
extern const Il2CppType KeyValuePair_2_t1842217471_0_0_0;
extern const Il2CppType KeyValuePair_2_t2710483019_0_0_0;
extern const Il2CppType KeyValuePair_2_t3246846072_0_0_0;
extern const Il2CppType Type_t2268913483_0_0_0;
extern const Il2CppType FillMethod_t1193514586_0_0_0;
extern const Il2CppType ContentType_t2862244902_0_0_0;
extern const Il2CppType LineType_t2260719266_0_0_0;
extern const Il2CppType InputType_t2301555234_0_0_0;
extern const Il2CppType TouchScreenKeyboardType_t3631231902_0_0_0;
extern const Il2CppType CharacterValidation_t1479726893_0_0_0;
extern const Il2CppType Mask_t1584318481_0_0_0;
extern const Il2CppType List_1_t1653936345_0_0_0;
extern const Il2CppType RectMask2D_t3187556863_0_0_0;
extern const Il2CppType List_1_t3257174727_0_0_0;
extern const Il2CppType Navigation_t643916199_0_0_0;
extern const Il2CppType IClippable_t3710278876_0_0_0;
extern const Il2CppType Direction_t1518645464_0_0_0;
extern const Il2CppType Selectable_t3045992923_0_0_0;
extern const Il2CppType Transition_t2847464031_0_0_0;
extern const Il2CppType SpriteState_t582609410_0_0_0;
extern const Il2CppType CanvasGroup_t3466240198_0_0_0;
extern const Il2CppType Direction_t1638109238_0_0_0;
extern const Il2CppType MatEntry_t767401855_0_0_0;
extern const Il2CppType Toggle_t510632154_0_0_0;
extern const Il2CppType IClipper_t1019235875_0_0_0;
extern const Il2CppType KeyValuePair_2_t4113814779_0_0_0;
extern const Il2CppType AspectMode_t2018207616_0_0_0;
extern const Il2CppType FitMode_t1416694921_0_0_0;
extern const Il2CppType RectTransform_t3890948561_0_0_0;
extern const Il2CppType LayoutRebuilder_t9522069_0_0_0;
extern const Il2CppType ILayoutElement_t2893856145_0_0_0;
extern const Il2CppType List_1_t244535811_0_0_0;
extern const Il2CppType List_1_t407959034_0_0_0;
extern const Il2CppType List_1_t2936728624_0_0_0;
extern const Il2CppType List_1_t251382113_0_0_0;
extern const Il2CppType List_1_t2873149478_0_0_0;
extern const Il2CppType List_1_t2623970690_0_0_0;
extern const Il2CppType IEnumerable_1_t2416028975_gp_0_0_0_0;
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m1287759456_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2282796150_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3892888058_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3892888058_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m551987201_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1923995114_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1923995114_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m2177995984_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1851492001_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1851492001_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m444933298_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2428568558_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2428568558_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m211883471_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2013146379_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m425782624_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m425782624_gp_1_0_0_0;
extern const Il2CppType Array_compare_m1097872614_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m2192871689_gp_0_0_0_0;
extern const Il2CppType Array_Resize_m3427076075_gp_0_0_0_0;
extern const Il2CppType Array_TrueForAll_m833996309_gp_0_0_0_0;
extern const Il2CppType Array_ForEach_m3624317836_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m941549552_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m941549552_gp_1_0_0_0;
extern const Il2CppType Array_FindLastIndex_m1134336487_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m3644051563_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m2327043970_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m2887368280_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m3008983722_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m464982704_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m1764795623_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m890810051_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m2999506427_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m612995514_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m127944267_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m4159481738_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m1333370475_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m2770575792_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m2027197727_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m696292085_gp_0_0_0_0;
extern const Il2CppType Array_FindAll_m3649758798_gp_0_0_0_0;
extern const Il2CppType Array_Exists_m3309978611_gp_0_0_0_0;
extern const Il2CppType Array_AsReadOnly_m2407970135_gp_0_0_0_0;
extern const Il2CppType Array_Find_m15451854_gp_0_0_0_0;
extern const Il2CppType Array_FindLast_m2951830467_gp_0_0_0_0;
extern const Il2CppType InternalEnumerator_1_t4167398077_gp_0_0_0_0;
extern const Il2CppType ArrayReadOnlyList_1_t3192552300_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t3188906990_gp_0_0_0_0;
extern const Il2CppType IList_1_t74343611_gp_0_0_0_0;
extern const Il2CppType ICollection_1_t1964006621_gp_0_0_0_0;
extern const Il2CppType Nullable_1_t2192590836_gp_0_0_0_0;
extern const Il2CppType Comparer_1_t117739795_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t2087338667_gp_0_0_0_0;
extern const Il2CppType GenericComparer_1_t295359490_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t3672574211_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t3672574211_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t3732153717_0_0_0;
extern const Il2CppType Dictionary_2_Do_CopyTo_m1998764678_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t1214271919_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t1214271919_gp_1_0_0_0;
extern const Il2CppType Enumerator_t1564904491_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1564904491_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t3466079189_0_0_0;
extern const Il2CppType ValueCollection_t3865265530_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t3865265530_gp_1_0_0_0;
extern const Il2CppType Enumerator_t2660683481_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2660683481_gp_1_0_0_0;
extern const Il2CppType EqualityComparer_1_t1389874609_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t1445765274_gp_0_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t2215272400_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t171571701_0_0_0;
extern const Il2CppType IDictionary_2_t3746618595_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t3746618595_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t3524528471_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t3524528471_gp_1_0_0_0;
extern const Il2CppType List_1_t2110594299_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1180177267_gp_0_0_0_0;
extern const Il2CppType Collection_1_t2075259255_gp_0_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t4123029339_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1924011743_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1924011743_gp_1_0_0_0;
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1418720114_gp_0_0_0_0;
extern const Il2CppType Queue_1_t3547065007_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3106742487_gp_0_0_0_0;
extern const Il2CppType Stack_1_t452188541_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1170309493_gp_0_0_0_0;
extern const Il2CppType HashSet_1_t772885429_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2406876998_gp_0_0_0_0;
extern const Il2CppType PrimeHelper_t3750465633_gp_0_0_0_0;
extern const Il2CppType Enumerable_Any_m41080623_gp_0_0_0_0;
extern const Il2CppType Enumerable_Where_m2792816853_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentInChildren_m3597157616_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m3020516785_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m1275101576_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m328034652_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m1749718494_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m3436671515_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m11573404_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m2073010651_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m2703880170_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m2817529289_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentInChildren_m130752911_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponents_m1760858099_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInChildren_m560504938_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInParent_m1658428519_gp_0_0_0_0;
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m2206468965_gp_0_0_0_0;
extern const Il2CppType Mesh_SafeLength_m4141435376_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m523604312_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m1158588740_gp_0_0_0_0;
extern const Il2CppType Mesh_SetUvsImpl_m157937555_gp_0_0_0_0;
extern const Il2CppType Object_Instantiate_m2704180360_gp_0_0_0_0;
extern const Il2CppType Object_FindObjectsOfType_m21843941_gp_0_0_0_0;
extern const Il2CppType Playable_IsPlayableOfType_m1014032790_gp_0_0_0_0;
extern const Il2CppType PlayableOutput_IsPlayableOutputOfType_m2401824904_gp_0_0_0_0;
extern const Il2CppType InvokableCall_1_t608438805_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t1170212876_0_0_0;
extern const Il2CppType InvokableCall_2_t3393839338_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3393839338_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3121392964_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3121392964_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3121392964_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t320322180_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t320322180_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t320322180_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t320322180_gp_3_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t2373840750_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t3654565707_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4148778253_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4148778253_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t2171759262_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t2171759262_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t2171759262_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t401569282_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t401569282_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t401569282_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t401569282_gp_3_0_0_0;
extern const Il2CppType ExecuteEvents_Execute_m1461001727_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m3588049849_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventList_m1070087876_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_CanHandleEvent_m2083532058_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventHandler_m472693225_gp_0_0_0_0;
extern const Il2CppType TweenRunner_1_t1959029912_gp_0_0_0_0;
extern const Il2CppType Dropdown_GetOrAddComponent_m2960389033_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m71196877_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t767473291_gp_0_0_0_0;
extern const Il2CppType ListPool_1_t306790258_gp_0_0_0_0;
extern const Il2CppType List_1_t1024040571_0_0_0;
extern const Il2CppType ObjectPool_1_t712677698_gp_0_0_0_0;
extern const Il2CppType AnimationPlayableOutput_t3060749678_0_0_0;
extern const Il2CppType DefaultExecutionOrder_t585808243_0_0_0;
extern const Il2CppType AudioPlayableOutput_t1627733019_0_0_0;
extern const Il2CppType PlayerConnection_t944997452_0_0_0;
extern const Il2CppType ScriptPlayableOutput_t4185099258_0_0_0;
extern const Il2CppType GUILayer_t3764231201_0_0_0;
extern const Il2CppType EventSystem_t2634658781_0_0_0;
extern const Il2CppType AxisEventData_t1013140673_0_0_0;
extern const Il2CppType SpriteRenderer_t3490489848_0_0_0;
extern const Il2CppType Image_t1612257916_0_0_0;
extern const Il2CppType Button_t1637722950_0_0_0;
extern const Il2CppType RawImage_t4039118384_0_0_0;
extern const Il2CppType Slider_t42002792_0_0_0;
extern const Il2CppType Scrollbar_t3263882543_0_0_0;
extern const Il2CppType InputField_t621374856_0_0_0;
extern const Il2CppType ScrollRect_t2248909708_0_0_0;
extern const Il2CppType Dropdown_t2100269451_0_0_0;
extern const Il2CppType GraphicRaycaster_t3508845319_0_0_0;
extern const Il2CppType CanvasRenderer_t1499400789_0_0_0;
extern const Il2CppType Corner_t141238654_0_0_0;
extern const Il2CppType Axis_t2562381249_0_0_0;
extern const Il2CppType Constraint_t508081870_0_0_0;
extern const Il2CppType SubmitEvent_t527990981_0_0_0;
extern const Il2CppType OnChangeEvent_t3634356784_0_0_0;
extern const Il2CppType OnValidateInput_t3553388536_0_0_0;
extern const Il2CppType LayoutElement_t1499649617_0_0_0;
extern const Il2CppType RectOffset_t1388178548_0_0_0;
extern const Il2CppType TextAnchor_t54660852_0_0_0;
extern const Il2CppType AnimationTriggers_t1051497479_0_0_0;
extern const Il2CppType Animator_t2124055305_0_0_0;
extern const Il2CppType Paddle_t4050334188_0_0_0;
extern const Il2CppType LevelManager_t3966866889_0_0_0;
extern const Il2CppType Ball_t3103655791_0_0_0;




static const RuntimeType* GenInst_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0 = { 1, GenInst_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0 = { 1, GenInst_Int32_t2803531614_0_0_0_Types };
static const RuntimeType* GenInst_Char_t1334203673_0_0_0_Types[] = { (&Char_t1334203673_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t1334203673_0_0_0 = { 1, GenInst_Char_t1334203673_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t338563217_0_0_0_Types[] = { (&Int64_t338563217_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t338563217_0_0_0 = { 1, GenInst_Int64_t338563217_0_0_0_Types };
static const RuntimeType* GenInst_UInt32_t413574147_0_0_0_Types[] = { (&UInt32_t413574147_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt32_t413574147_0_0_0 = { 1, GenInst_UInt32_t413574147_0_0_0_Types };
static const RuntimeType* GenInst_UInt64_t921683320_0_0_0_Types[] = { (&UInt64_t921683320_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt64_t921683320_0_0_0 = { 1, GenInst_UInt64_t921683320_0_0_0_Types };
static const RuntimeType* GenInst_Byte_t2822626181_0_0_0_Types[] = { (&Byte_t2822626181_0_0_0) };
extern const Il2CppGenericInst GenInst_Byte_t2822626181_0_0_0 = { 1, GenInst_Byte_t2822626181_0_0_0_Types };
static const RuntimeType* GenInst_SByte_t1604185109_0_0_0_Types[] = { (&SByte_t1604185109_0_0_0) };
extern const Il2CppGenericInst GenInst_SByte_t1604185109_0_0_0 = { 1, GenInst_SByte_t1604185109_0_0_0_Types };
static const RuntimeType* GenInst_Int16_t1178792569_0_0_0_Types[] = { (&Int16_t1178792569_0_0_0) };
extern const Il2CppGenericInst GenInst_Int16_t1178792569_0_0_0 = { 1, GenInst_Int16_t1178792569_0_0_0_Types };
static const RuntimeType* GenInst_UInt16_t2269804182_0_0_0_Types[] = { (&UInt16_t2269804182_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt16_t2269804182_0_0_0 = { 1, GenInst_UInt16_t2269804182_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Types[] = { (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
static const RuntimeType* GenInst_IConvertible_t2316153507_0_0_0_Types[] = { (&IConvertible_t2316153507_0_0_0) };
extern const Il2CppGenericInst GenInst_IConvertible_t2316153507_0_0_0 = { 1, GenInst_IConvertible_t2316153507_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_t353233170_0_0_0_Types[] = { (&IComparable_t353233170_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_t353233170_0_0_0 = { 1, GenInst_IComparable_t353233170_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_t863395423_0_0_0_Types[] = { (&IEnumerable_t863395423_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_t863395423_0_0_0 = { 1, GenInst_IEnumerable_t863395423_0_0_0_Types };
static const RuntimeType* GenInst_ICloneable_t3398578658_0_0_0_Types[] = { (&ICloneable_t3398578658_0_0_0) };
extern const Il2CppGenericInst GenInst_ICloneable_t3398578658_0_0_0 = { 1, GenInst_ICloneable_t3398578658_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_1_t2439511737_0_0_0_Types[] = { (&IComparable_1_t2439511737_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_1_t2439511737_0_0_0 = { 1, GenInst_IComparable_1_t2439511737_0_0_0_Types };
static const RuntimeType* GenInst_IEquatable_1_t1780045377_0_0_0_Types[] = { (&IEquatable_1_t1780045377_0_0_0) };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1780045377_0_0_0 = { 1, GenInst_IEquatable_1_t1780045377_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Types[] = { (&Type_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
static const RuntimeType* GenInst_IReflect_t4232730689_0_0_0_Types[] = { (&IReflect_t4232730689_0_0_0) };
extern const Il2CppGenericInst GenInst_IReflect_t4232730689_0_0_0 = { 1, GenInst_IReflect_t4232730689_0_0_0_Types };
static const RuntimeType* GenInst__Type_t2861529173_0_0_0_Types[] = { (&_Type_t2861529173_0_0_0) };
extern const Il2CppGenericInst GenInst__Type_t2861529173_0_0_0 = { 1, GenInst__Type_t2861529173_0_0_0_Types };
static const RuntimeType* GenInst_MemberInfo_t_0_0_0_Types[] = { (&MemberInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
static const RuntimeType* GenInst_ICustomAttributeProvider_t173947886_0_0_0_Types[] = { (&ICustomAttributeProvider_t173947886_0_0_0) };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t173947886_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t173947886_0_0_0_Types };
static const RuntimeType* GenInst__MemberInfo_t1846860436_0_0_0_Types[] = { (&_MemberInfo_t1846860436_0_0_0) };
extern const Il2CppGenericInst GenInst__MemberInfo_t1846860436_0_0_0 = { 1, GenInst__MemberInfo_t1846860436_0_0_0_Types };
static const RuntimeType* GenInst_Double_t2276348901_0_0_0_Types[] = { (&Double_t2276348901_0_0_0) };
extern const Il2CppGenericInst GenInst_Double_t2276348901_0_0_0 = { 1, GenInst_Double_t2276348901_0_0_0_Types };
static const RuntimeType* GenInst_Single_t473017394_0_0_0_Types[] = { (&Single_t473017394_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t473017394_0_0_0 = { 1, GenInst_Single_t473017394_0_0_0_Types };
static const RuntimeType* GenInst_Decimal_t1717466512_0_0_0_Types[] = { (&Decimal_t1717466512_0_0_0) };
extern const Il2CppGenericInst GenInst_Decimal_t1717466512_0_0_0 = { 1, GenInst_Decimal_t1717466512_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t988759797_0_0_0_Types[] = { (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t988759797_0_0_0 = { 1, GenInst_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_Delegate_t3172540527_0_0_0_Types[] = { (&Delegate_t3172540527_0_0_0) };
extern const Il2CppGenericInst GenInst_Delegate_t3172540527_0_0_0 = { 1, GenInst_Delegate_t3172540527_0_0_0_Types };
static const RuntimeType* GenInst_ISerializable_t1943426027_0_0_0_Types[] = { (&ISerializable_t1943426027_0_0_0) };
extern const Il2CppGenericInst GenInst_ISerializable_t1943426027_0_0_0 = { 1, GenInst_ISerializable_t1943426027_0_0_0_Types };
static const RuntimeType* GenInst_ParameterInfo_t3977181738_0_0_0_Types[] = { (&ParameterInfo_t3977181738_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterInfo_t3977181738_0_0_0 = { 1, GenInst_ParameterInfo_t3977181738_0_0_0_Types };
static const RuntimeType* GenInst__ParameterInfo_t4262047638_0_0_0_Types[] = { (&_ParameterInfo_t4262047638_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterInfo_t4262047638_0_0_0 = { 1, GenInst__ParameterInfo_t4262047638_0_0_0_Types };
static const RuntimeType* GenInst_ParameterModifier_t2173190739_0_0_0_Types[] = { (&ParameterModifier_t2173190739_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterModifier_t2173190739_0_0_0 = { 1, GenInst_ParameterModifier_t2173190739_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_FieldInfo_t_0_0_0_Types[] = { (&FieldInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__FieldInfo_t619352020_0_0_0_Types[] = { (&_FieldInfo_t619352020_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldInfo_t619352020_0_0_0 = { 1, GenInst__FieldInfo_t619352020_0_0_0_Types };
static const RuntimeType* GenInst_MethodInfo_t_0_0_0_Types[] = { (&MethodInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__MethodInfo_t119590885_0_0_0_Types[] = { (&_MethodInfo_t119590885_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodInfo_t119590885_0_0_0 = { 1, GenInst__MethodInfo_t119590885_0_0_0_Types };
static const RuntimeType* GenInst_MethodBase_t3482457506_0_0_0_Types[] = { (&MethodBase_t3482457506_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBase_t3482457506_0_0_0 = { 1, GenInst_MethodBase_t3482457506_0_0_0_Types };
static const RuntimeType* GenInst__MethodBase_t3230368487_0_0_0_Types[] = { (&_MethodBase_t3230368487_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBase_t3230368487_0_0_0 = { 1, GenInst__MethodBase_t3230368487_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorInfo_t1090520116_0_0_0_Types[] = { (&ConstructorInfo_t1090520116_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t1090520116_0_0_0 = { 1, GenInst_ConstructorInfo_t1090520116_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorInfo_t2766353297_0_0_0_Types[] = { (&_ConstructorInfo_t2766353297_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t2766353297_0_0_0 = { 1, GenInst__ConstructorInfo_t2766353297_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_TableRange_t1615225767_0_0_0_Types[] = { (&TableRange_t1615225767_0_0_0) };
extern const Il2CppGenericInst GenInst_TableRange_t1615225767_0_0_0 = { 1, GenInst_TableRange_t1615225767_0_0_0_Types };
static const RuntimeType* GenInst_TailoringInfo_t4005436652_0_0_0_Types[] = { (&TailoringInfo_t4005436652_0_0_0) };
extern const Il2CppGenericInst GenInst_TailoringInfo_t4005436652_0_0_0 = { 1, GenInst_TailoringInfo_t4005436652_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2803531614_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2803531614_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3632715362_0_0_0_Types[] = { (&KeyValuePair_2_t3632715362_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3632715362_0_0_0 = { 1, GenInst_KeyValuePair_2_t3632715362_0_0_0_Types };
static const RuntimeType* GenInst_Link_t3811480030_0_0_0_Types[] = { (&Link_t3811480030_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t3811480030_0_0_0 = { 1, GenInst_Link_t3811480030_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_Int32_t2803531614_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2803531614_0_0_0), (&Int32_t2803531614_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_Int32_t2803531614_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_Int32_t2803531614_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2803531614_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t2929293879_0_0_0 = { 1, GenInst_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3632715362_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2803531614_0_0_0), (&KeyValuePair_2_t3632715362_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3632715362_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3632715362_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2803531614_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3357489110_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2803531614_0_0_0), (&KeyValuePair_2_t3357489110_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3357489110_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3357489110_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3357489110_0_0_0_Types[] = { (&KeyValuePair_2_t3357489110_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3357489110_0_0_0 = { 1, GenInst_KeyValuePair_2_t3357489110_0_0_0_Types };
static const RuntimeType* GenInst_Contraction_t691510939_0_0_0_Types[] = { (&Contraction_t691510939_0_0_0) };
extern const Il2CppGenericInst GenInst_Contraction_t691510939_0_0_0 = { 1, GenInst_Contraction_t691510939_0_0_0_Types };
static const RuntimeType* GenInst_Level2Map_t1316326550_0_0_0_Types[] = { (&Level2Map_t1316326550_0_0_0) };
extern const Il2CppGenericInst GenInst_Level2Map_t1316326550_0_0_0 = { 1, GenInst_Level2Map_t1316326550_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t1041289570_0_0_0_Types[] = { (&BigInteger_t1041289570_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t1041289570_0_0_0 = { 1, GenInst_BigInteger_t1041289570_0_0_0_Types };
static const RuntimeType* GenInst_KeySizes_t2289235637_0_0_0_Types[] = { (&KeySizes_t2289235637_0_0_0) };
extern const Il2CppGenericInst GenInst_KeySizes_t2289235637_0_0_0 = { 1, GenInst_KeySizes_t2289235637_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2818848908_0_0_0_Types[] = { (&KeyValuePair_2_t2818848908_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2818848908_0_0_0 = { 1, GenInst_KeyValuePair_2_t2818848908_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2818848908_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2818848908_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2818848908_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2818848908_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t334802244_0_0_0_Types[] = { (&Slot_t334802244_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t334802244_0_0_0 = { 1, GenInst_Slot_t334802244_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t3383151216_0_0_0_Types[] = { (&Slot_t3383151216_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t3383151216_0_0_0 = { 1, GenInst_Slot_t3383151216_0_0_0_Types };
static const RuntimeType* GenInst_StackFrame_t1229074178_0_0_0_Types[] = { (&StackFrame_t1229074178_0_0_0) };
extern const Il2CppGenericInst GenInst_StackFrame_t1229074178_0_0_0 = { 1, GenInst_StackFrame_t1229074178_0_0_0_Types };
static const RuntimeType* GenInst_Calendar_t1129585511_0_0_0_Types[] = { (&Calendar_t1129585511_0_0_0) };
extern const Il2CppGenericInst GenInst_Calendar_t1129585511_0_0_0 = { 1, GenInst_Calendar_t1129585511_0_0_0_Types };
static const RuntimeType* GenInst_ModuleBuilder_t3841773814_0_0_0_Types[] = { (&ModuleBuilder_t3841773814_0_0_0) };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t3841773814_0_0_0 = { 1, GenInst_ModuleBuilder_t3841773814_0_0_0_Types };
static const RuntimeType* GenInst__ModuleBuilder_t1161624706_0_0_0_Types[] = { (&_ModuleBuilder_t1161624706_0_0_0) };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1161624706_0_0_0 = { 1, GenInst__ModuleBuilder_t1161624706_0_0_0_Types };
static const RuntimeType* GenInst_Module_t2180181500_0_0_0_Types[] = { (&Module_t2180181500_0_0_0) };
extern const Il2CppGenericInst GenInst_Module_t2180181500_0_0_0 = { 1, GenInst_Module_t2180181500_0_0_0_Types };
static const RuntimeType* GenInst__Module_t2704852488_0_0_0_Types[] = { (&_Module_t2704852488_0_0_0) };
extern const Il2CppGenericInst GenInst__Module_t2704852488_0_0_0 = { 1, GenInst__Module_t2704852488_0_0_0_Types };
static const RuntimeType* GenInst_ParameterBuilder_t3288171829_0_0_0_Types[] = { (&ParameterBuilder_t3288171829_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3288171829_0_0_0 = { 1, GenInst_ParameterBuilder_t3288171829_0_0_0_Types };
static const RuntimeType* GenInst__ParameterBuilder_t134282708_0_0_0_Types[] = { (&_ParameterBuilder_t134282708_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t134282708_0_0_0 = { 1, GenInst__ParameterBuilder_t134282708_0_0_0_Types };
static const RuntimeType* GenInst_TypeU5BU5D_t60035734_0_0_0_Types[] = { (&TypeU5BU5D_t60035734_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t60035734_0_0_0 = { 1, GenInst_TypeU5BU5D_t60035734_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeArray_0_0_0_Types[] = { (&RuntimeArray_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeArray_0_0_0 = { 1, GenInst_RuntimeArray_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_t2097114642_0_0_0_Types[] = { (&ICollection_t2097114642_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_t2097114642_0_0_0 = { 1, GenInst_ICollection_t2097114642_0_0_0_Types };
static const RuntimeType* GenInst_IList_t2084693791_0_0_0_Types[] = { (&IList_t2084693791_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_t2084693791_0_0_0 = { 1, GenInst_IList_t2084693791_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2599697879_0_0_0_Types[] = { (&IList_1_t2599697879_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2599697879_0_0_0 = { 1, GenInst_IList_1_t2599697879_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1936994217_0_0_0_Types[] = { (&ICollection_1_t1936994217_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1936994217_0_0_0 = { 1, GenInst_ICollection_1_t1936994217_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t4060944939_0_0_0_Types[] = { (&IEnumerable_1_t4060944939_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4060944939_0_0_0 = { 1, GenInst_IEnumerable_1_t4060944939_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1761661641_0_0_0_Types[] = { (&IList_1_t1761661641_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1761661641_0_0_0 = { 1, GenInst_IList_1_t1761661641_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1098957979_0_0_0_Types[] = { (&ICollection_1_t1098957979_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1098957979_0_0_0 = { 1, GenInst_ICollection_1_t1098957979_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3222908701_0_0_0_Types[] = { (&IEnumerable_1_t3222908701_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3222908701_0_0_0 = { 1, GenInst_IEnumerable_1_t3222908701_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t390460125_0_0_0_Types[] = { (&IList_1_t390460125_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t390460125_0_0_0 = { 1, GenInst_IList_1_t390460125_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t4022723759_0_0_0_Types[] = { (&ICollection_1_t4022723759_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t4022723759_0_0_0 = { 1, GenInst_ICollection_1_t4022723759_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1851707185_0_0_0_Types[] = { (&IEnumerable_1_t1851707185_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1851707185_0_0_0 = { 1, GenInst_IEnumerable_1_t1851707185_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3242441985_0_0_0_Types[] = { (&IList_1_t3242441985_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3242441985_0_0_0 = { 1, GenInst_IList_1_t3242441985_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2579738323_0_0_0_Types[] = { (&ICollection_1_t2579738323_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2579738323_0_0_0 = { 1, GenInst_ICollection_1_t2579738323_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t408721749_0_0_0_Types[] = { (&IEnumerable_1_t408721749_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t408721749_0_0_0 = { 1, GenInst_IEnumerable_1_t408721749_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1997846134_0_0_0_Types[] = { (&IList_1_t1997846134_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1997846134_0_0_0 = { 1, GenInst_IList_1_t1997846134_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1335142472_0_0_0_Types[] = { (&ICollection_1_t1335142472_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1335142472_0_0_0 = { 1, GenInst_ICollection_1_t1335142472_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3459093194_0_0_0_Types[] = { (&IEnumerable_1_t3459093194_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3459093194_0_0_0 = { 1, GenInst_IEnumerable_1_t3459093194_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3670758684_0_0_0_Types[] = { (&IList_1_t3670758684_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3670758684_0_0_0 = { 1, GenInst_IList_1_t3670758684_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3008055022_0_0_0_Types[] = { (&ICollection_1_t3008055022_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3008055022_0_0_0 = { 1, GenInst_ICollection_1_t3008055022_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t837038448_0_0_0_Types[] = { (&IEnumerable_1_t837038448_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t837038448_0_0_0 = { 1, GenInst_IEnumerable_1_t837038448_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3813563408_0_0_0_Types[] = { (&IList_1_t3813563408_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3813563408_0_0_0 = { 1, GenInst_IList_1_t3813563408_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3150859746_0_0_0_Types[] = { (&ICollection_1_t3150859746_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3150859746_0_0_0 = { 1, GenInst_ICollection_1_t3150859746_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t979843172_0_0_0_Types[] = { (&IEnumerable_1_t979843172_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t979843172_0_0_0 = { 1, GenInst_IEnumerable_1_t979843172_0_0_0_Types };
static const RuntimeType* GenInst_ILTokenInfo_t1872913708_0_0_0_Types[] = { (&ILTokenInfo_t1872913708_0_0_0) };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1872913708_0_0_0 = { 1, GenInst_ILTokenInfo_t1872913708_0_0_0_Types };
static const RuntimeType* GenInst_LabelData_t1543367224_0_0_0_Types[] = { (&LabelData_t1543367224_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelData_t1543367224_0_0_0 = { 1, GenInst_LabelData_t1543367224_0_0_0_Types };
static const RuntimeType* GenInst_LabelFixup_t340982400_0_0_0_Types[] = { (&LabelFixup_t340982400_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelFixup_t340982400_0_0_0 = { 1, GenInst_LabelFixup_t340982400_0_0_0_Types };
static const RuntimeType* GenInst_GenericTypeParameterBuilder_t2722992877_0_0_0_Types[] = { (&GenericTypeParameterBuilder_t2722992877_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t2722992877_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t2722992877_0_0_0_Types };
static const RuntimeType* GenInst_TypeBuilder_t3421641907_0_0_0_Types[] = { (&TypeBuilder_t3421641907_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3421641907_0_0_0 = { 1, GenInst_TypeBuilder_t3421641907_0_0_0_Types };
static const RuntimeType* GenInst__TypeBuilder_t2650947519_0_0_0_Types[] = { (&_TypeBuilder_t2650947519_0_0_0) };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2650947519_0_0_0 = { 1, GenInst__TypeBuilder_t2650947519_0_0_0_Types };
static const RuntimeType* GenInst_MethodBuilder_t473061604_0_0_0_Types[] = { (&MethodBuilder_t473061604_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBuilder_t473061604_0_0_0 = { 1, GenInst_MethodBuilder_t473061604_0_0_0_Types };
static const RuntimeType* GenInst__MethodBuilder_t1927917387_0_0_0_Types[] = { (&_MethodBuilder_t1927917387_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBuilder_t1927917387_0_0_0 = { 1, GenInst__MethodBuilder_t1927917387_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorBuilder_t2542337473_0_0_0_Types[] = { (&ConstructorBuilder_t2542337473_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t2542337473_0_0_0 = { 1, GenInst_ConstructorBuilder_t2542337473_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorBuilder_t812717629_0_0_0_Types[] = { (&_ConstructorBuilder_t812717629_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t812717629_0_0_0 = { 1, GenInst__ConstructorBuilder_t812717629_0_0_0_Types };
static const RuntimeType* GenInst_FieldBuilder_t1185537865_0_0_0_Types[] = { (&FieldBuilder_t1185537865_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1185537865_0_0_0 = { 1, GenInst_FieldBuilder_t1185537865_0_0_0_Types };
static const RuntimeType* GenInst__FieldBuilder_t726804693_0_0_0_Types[] = { (&_FieldBuilder_t726804693_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldBuilder_t726804693_0_0_0 = { 1, GenInst__FieldBuilder_t726804693_0_0_0_Types };
static const RuntimeType* GenInst_PropertyInfo_t_0_0_0_Types[] = { (&PropertyInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__PropertyInfo_t3021247535_0_0_0_Types[] = { (&_PropertyInfo_t3021247535_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyInfo_t3021247535_0_0_0 = { 1, GenInst__PropertyInfo_t3021247535_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t722066768_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t722066768_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t722066768_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t722066768_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t2815983049_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t2815983049_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t2815983049_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t2815983049_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeData_t2504078644_0_0_0_Types[] = { (&CustomAttributeData_t2504078644_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t2504078644_0_0_0 = { 1, GenInst_CustomAttributeData_t2504078644_0_0_0_Types };
static const RuntimeType* GenInst_ResourceInfo_t3425727170_0_0_0_Types[] = { (&ResourceInfo_t3425727170_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3425727170_0_0_0 = { 1, GenInst_ResourceInfo_t3425727170_0_0_0_Types };
static const RuntimeType* GenInst_ResourceCacheItem_t3158443302_0_0_0_Types[] = { (&ResourceCacheItem_t3158443302_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t3158443302_0_0_0 = { 1, GenInst_ResourceCacheItem_t3158443302_0_0_0_Types };
static const RuntimeType* GenInst_IContextProperty_t3681610108_0_0_0_Types[] = { (&IContextProperty_t3681610108_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextProperty_t3681610108_0_0_0 = { 1, GenInst_IContextProperty_t3681610108_0_0_0_Types };
static const RuntimeType* GenInst_Header_t1031972396_0_0_0_Types[] = { (&Header_t1031972396_0_0_0) };
extern const Il2CppGenericInst GenInst_Header_t1031972396_0_0_0 = { 1, GenInst_Header_t1031972396_0_0_0_Types };
static const RuntimeType* GenInst_ITrackingHandler_t3502059095_0_0_0_Types[] = { (&ITrackingHandler_t3502059095_0_0_0) };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t3502059095_0_0_0 = { 1, GenInst_ITrackingHandler_t3502059095_0_0_0_Types };
static const RuntimeType* GenInst_IContextAttribute_t1394016211_0_0_0_Types[] = { (&IContextAttribute_t1394016211_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextAttribute_t1394016211_0_0_0 = { 1, GenInst_IContextAttribute_t1394016211_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t173522653_0_0_0_Types[] = { (&DateTime_t173522653_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t173522653_0_0_0 = { 1, GenInst_DateTime_t173522653_0_0_0_Types };
static const RuntimeType* GenInst_TimeSpan_t384326017_0_0_0_Types[] = { (&TimeSpan_t384326017_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeSpan_t384326017_0_0_0 = { 1, GenInst_TimeSpan_t384326017_0_0_0_Types };
static const RuntimeType* GenInst_TypeTag_t3240722718_0_0_0_Types[] = { (&TypeTag_t3240722718_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeTag_t3240722718_0_0_0 = { 1, GenInst_TypeTag_t3240722718_0_0_0_Types };
static const RuntimeType* GenInst_MonoType_t_0_0_0_Types[] = { (&MonoType_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
static const RuntimeType* GenInst_StrongName_t2336798789_0_0_0_Types[] = { (&StrongName_t2336798789_0_0_0) };
extern const Il2CppGenericInst GenInst_StrongName_t2336798789_0_0_0 = { 1, GenInst_StrongName_t2336798789_0_0_0_Types };
static const RuntimeType* GenInst_IBuiltInEvidence_t101773638_0_0_0_Types[] = { (&IBuiltInEvidence_t101773638_0_0_0) };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t101773638_0_0_0 = { 1, GenInst_IBuiltInEvidence_t101773638_0_0_0_Types };
static const RuntimeType* GenInst_IIdentityPermissionFactory_t2821289363_0_0_0_Types[] = { (&IIdentityPermissionFactory_t2821289363_0_0_0) };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t2821289363_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t2821289363_0_0_0_Types };
static const RuntimeType* GenInst_DateTimeOffset_t2358022680_0_0_0_Types[] = { (&DateTimeOffset_t2358022680_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t2358022680_0_0_0 = { 1, GenInst_DateTimeOffset_t2358022680_0_0_0_Types };
static const RuntimeType* GenInst_Guid_t_0_0_0_Types[] = { (&Guid_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
static const RuntimeType* GenInst_Version_t618301286_0_0_0_Types[] = { (&Version_t618301286_0_0_0) };
extern const Il2CppGenericInst GenInst_Version_t618301286_0_0_0 = { 1, GenInst_Version_t618301286_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1817943545_0_0_0_Types[] = { (&KeyValuePair_2_t1817943545_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1817943545_0_0_0 = { 1, GenInst_KeyValuePair_2_t1817943545_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t988759797_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_Boolean_t988759797_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t988759797_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_KeyValuePair_2_t1817943545_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t988759797_0_0_0), (&KeyValuePair_2_t1817943545_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_KeyValuePair_2_t1817943545_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_KeyValuePair_2_t1817943545_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t988759797_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0_KeyValuePair_2_t1542717293_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t988759797_0_0_0), (&KeyValuePair_2_t1542717293_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0_KeyValuePair_2_t1542717293_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0_KeyValuePair_2_t1542717293_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1542717293_0_0_0_Types[] = { (&KeyValuePair_2_t1542717293_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1542717293_0_0_0 = { 1, GenInst_KeyValuePair_2_t1542717293_0_0_0_Types };
static const RuntimeType* GenInst_X509Certificate_t3436509184_0_0_0_Types[] = { (&X509Certificate_t3436509184_0_0_0) };
extern const Il2CppGenericInst GenInst_X509Certificate_t3436509184_0_0_0 = { 1, GenInst_X509Certificate_t3436509184_0_0_0_Types };
static const RuntimeType* GenInst_IDeserializationCallback_t306402524_0_0_0_Types[] = { (&IDeserializationCallback_t306402524_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t306402524_0_0_0 = { 1, GenInst_IDeserializationCallback_t306402524_0_0_0_Types };
static const RuntimeType* GenInst_X509ChainStatus_t703314169_0_0_0_Types[] = { (&X509ChainStatus_t703314169_0_0_0) };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t703314169_0_0_0 = { 1, GenInst_X509ChainStatus_t703314169_0_0_0_Types };
static const RuntimeType* GenInst_Capture_t2938944230_0_0_0_Types[] = { (&Capture_t2938944230_0_0_0) };
extern const Il2CppGenericInst GenInst_Capture_t2938944230_0_0_0 = { 1, GenInst_Capture_t2938944230_0_0_0_Types };
static const RuntimeType* GenInst_Group_t2651207689_0_0_0_Types[] = { (&Group_t2651207689_0_0_0) };
extern const Il2CppGenericInst GenInst_Group_t2651207689_0_0_0 = { 1, GenInst_Group_t2651207689_0_0_0_Types };
static const RuntimeType* GenInst_Mark_t3695721384_0_0_0_Types[] = { (&Mark_t3695721384_0_0_0) };
extern const Il2CppGenericInst GenInst_Mark_t3695721384_0_0_0 = { 1, GenInst_Mark_t3695721384_0_0_0_Types };
static const RuntimeType* GenInst_UriScheme_t2057891215_0_0_0_Types[] = { (&UriScheme_t2057891215_0_0_0) };
extern const Il2CppGenericInst GenInst_UriScheme_t2057891215_0_0_0 = { 1, GenInst_UriScheme_t2057891215_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t1041289571_0_0_0_Types[] = { (&BigInteger_t1041289571_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t1041289571_0_0_0 = { 1, GenInst_BigInteger_t1041289571_0_0_0_Types };
static const RuntimeType* GenInst_ByteU5BU5D_t4256625864_0_0_0_Types[] = { (&ByteU5BU5D_t4256625864_0_0_0) };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t4256625864_0_0_0 = { 1, GenInst_ByteU5BU5D_t4256625864_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t351557133_0_0_0_Types[] = { (&IList_1_t351557133_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t351557133_0_0_0 = { 1, GenInst_IList_1_t351557133_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3983820767_0_0_0_Types[] = { (&ICollection_1_t3983820767_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3983820767_0_0_0 = { 1, GenInst_ICollection_1_t3983820767_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1812804193_0_0_0_Types[] = { (&IEnumerable_1_t1812804193_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1812804193_0_0_0 = { 1, GenInst_IEnumerable_1_t1812804193_0_0_0_Types };
static const RuntimeType* GenInst_ClientCertificateType_t1318574781_0_0_0_Types[] = { (&ClientCertificateType_t1318574781_0_0_0) };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1318574781_0_0_0 = { 1, GenInst_ClientCertificateType_t1318574781_0_0_0_Types };
static const RuntimeType* GenInst_Link_t2244904633_0_0_0_Types[] = { (&Link_t2244904633_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t2244904633_0_0_0 = { 1, GenInst_Link_t2244904633_0_0_0_Types };
static const RuntimeType* GenInst_Object_t2399819029_0_0_0_Types[] = { (&Object_t2399819029_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_t2399819029_0_0_0 = { 1, GenInst_Object_t2399819029_0_0_0_Types };
static const RuntimeType* GenInst_Camera_t4214422582_0_0_0_Types[] = { (&Camera_t4214422582_0_0_0) };
extern const Il2CppGenericInst GenInst_Camera_t4214422582_0_0_0 = { 1, GenInst_Camera_t4214422582_0_0_0_Types };
static const RuntimeType* GenInst_Behaviour_t1869462422_0_0_0_Types[] = { (&Behaviour_t1869462422_0_0_0) };
extern const Il2CppGenericInst GenInst_Behaviour_t1869462422_0_0_0 = { 1, GenInst_Behaviour_t1869462422_0_0_0_Types };
static const RuntimeType* GenInst_Component_t1209726582_0_0_0_Types[] = { (&Component_t1209726582_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_t1209726582_0_0_0 = { 1, GenInst_Component_t1209726582_0_0_0_Types };
static const RuntimeType* GenInst_Display_t964176615_0_0_0_Types[] = { (&Display_t964176615_0_0_0) };
extern const Il2CppGenericInst GenInst_Display_t964176615_0_0_0 = { 1, GenInst_Display_t964176615_0_0_0_Types };
static const RuntimeType* GenInst_Keyframe_t1034491164_0_0_0_Types[] = { (&Keyframe_t1034491164_0_0_0) };
extern const Il2CppGenericInst GenInst_Keyframe_t1034491164_0_0_0 = { 1, GenInst_Keyframe_t1034491164_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t174917947_0_0_0_Types[] = { (&Vector3_t174917947_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t174917947_0_0_0 = { 1, GenInst_Vector3_t174917947_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t181764249_0_0_0_Types[] = { (&Vector4_t181764249_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t181764249_0_0_0 = { 1, GenInst_Vector4_t181764249_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t2867110760_0_0_0_Types[] = { (&Vector2_t2867110760_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t2867110760_0_0_0 = { 1, GenInst_Vector2_t2867110760_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t338341170_0_0_0_Types[] = { (&Color32_t338341170_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t338341170_0_0_0 = { 1, GenInst_Color32_t338341170_0_0_0_Types };
static const RuntimeType* GenInst_Playable_t882738958_0_0_0_Types[] = { (&Playable_t882738958_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_t882738958_0_0_0 = { 1, GenInst_Playable_t882738958_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_t2283729907_0_0_0_Types[] = { (&PlayableOutput_t2283729907_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_t2283729907_0_0_0 = { 1, GenInst_PlayableOutput_t2283729907_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1653993680_0_0_0_LoadSceneMode_t3425002508_0_0_0_Types[] = { (&Scene_t1653993680_0_0_0), (&LoadSceneMode_t3425002508_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1653993680_0_0_0_LoadSceneMode_t3425002508_0_0_0 = { 2, GenInst_Scene_t1653993680_0_0_0_LoadSceneMode_t3425002508_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1653993680_0_0_0_Types[] = { (&Scene_t1653993680_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1653993680_0_0_0 = { 1, GenInst_Scene_t1653993680_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1653993680_0_0_0_Scene_t1653993680_0_0_0_Types[] = { (&Scene_t1653993680_0_0_0), (&Scene_t1653993680_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1653993680_0_0_0_Scene_t1653993680_0_0_0 = { 2, GenInst_Scene_t1653993680_0_0_0_Scene_t1653993680_0_0_0_Types };
static const RuntimeType* GenInst_SpriteAtlas_t2865821040_0_0_0_Types[] = { (&SpriteAtlas_t2865821040_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteAtlas_t2865821040_0_0_0 = { 1, GenInst_SpriteAtlas_t2865821040_0_0_0_Types };
static const RuntimeType* GenInst_Rigidbody2D_t1937802487_0_0_0_Types[] = { (&Rigidbody2D_t1937802487_0_0_0) };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t1937802487_0_0_0 = { 1, GenInst_Rigidbody2D_t1937802487_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit2D_t2595119295_0_0_0_Types[] = { (&RaycastHit2D_t2595119295_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t2595119295_0_0_0 = { 1, GenInst_RaycastHit2D_t2595119295_0_0_0_Types };
static const RuntimeType* GenInst_ContactPoint2D_t4070334835_0_0_0_Types[] = { (&ContactPoint2D_t4070334835_0_0_0) };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t4070334835_0_0_0 = { 1, GenInst_ContactPoint2D_t4070334835_0_0_0_Types };
static const RuntimeType* GenInst_AudioClipPlayable_t2750948543_0_0_0_Types[] = { (&AudioClipPlayable_t2750948543_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioClipPlayable_t2750948543_0_0_0 = { 1, GenInst_AudioClipPlayable_t2750948543_0_0_0_Types };
static const RuntimeType* GenInst_AudioMixerPlayable_t2521349572_0_0_0_Types[] = { (&AudioMixerPlayable_t2521349572_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioMixerPlayable_t2521349572_0_0_0 = { 1, GenInst_AudioMixerPlayable_t2521349572_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClipPlayable_t2199148327_0_0_0_Types[] = { (&AnimationClipPlayable_t2199148327_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClipPlayable_t2199148327_0_0_0 = { 1, GenInst_AnimationClipPlayable_t2199148327_0_0_0_Types };
static const RuntimeType* GenInst_AnimationLayerMixerPlayable_t3950440205_0_0_0_Types[] = { (&AnimationLayerMixerPlayable_t3950440205_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationLayerMixerPlayable_t3950440205_0_0_0 = { 1, GenInst_AnimationLayerMixerPlayable_t3950440205_0_0_0_Types };
static const RuntimeType* GenInst_AnimationMixerPlayable_t635383928_0_0_0_Types[] = { (&AnimationMixerPlayable_t635383928_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationMixerPlayable_t635383928_0_0_0 = { 1, GenInst_AnimationMixerPlayable_t635383928_0_0_0_Types };
static const RuntimeType* GenInst_AnimationOffsetPlayable_t1025003618_0_0_0_Types[] = { (&AnimationOffsetPlayable_t1025003618_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationOffsetPlayable_t1025003618_0_0_0 = { 1, GenInst_AnimationOffsetPlayable_t1025003618_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerPlayable_t421975501_0_0_0_Types[] = { (&AnimatorControllerPlayable_t421975501_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerPlayable_t421975501_0_0_0 = { 1, GenInst_AnimatorControllerPlayable_t421975501_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorClipInfo_t506398021_0_0_0_Types[] = { (&AnimatorClipInfo_t506398021_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t506398021_0_0_0 = { 1, GenInst_AnimatorClipInfo_t506398021_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerParameter_t923037798_0_0_0_Types[] = { (&AnimatorControllerParameter_t923037798_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerParameter_t923037798_0_0_0 = { 1, GenInst_AnimatorControllerParameter_t923037798_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t2554352826_0_0_0_Types[] = { (&UIVertex_t2554352826_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t2554352826_0_0_0 = { 1, GenInst_UIVertex_t2554352826_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t1978200295_0_0_0_Types[] = { (&UICharInfo_t1978200295_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t1978200295_0_0_0 = { 1, GenInst_UICharInfo_t1978200295_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t202048947_0_0_0_Types[] = { (&UILineInfo_t202048947_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t202048947_0_0_0 = { 1, GenInst_UILineInfo_t202048947_0_0_0_Types };
static const RuntimeType* GenInst_Font_t3423958016_0_0_0_Types[] = { (&Font_t3423958016_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t3423958016_0_0_0 = { 1, GenInst_Font_t3423958016_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutOption_t1026689508_0_0_0_Types[] = { (&GUILayoutOption_t1026689508_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t1026689508_0_0_0 = { 1, GenInst_GUILayoutOption_t1026689508_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutEntry_t3218050312_0_0_0_Types[] = { (&GUILayoutEntry_t3218050312_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3218050312_0_0_0 = { 1, GenInst_GUILayoutEntry_t3218050312_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&LayoutCache_t3250357553_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0 = { 2, GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1520347806_0_0_0_Types[] = { (&KeyValuePair_2_t1520347806_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1520347806_0_0_0 = { 1, GenInst_KeyValuePair_2_t1520347806_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1520347806_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t1520347806_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1520347806_0_0_0 = { 3, GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1520347806_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&LayoutCache_t3250357553_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0_KeyValuePair_2_t2781040199_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&LayoutCache_t3250357553_0_0_0), (&KeyValuePair_2_t2781040199_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0_KeyValuePair_2_t2781040199_0_0_0 = { 3, GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0_KeyValuePair_2_t2781040199_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2781040199_0_0_0_Types[] = { (&KeyValuePair_2_t2781040199_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2781040199_0_0_0 = { 1, GenInst_KeyValuePair_2_t2781040199_0_0_0_Types };
static const RuntimeType* GenInst_GUIStyle_t236231435_0_0_0_Types[] = { (&GUIStyle_t236231435_0_0_0) };
extern const Il2CppGenericInst GenInst_GUIStyle_t236231435_0_0_0 = { 1, GenInst_GUIStyle_t236231435_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t236231435_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t236231435_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_KeyValuePair_2_t790188931_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t236231435_0_0_0), (&KeyValuePair_2_t790188931_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_KeyValuePair_2_t790188931_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_KeyValuePair_2_t790188931_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t790188931_0_0_0_Types[] = { (&KeyValuePair_2_t790188931_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t790188931_0_0_0 = { 1, GenInst_KeyValuePair_2_t790188931_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_GUIStyle_t236231435_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t236231435_0_0_0), (&GUIStyle_t236231435_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_GUIStyle_t236231435_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_GUIStyle_t236231435_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_IntPtr_t_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&IntPtr_t_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_IntPtr_t_0_0_0_Boolean_t988759797_0_0_0 = { 3, GenInst_Int32_t2803531614_0_0_0_IntPtr_t_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t4239825407_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&Exception_t4239825407_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t4239825407_0_0_0_Boolean_t988759797_0_0_0 = { 2, GenInst_Exception_t4239825407_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_UserProfile_t700863329_0_0_0_Types[] = { (&UserProfile_t700863329_0_0_0) };
extern const Il2CppGenericInst GenInst_UserProfile_t700863329_0_0_0 = { 1, GenInst_UserProfile_t700863329_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfile_t2763119316_0_0_0_Types[] = { (&IUserProfile_t2763119316_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfile_t2763119316_0_0_0 = { 1, GenInst_IUserProfile_t2763119316_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t988759797_0_0_0_String_t_0_0_0_Types[] = { (&Boolean_t988759797_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t988759797_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t988759797_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t988759797_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Boolean_t988759797_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t988759797_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Boolean_t988759797_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AchievementDescription_t1494495164_0_0_0_Types[] = { (&AchievementDescription_t1494495164_0_0_0) };
extern const Il2CppGenericInst GenInst_AchievementDescription_t1494495164_0_0_0 = { 1, GenInst_AchievementDescription_t1494495164_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescription_t452714252_0_0_0_Types[] = { (&IAchievementDescription_t452714252_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t452714252_0_0_0 = { 1, GenInst_IAchievementDescription_t452714252_0_0_0_Types };
static const RuntimeType* GenInst_GcLeaderboard_t436677311_0_0_0_Types[] = { (&GcLeaderboard_t436677311_0_0_0) };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t436677311_0_0_0 = { 1, GenInst_GcLeaderboard_t436677311_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescriptionU5BU5D_t2589288261_0_0_0_Types[] = { (&IAchievementDescriptionU5BU5D_t2589288261_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t2589288261_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t2589288261_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementU5BU5D_t2444041797_0_0_0_Types[] = { (&IAchievementU5BU5D_t2444041797_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2444041797_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2444041797_0_0_0_Types };
static const RuntimeType* GenInst_IAchievement_t190526476_0_0_0_Types[] = { (&IAchievement_t190526476_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievement_t190526476_0_0_0 = { 1, GenInst_IAchievement_t190526476_0_0_0_Types };
static const RuntimeType* GenInst_GcAchievementData_t3124305070_0_0_0_Types[] = { (&GcAchievementData_t3124305070_0_0_0) };
extern const Il2CppGenericInst GenInst_GcAchievementData_t3124305070_0_0_0 = { 1, GenInst_GcAchievementData_t3124305070_0_0_0_Types };
static const RuntimeType* GenInst_Achievement_t1395733712_0_0_0_Types[] = { (&Achievement_t1395733712_0_0_0) };
extern const Il2CppGenericInst GenInst_Achievement_t1395733712_0_0_0 = { 1, GenInst_Achievement_t1395733712_0_0_0_Types };
static const RuntimeType* GenInst_IScoreU5BU5D_t3632149934_0_0_0_Types[] = { (&IScoreU5BU5D_t3632149934_0_0_0) };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3632149934_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3632149934_0_0_0_Types };
static const RuntimeType* GenInst_IScore_t1344207895_0_0_0_Types[] = { (&IScore_t1344207895_0_0_0) };
extern const Il2CppGenericInst GenInst_IScore_t1344207895_0_0_0 = { 1, GenInst_IScore_t1344207895_0_0_0_Types };
static const RuntimeType* GenInst_GcScoreData_t104171333_0_0_0_Types[] = { (&GcScoreData_t104171333_0_0_0) };
extern const Il2CppGenericInst GenInst_GcScoreData_t104171333_0_0_0 = { 1, GenInst_GcScoreData_t104171333_0_0_0_Types };
static const RuntimeType* GenInst_Score_t939050586_0_0_0_Types[] = { (&Score_t939050586_0_0_0) };
extern const Il2CppGenericInst GenInst_Score_t939050586_0_0_0 = { 1, GenInst_Score_t939050586_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfileU5BU5D_t1403542365_0_0_0_Types[] = { (&IUserProfileU5BU5D_t1403542365_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t1403542365_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t1403542365_0_0_0_Types };
static const RuntimeType* GenInst_DisallowMultipleComponent_t2116310945_0_0_0_Types[] = { (&DisallowMultipleComponent_t2116310945_0_0_0) };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2116310945_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2116310945_0_0_0_Types };
static const RuntimeType* GenInst_Attribute_t3232323375_0_0_0_Types[] = { (&Attribute_t3232323375_0_0_0) };
extern const Il2CppGenericInst GenInst_Attribute_t3232323375_0_0_0 = { 1, GenInst_Attribute_t3232323375_0_0_0_Types };
static const RuntimeType* GenInst__Attribute_t3061038810_0_0_0_Types[] = { (&_Attribute_t3061038810_0_0_0) };
extern const Il2CppGenericInst GenInst__Attribute_t3061038810_0_0_0 = { 1, GenInst__Attribute_t3061038810_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteInEditMode_t2112836633_0_0_0_Types[] = { (&ExecuteInEditMode_t2112836633_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t2112836633_0_0_0 = { 1, GenInst_ExecuteInEditMode_t2112836633_0_0_0_Types };
static const RuntimeType* GenInst_RequireComponent_t709491642_0_0_0_Types[] = { (&RequireComponent_t709491642_0_0_0) };
extern const Il2CppGenericInst GenInst_RequireComponent_t709491642_0_0_0 = { 1, GenInst_RequireComponent_t709491642_0_0_0_Types };
static const RuntimeType* GenInst_HitInfo_t808200858_0_0_0_Types[] = { (&HitInfo_t808200858_0_0_0) };
extern const Il2CppGenericInst GenInst_HitInfo_t808200858_0_0_0 = { 1, GenInst_HitInfo_t808200858_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 4, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_PersistentCall_t1665637431_0_0_0_Types[] = { (&PersistentCall_t1665637431_0_0_0) };
extern const Il2CppGenericInst GenInst_PersistentCall_t1665637431_0_0_0 = { 1, GenInst_PersistentCall_t1665637431_0_0_0_Types };
static const RuntimeType* GenInst_BaseInvokableCall_t185741398_0_0_0_Types[] = { (&BaseInvokableCall_t185741398_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t185741398_0_0_0 = { 1, GenInst_BaseInvokableCall_t185741398_0_0_0_Types };
static const RuntimeType* GenInst_WorkRequest_t1285289635_0_0_0_Types[] = { (&WorkRequest_t1285289635_0_0_0) };
extern const Il2CppGenericInst GenInst_WorkRequest_t1285289635_0_0_0 = { 1, GenInst_WorkRequest_t1285289635_0_0_0_Types };
static const RuntimeType* GenInst_PlayableBinding_t198519643_0_0_0_Types[] = { (&PlayableBinding_t198519643_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableBinding_t198519643_0_0_0 = { 1, GenInst_PlayableBinding_t198519643_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t2455387773_0_0_0_Types[] = { (&MessageTypeSubscribers_t2455387773_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2455387773_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t2455387773_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t2455387773_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&MessageTypeSubscribers_t2455387773_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2455387773_0_0_0_Boolean_t988759797_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t2455387773_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_MessageEventArgs_t1676961500_0_0_0_Types[] = { (&MessageEventArgs_t1676961500_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t1676961500_0_0_0 = { 1, GenInst_MessageEventArgs_t1676961500_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t3337740345_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t153286347_0_0_0_Types[] = { (&KeyValuePair_2_t153286347_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t153286347_0_0_0 = { 1, GenInst_KeyValuePair_2_t153286347_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t153286347_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t153286347_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t153286347_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t153286347_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t3337740345_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0_KeyValuePair_2_t1501361532_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t3337740345_0_0_0), (&KeyValuePair_2_t1501361532_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0_KeyValuePair_2_t1501361532_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0_KeyValuePair_2_t1501361532_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1501361532_0_0_0_Types[] = { (&KeyValuePair_2_t1501361532_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1501361532_0_0_0 = { 1, GenInst_KeyValuePair_2_t1501361532_0_0_0_Types };
static const RuntimeType* GenInst_BaseInputModule_t3534238441_0_0_0_Types[] = { (&BaseInputModule_t3534238441_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInputModule_t3534238441_0_0_0 = { 1, GenInst_BaseInputModule_t3534238441_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t791495392_0_0_0_Types[] = { (&RaycastResult_t791495392_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t791495392_0_0_0 = { 1, GenInst_RaycastResult_t791495392_0_0_0_Types };
static const RuntimeType* GenInst_IDeselectHandler_t1821424147_0_0_0_Types[] = { (&IDeselectHandler_t1821424147_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t1821424147_0_0_0 = { 1, GenInst_IDeselectHandler_t1821424147_0_0_0_Types };
static const RuntimeType* GenInst_IEventSystemHandler_t550700969_0_0_0_Types[] = { (&IEventSystemHandler_t550700969_0_0_0) };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t550700969_0_0_0 = { 1, GenInst_IEventSystemHandler_t550700969_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t620318833_0_0_0_Types[] = { (&List_1_t620318833_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t620318833_0_0_0 = { 1, GenInst_List_1_t620318833_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2059283024_0_0_0_Types[] = { (&List_1_t2059283024_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2059283024_0_0_0 = { 1, GenInst_List_1_t2059283024_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1279344446_0_0_0_Types[] = { (&List_1_t1279344446_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1279344446_0_0_0 = { 1, GenInst_List_1_t1279344446_0_0_0_Types };
static const RuntimeType* GenInst_ISelectHandler_t3646584355_0_0_0_Types[] = { (&ISelectHandler_t3646584355_0_0_0) };
extern const Il2CppGenericInst GenInst_ISelectHandler_t3646584355_0_0_0 = { 1, GenInst_ISelectHandler_t3646584355_0_0_0_Types };
static const RuntimeType* GenInst_BaseRaycaster_t2834558618_0_0_0_Types[] = { (&BaseRaycaster_t2834558618_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2834558618_0_0_0 = { 1, GenInst_BaseRaycaster_t2834558618_0_0_0_Types };
static const RuntimeType* GenInst_Entry_t1721113290_0_0_0_Types[] = { (&Entry_t1721113290_0_0_0) };
extern const Il2CppGenericInst GenInst_Entry_t1721113290_0_0_0 = { 1, GenInst_Entry_t1721113290_0_0_0_Types };
static const RuntimeType* GenInst_BaseEventData_t2239799759_0_0_0_Types[] = { (&BaseEventData_t2239799759_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseEventData_t2239799759_0_0_0 = { 1, GenInst_BaseEventData_t2239799759_0_0_0_Types };
static const RuntimeType* GenInst_IPointerEnterHandler_t3359589364_0_0_0_Types[] = { (&IPointerEnterHandler_t3359589364_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t3359589364_0_0_0 = { 1, GenInst_IPointerEnterHandler_t3359589364_0_0_0_Types };
static const RuntimeType* GenInst_IPointerExitHandler_t1194585081_0_0_0_Types[] = { (&IPointerExitHandler_t1194585081_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t1194585081_0_0_0 = { 1, GenInst_IPointerExitHandler_t1194585081_0_0_0_Types };
static const RuntimeType* GenInst_IPointerDownHandler_t1934540052_0_0_0_Types[] = { (&IPointerDownHandler_t1934540052_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t1934540052_0_0_0 = { 1, GenInst_IPointerDownHandler_t1934540052_0_0_0_Types };
static const RuntimeType* GenInst_IPointerUpHandler_t1595114040_0_0_0_Types[] = { (&IPointerUpHandler_t1595114040_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t1595114040_0_0_0 = { 1, GenInst_IPointerUpHandler_t1595114040_0_0_0_Types };
static const RuntimeType* GenInst_IPointerClickHandler_t3898132831_0_0_0_Types[] = { (&IPointerClickHandler_t3898132831_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t3898132831_0_0_0 = { 1, GenInst_IPointerClickHandler_t3898132831_0_0_0_Types };
static const RuntimeType* GenInst_IInitializePotentialDragHandler_t133502056_0_0_0_Types[] = { (&IInitializePotentialDragHandler_t133502056_0_0_0) };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t133502056_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t133502056_0_0_0_Types };
static const RuntimeType* GenInst_IBeginDragHandler_t3066996802_0_0_0_Types[] = { (&IBeginDragHandler_t3066996802_0_0_0) };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3066996802_0_0_0 = { 1, GenInst_IBeginDragHandler_t3066996802_0_0_0_Types };
static const RuntimeType* GenInst_IDragHandler_t1430837312_0_0_0_Types[] = { (&IDragHandler_t1430837312_0_0_0) };
extern const Il2CppGenericInst GenInst_IDragHandler_t1430837312_0_0_0 = { 1, GenInst_IDragHandler_t1430837312_0_0_0_Types };
static const RuntimeType* GenInst_IEndDragHandler_t1502010433_0_0_0_Types[] = { (&IEndDragHandler_t1502010433_0_0_0) };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1502010433_0_0_0 = { 1, GenInst_IEndDragHandler_t1502010433_0_0_0_Types };
static const RuntimeType* GenInst_IDropHandler_t4289830363_0_0_0_Types[] = { (&IDropHandler_t4289830363_0_0_0) };
extern const Il2CppGenericInst GenInst_IDropHandler_t4289830363_0_0_0 = { 1, GenInst_IDropHandler_t4289830363_0_0_0_Types };
static const RuntimeType* GenInst_IScrollHandler_t2947048734_0_0_0_Types[] = { (&IScrollHandler_t2947048734_0_0_0) };
extern const Il2CppGenericInst GenInst_IScrollHandler_t2947048734_0_0_0 = { 1, GenInst_IScrollHandler_t2947048734_0_0_0_Types };
static const RuntimeType* GenInst_IUpdateSelectedHandler_t2533267811_0_0_0_Types[] = { (&IUpdateSelectedHandler_t2533267811_0_0_0) };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t2533267811_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t2533267811_0_0_0_Types };
static const RuntimeType* GenInst_IMoveHandler_t2306254927_0_0_0_Types[] = { (&IMoveHandler_t2306254927_0_0_0) };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2306254927_0_0_0 = { 1, GenInst_IMoveHandler_t2306254927_0_0_0_Types };
static const RuntimeType* GenInst_ISubmitHandler_t1593443308_0_0_0_Types[] = { (&ISubmitHandler_t1593443308_0_0_0) };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t1593443308_0_0_0 = { 1, GenInst_ISubmitHandler_t1593443308_0_0_0_Types };
static const RuntimeType* GenInst_ICancelHandler_t1896074317_0_0_0_Types[] = { (&ICancelHandler_t1896074317_0_0_0) };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1896074317_0_0_0 = { 1, GenInst_ICancelHandler_t1896074317_0_0_0_Types };
static const RuntimeType* GenInst_Transform_t2451218446_0_0_0_Types[] = { (&Transform_t2451218446_0_0_0) };
extern const Il2CppGenericInst GenInst_Transform_t2451218446_0_0_0 = { 1, GenInst_Transform_t2451218446_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_t2629728357_0_0_0_Types[] = { (&GameObject_t2629728357_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_t2629728357_0_0_0 = { 1, GenInst_GameObject_t2629728357_0_0_0_Types };
static const RuntimeType* GenInst_BaseInput_t718558484_0_0_0_Types[] = { (&BaseInput_t718558484_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInput_t718558484_0_0_0 = { 1, GenInst_BaseInput_t718558484_0_0_0_Types };
static const RuntimeType* GenInst_UIBehaviour_t4190033142_0_0_0_Types[] = { (&UIBehaviour_t4190033142_0_0_0) };
extern const Il2CppGenericInst GenInst_UIBehaviour_t4190033142_0_0_0 = { 1, GenInst_UIBehaviour_t4190033142_0_0_0_Types };
static const RuntimeType* GenInst_MonoBehaviour_t3163037868_0_0_0_Types[] = { (&MonoBehaviour_t3163037868_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t3163037868_0_0_0 = { 1, GenInst_MonoBehaviour_t3163037868_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&PointerEventData_t2898711789_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0 = { 2, GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&PointerEventData_t2898711789_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_KeyValuePair_2_t2429394435_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&PointerEventData_t2898711789_0_0_0), (&KeyValuePair_2_t2429394435_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_KeyValuePair_2_t2429394435_0_0_0 = { 3, GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_KeyValuePair_2_t2429394435_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2429394435_0_0_0_Types[] = { (&KeyValuePair_2_t2429394435_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2429394435_0_0_0 = { 1, GenInst_KeyValuePair_2_t2429394435_0_0_0_Types };
static const RuntimeType* GenInst_PointerEventData_t2898711789_0_0_0_Types[] = { (&PointerEventData_t2898711789_0_0_0) };
extern const Il2CppGenericInst GenInst_PointerEventData_t2898711789_0_0_0 = { 1, GenInst_PointerEventData_t2898711789_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_PointerEventData_t2898711789_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&PointerEventData_t2898711789_0_0_0), (&PointerEventData_t2898711789_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_PointerEventData_t2898711789_0_0_0 = { 3, GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_PointerEventData_t2898711789_0_0_0_Types };
static const RuntimeType* GenInst_ButtonState_t4280795307_0_0_0_Types[] = { (&ButtonState_t4280795307_0_0_0) };
extern const Il2CppGenericInst GenInst_ButtonState_t4280795307_0_0_0 = { 1, GenInst_ButtonState_t4280795307_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit_t3326614521_0_0_0_Types[] = { (&RaycastHit_t3326614521_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit_t3326614521_0_0_0 = { 1, GenInst_RaycastHit_t3326614521_0_0_0_Types };
static const RuntimeType* GenInst_Color_t1894673040_0_0_0_Types[] = { (&Color_t1894673040_0_0_0) };
extern const Il2CppGenericInst GenInst_Color_t1894673040_0_0_0 = { 1, GenInst_Color_t1894673040_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t3347193002_0_0_0_Types[] = { (&ICanvasElement_t3347193002_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t3347193002_0_0_0 = { 1, GenInst_ICanvasElement_t3347193002_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0_Types[] = { (&ICanvasElement_t3347193002_0_0_0), (&Int32_t2803531614_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0 = { 2, GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&ICanvasElement_t3347193002_0_0_0), (&Int32_t2803531614_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_ColorBlock_t3790099472_0_0_0_Types[] = { (&ColorBlock_t3790099472_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorBlock_t3790099472_0_0_0 = { 1, GenInst_ColorBlock_t3790099472_0_0_0_Types };
static const RuntimeType* GenInst_OptionData_t309155721_0_0_0_Types[] = { (&OptionData_t309155721_0_0_0) };
extern const Il2CppGenericInst GenInst_OptionData_t309155721_0_0_0 = { 1, GenInst_OptionData_t309155721_0_0_0_Types };
static const RuntimeType* GenInst_DropdownItem_t1339148254_0_0_0_Types[] = { (&DropdownItem_t1339148254_0_0_0) };
extern const Il2CppGenericInst GenInst_DropdownItem_t1339148254_0_0_0 = { 1, GenInst_DropdownItem_t1339148254_0_0_0_Types };
static const RuntimeType* GenInst_FloatTween_t820617968_0_0_0_Types[] = { (&FloatTween_t820617968_0_0_0) };
extern const Il2CppGenericInst GenInst_FloatTween_t820617968_0_0_0 = { 1, GenInst_FloatTween_t820617968_0_0_0_Types };
static const RuntimeType* GenInst_Sprite_t3431485579_0_0_0_Types[] = { (&Sprite_t3431485579_0_0_0) };
extern const Il2CppGenericInst GenInst_Sprite_t3431485579_0_0_0 = { 1, GenInst_Sprite_t3431485579_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t2785269944_0_0_0_Types[] = { (&Canvas_t2785269944_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t2785269944_0_0_0 = { 1, GenInst_Canvas_t2785269944_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2854887808_0_0_0_Types[] = { (&List_1_t2854887808_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2854887808_0_0_0 = { 1, GenInst_List_1_t2854887808_0_0_0_Types };
static const RuntimeType* GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0_Types[] = { (&Font_t3423958016_0_0_0), (&HashSet_1_t1006781235_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0 = { 2, GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0_Types };
static const RuntimeType* GenInst_Text_t4164675009_0_0_0_Types[] = { (&Text_t4164675009_0_0_0) };
extern const Il2CppGenericInst GenInst_Text_t4164675009_0_0_0 = { 1, GenInst_Text_t4164675009_0_0_0_Types };
static const RuntimeType* GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&Font_t3423958016_0_0_0), (&HashSet_1_t1006781235_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0_KeyValuePair_2_t800773855_0_0_0_Types[] = { (&Font_t3423958016_0_0_0), (&HashSet_1_t1006781235_0_0_0), (&KeyValuePair_2_t800773855_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0_KeyValuePair_2_t800773855_0_0_0 = { 3, GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0_KeyValuePair_2_t800773855_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t800773855_0_0_0_Types[] = { (&KeyValuePair_2_t800773855_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t800773855_0_0_0 = { 1, GenInst_KeyValuePair_2_t800773855_0_0_0_Types };
static const RuntimeType* GenInst_ColorTween_t3542451946_0_0_0_Types[] = { (&ColorTween_t3542451946_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorTween_t3542451946_0_0_0 = { 1, GenInst_ColorTween_t3542451946_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t3593681171_0_0_0_Types[] = { (&Graphic_t3593681171_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t3593681171_0_0_0 = { 1, GenInst_Graphic_t3593681171_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0_Types[] = { (&Canvas_t2785269944_0_0_0), (&IndexedSet_1_t3015234795_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0 = { 2, GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0_Types[] = { (&Graphic_t3593681171_0_0_0), (&Int32_t2803531614_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0 = { 2, GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&Graphic_t3593681171_0_0_0), (&Int32_t2803531614_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&Canvas_t2785269944_0_0_0), (&IndexedSet_1_t3015234795_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0_KeyValuePair_2_t1842217471_0_0_0_Types[] = { (&Canvas_t2785269944_0_0_0), (&IndexedSet_1_t3015234795_0_0_0), (&KeyValuePair_2_t1842217471_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0_KeyValuePair_2_t1842217471_0_0_0 = { 3, GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0_KeyValuePair_2_t1842217471_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1842217471_0_0_0_Types[] = { (&KeyValuePair_2_t1842217471_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1842217471_0_0_0 = { 1, GenInst_KeyValuePair_2_t1842217471_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t2710483019_0_0_0_Types[] = { (&Graphic_t3593681171_0_0_0), (&Int32_t2803531614_0_0_0), (&KeyValuePair_2_t2710483019_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t2710483019_0_0_0 = { 3, GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t2710483019_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2710483019_0_0_0_Types[] = { (&KeyValuePair_2_t2710483019_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2710483019_0_0_0 = { 1, GenInst_KeyValuePair_2_t2710483019_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3246846072_0_0_0_Types[] = { (&ICanvasElement_t3347193002_0_0_0), (&Int32_t2803531614_0_0_0), (&KeyValuePair_2_t3246846072_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3246846072_0_0_0 = { 3, GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3246846072_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3246846072_0_0_0_Types[] = { (&KeyValuePair_2_t3246846072_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3246846072_0_0_0 = { 1, GenInst_KeyValuePair_2_t3246846072_0_0_0_Types };
static const RuntimeType* GenInst_Type_t2268913483_0_0_0_Types[] = { (&Type_t2268913483_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t2268913483_0_0_0 = { 1, GenInst_Type_t2268913483_0_0_0_Types };
static const RuntimeType* GenInst_FillMethod_t1193514586_0_0_0_Types[] = { (&FillMethod_t1193514586_0_0_0) };
extern const Il2CppGenericInst GenInst_FillMethod_t1193514586_0_0_0 = { 1, GenInst_FillMethod_t1193514586_0_0_0_Types };
static const RuntimeType* GenInst_ContentType_t2862244902_0_0_0_Types[] = { (&ContentType_t2862244902_0_0_0) };
extern const Il2CppGenericInst GenInst_ContentType_t2862244902_0_0_0 = { 1, GenInst_ContentType_t2862244902_0_0_0_Types };
static const RuntimeType* GenInst_LineType_t2260719266_0_0_0_Types[] = { (&LineType_t2260719266_0_0_0) };
extern const Il2CppGenericInst GenInst_LineType_t2260719266_0_0_0 = { 1, GenInst_LineType_t2260719266_0_0_0_Types };
static const RuntimeType* GenInst_InputType_t2301555234_0_0_0_Types[] = { (&InputType_t2301555234_0_0_0) };
extern const Il2CppGenericInst GenInst_InputType_t2301555234_0_0_0 = { 1, GenInst_InputType_t2301555234_0_0_0_Types };
static const RuntimeType* GenInst_TouchScreenKeyboardType_t3631231902_0_0_0_Types[] = { (&TouchScreenKeyboardType_t3631231902_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t3631231902_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t3631231902_0_0_0_Types };
static const RuntimeType* GenInst_CharacterValidation_t1479726893_0_0_0_Types[] = { (&CharacterValidation_t1479726893_0_0_0) };
extern const Il2CppGenericInst GenInst_CharacterValidation_t1479726893_0_0_0 = { 1, GenInst_CharacterValidation_t1479726893_0_0_0_Types };
static const RuntimeType* GenInst_Mask_t1584318481_0_0_0_Types[] = { (&Mask_t1584318481_0_0_0) };
extern const Il2CppGenericInst GenInst_Mask_t1584318481_0_0_0 = { 1, GenInst_Mask_t1584318481_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1653936345_0_0_0_Types[] = { (&List_1_t1653936345_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1653936345_0_0_0 = { 1, GenInst_List_1_t1653936345_0_0_0_Types };
static const RuntimeType* GenInst_RectMask2D_t3187556863_0_0_0_Types[] = { (&RectMask2D_t3187556863_0_0_0) };
extern const Il2CppGenericInst GenInst_RectMask2D_t3187556863_0_0_0 = { 1, GenInst_RectMask2D_t3187556863_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3257174727_0_0_0_Types[] = { (&List_1_t3257174727_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3257174727_0_0_0 = { 1, GenInst_List_1_t3257174727_0_0_0_Types };
static const RuntimeType* GenInst_Navigation_t643916199_0_0_0_Types[] = { (&Navigation_t643916199_0_0_0) };
extern const Il2CppGenericInst GenInst_Navigation_t643916199_0_0_0 = { 1, GenInst_Navigation_t643916199_0_0_0_Types };
static const RuntimeType* GenInst_IClippable_t3710278876_0_0_0_Types[] = { (&IClippable_t3710278876_0_0_0) };
extern const Il2CppGenericInst GenInst_IClippable_t3710278876_0_0_0 = { 1, GenInst_IClippable_t3710278876_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t1518645464_0_0_0_Types[] = { (&Direction_t1518645464_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t1518645464_0_0_0 = { 1, GenInst_Direction_t1518645464_0_0_0_Types };
static const RuntimeType* GenInst_Selectable_t3045992923_0_0_0_Types[] = { (&Selectable_t3045992923_0_0_0) };
extern const Il2CppGenericInst GenInst_Selectable_t3045992923_0_0_0 = { 1, GenInst_Selectable_t3045992923_0_0_0_Types };
static const RuntimeType* GenInst_Transition_t2847464031_0_0_0_Types[] = { (&Transition_t2847464031_0_0_0) };
extern const Il2CppGenericInst GenInst_Transition_t2847464031_0_0_0 = { 1, GenInst_Transition_t2847464031_0_0_0_Types };
static const RuntimeType* GenInst_SpriteState_t582609410_0_0_0_Types[] = { (&SpriteState_t582609410_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteState_t582609410_0_0_0 = { 1, GenInst_SpriteState_t582609410_0_0_0_Types };
static const RuntimeType* GenInst_CanvasGroup_t3466240198_0_0_0_Types[] = { (&CanvasGroup_t3466240198_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3466240198_0_0_0 = { 1, GenInst_CanvasGroup_t3466240198_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t1638109238_0_0_0_Types[] = { (&Direction_t1638109238_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t1638109238_0_0_0 = { 1, GenInst_Direction_t1638109238_0_0_0_Types };
static const RuntimeType* GenInst_MatEntry_t767401855_0_0_0_Types[] = { (&MatEntry_t767401855_0_0_0) };
extern const Il2CppGenericInst GenInst_MatEntry_t767401855_0_0_0 = { 1, GenInst_MatEntry_t767401855_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t510632154_0_0_0_Types[] = { (&Toggle_t510632154_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t510632154_0_0_0 = { 1, GenInst_Toggle_t510632154_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t510632154_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&Toggle_t510632154_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t510632154_0_0_0_Boolean_t988759797_0_0_0 = { 2, GenInst_Toggle_t510632154_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t1019235875_0_0_0_Types[] = { (&IClipper_t1019235875_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t1019235875_0_0_0 = { 1, GenInst_IClipper_t1019235875_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0_Types[] = { (&IClipper_t1019235875_0_0_0), (&Int32_t2803531614_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0 = { 2, GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&IClipper_t1019235875_0_0_0), (&Int32_t2803531614_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t4113814779_0_0_0_Types[] = { (&IClipper_t1019235875_0_0_0), (&Int32_t2803531614_0_0_0), (&KeyValuePair_2_t4113814779_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t4113814779_0_0_0 = { 3, GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t4113814779_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4113814779_0_0_0_Types[] = { (&KeyValuePair_2_t4113814779_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4113814779_0_0_0 = { 1, GenInst_KeyValuePair_2_t4113814779_0_0_0_Types };
static const RuntimeType* GenInst_AspectMode_t2018207616_0_0_0_Types[] = { (&AspectMode_t2018207616_0_0_0) };
extern const Il2CppGenericInst GenInst_AspectMode_t2018207616_0_0_0 = { 1, GenInst_AspectMode_t2018207616_0_0_0_Types };
static const RuntimeType* GenInst_FitMode_t1416694921_0_0_0_Types[] = { (&FitMode_t1416694921_0_0_0) };
extern const Il2CppGenericInst GenInst_FitMode_t1416694921_0_0_0 = { 1, GenInst_FitMode_t1416694921_0_0_0_Types };
static const RuntimeType* GenInst_RectTransform_t3890948561_0_0_0_Types[] = { (&RectTransform_t3890948561_0_0_0) };
extern const Il2CppGenericInst GenInst_RectTransform_t3890948561_0_0_0 = { 1, GenInst_RectTransform_t3890948561_0_0_0_Types };
static const RuntimeType* GenInst_LayoutRebuilder_t9522069_0_0_0_Types[] = { (&LayoutRebuilder_t9522069_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t9522069_0_0_0 = { 1, GenInst_LayoutRebuilder_t9522069_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t2893856145_0_0_0_Single_t473017394_0_0_0_Types[] = { (&ILayoutElement_t2893856145_0_0_0), (&Single_t473017394_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t2893856145_0_0_0_Single_t473017394_0_0_0 = { 2, GenInst_ILayoutElement_t2893856145_0_0_0_Single_t473017394_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Single_t473017394_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Single_t473017394_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Single_t473017394_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Single_t473017394_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t244535811_0_0_0_Types[] = { (&List_1_t244535811_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t244535811_0_0_0 = { 1, GenInst_List_1_t244535811_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t407959034_0_0_0_Types[] = { (&List_1_t407959034_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t407959034_0_0_0 = { 1, GenInst_List_1_t407959034_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2936728624_0_0_0_Types[] = { (&List_1_t2936728624_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2936728624_0_0_0 = { 1, GenInst_List_1_t2936728624_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t251382113_0_0_0_Types[] = { (&List_1_t251382113_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t251382113_0_0_0 = { 1, GenInst_List_1_t251382113_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2873149478_0_0_0_Types[] = { (&List_1_t2873149478_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2873149478_0_0_0 = { 1, GenInst_List_1_t2873149478_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2623970690_0_0_0_Types[] = { (&List_1_t2623970690_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2623970690_0_0_0 = { 1, GenInst_List_1_t2623970690_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2416028975_gp_0_0_0_0_Types[] = { (&IEnumerable_1_t2416028975_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2416028975_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t2416028975_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1287759456_gp_0_0_0_0_Types[] = { (&Array_InternalArray__IEnumerable_GetEnumerator_m1287759456_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1287759456_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1287759456_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2282796150_gp_0_0_0_0_Array_Sort_m2282796150_gp_0_0_0_0_Types[] = { (&Array_Sort_m2282796150_gp_0_0_0_0), (&Array_Sort_m2282796150_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2282796150_gp_0_0_0_0_Array_Sort_m2282796150_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2282796150_gp_0_0_0_0_Array_Sort_m2282796150_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3892888058_gp_0_0_0_0_Array_Sort_m3892888058_gp_1_0_0_0_Types[] = { (&Array_Sort_m3892888058_gp_0_0_0_0), (&Array_Sort_m3892888058_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3892888058_gp_0_0_0_0_Array_Sort_m3892888058_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3892888058_gp_0_0_0_0_Array_Sort_m3892888058_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m551987201_gp_0_0_0_0_Types[] = { (&Array_Sort_m551987201_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m551987201_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m551987201_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m551987201_gp_0_0_0_0_Array_Sort_m551987201_gp_0_0_0_0_Types[] = { (&Array_Sort_m551987201_gp_0_0_0_0), (&Array_Sort_m551987201_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m551987201_gp_0_0_0_0_Array_Sort_m551987201_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m551987201_gp_0_0_0_0_Array_Sort_m551987201_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1923995114_gp_0_0_0_0_Types[] = { (&Array_Sort_m1923995114_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1923995114_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1923995114_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1923995114_gp_0_0_0_0_Array_Sort_m1923995114_gp_1_0_0_0_Types[] = { (&Array_Sort_m1923995114_gp_0_0_0_0), (&Array_Sort_m1923995114_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1923995114_gp_0_0_0_0_Array_Sort_m1923995114_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1923995114_gp_0_0_0_0_Array_Sort_m1923995114_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2177995984_gp_0_0_0_0_Array_Sort_m2177995984_gp_0_0_0_0_Types[] = { (&Array_Sort_m2177995984_gp_0_0_0_0), (&Array_Sort_m2177995984_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2177995984_gp_0_0_0_0_Array_Sort_m2177995984_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2177995984_gp_0_0_0_0_Array_Sort_m2177995984_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1851492001_gp_0_0_0_0_Array_Sort_m1851492001_gp_1_0_0_0_Types[] = { (&Array_Sort_m1851492001_gp_0_0_0_0), (&Array_Sort_m1851492001_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1851492001_gp_0_0_0_0_Array_Sort_m1851492001_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1851492001_gp_0_0_0_0_Array_Sort_m1851492001_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m444933298_gp_0_0_0_0_Types[] = { (&Array_Sort_m444933298_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m444933298_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m444933298_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m444933298_gp_0_0_0_0_Array_Sort_m444933298_gp_0_0_0_0_Types[] = { (&Array_Sort_m444933298_gp_0_0_0_0), (&Array_Sort_m444933298_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m444933298_gp_0_0_0_0_Array_Sort_m444933298_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m444933298_gp_0_0_0_0_Array_Sort_m444933298_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2428568558_gp_0_0_0_0_Types[] = { (&Array_Sort_m2428568558_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2428568558_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2428568558_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2428568558_gp_1_0_0_0_Types[] = { (&Array_Sort_m2428568558_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2428568558_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m2428568558_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2428568558_gp_0_0_0_0_Array_Sort_m2428568558_gp_1_0_0_0_Types[] = { (&Array_Sort_m2428568558_gp_0_0_0_0), (&Array_Sort_m2428568558_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2428568558_gp_0_0_0_0_Array_Sort_m2428568558_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2428568558_gp_0_0_0_0_Array_Sort_m2428568558_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m211883471_gp_0_0_0_0_Types[] = { (&Array_Sort_m211883471_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m211883471_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m211883471_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2013146379_gp_0_0_0_0_Types[] = { (&Array_Sort_m2013146379_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2013146379_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2013146379_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m425782624_gp_0_0_0_0_Types[] = { (&Array_qsort_m425782624_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m425782624_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m425782624_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m425782624_gp_0_0_0_0_Array_qsort_m425782624_gp_1_0_0_0_Types[] = { (&Array_qsort_m425782624_gp_0_0_0_0), (&Array_qsort_m425782624_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m425782624_gp_0_0_0_0_Array_qsort_m425782624_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m425782624_gp_0_0_0_0_Array_qsort_m425782624_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_compare_m1097872614_gp_0_0_0_0_Types[] = { (&Array_compare_m1097872614_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_compare_m1097872614_gp_0_0_0_0 = { 1, GenInst_Array_compare_m1097872614_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m2192871689_gp_0_0_0_0_Types[] = { (&Array_qsort_m2192871689_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m2192871689_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m2192871689_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Resize_m3427076075_gp_0_0_0_0_Types[] = { (&Array_Resize_m3427076075_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Resize_m3427076075_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m3427076075_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_TrueForAll_m833996309_gp_0_0_0_0_Types[] = { (&Array_TrueForAll_m833996309_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m833996309_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m833996309_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ForEach_m3624317836_gp_0_0_0_0_Types[] = { (&Array_ForEach_m3624317836_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ForEach_m3624317836_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m3624317836_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ConvertAll_m941549552_gp_0_0_0_0_Array_ConvertAll_m941549552_gp_1_0_0_0_Types[] = { (&Array_ConvertAll_m941549552_gp_0_0_0_0), (&Array_ConvertAll_m941549552_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m941549552_gp_0_0_0_0_Array_ConvertAll_m941549552_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m941549552_gp_0_0_0_0_Array_ConvertAll_m941549552_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m1134336487_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m1134336487_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1134336487_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1134336487_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m3644051563_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m3644051563_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3644051563_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3644051563_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m2327043970_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m2327043970_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m2327043970_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m2327043970_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m2887368280_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m2887368280_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m2887368280_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m2887368280_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m3008983722_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m3008983722_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3008983722_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3008983722_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m464982704_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m464982704_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m464982704_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m464982704_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m1764795623_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m1764795623_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1764795623_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1764795623_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m890810051_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m890810051_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m890810051_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m890810051_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m2999506427_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m2999506427_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m2999506427_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m2999506427_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m612995514_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m612995514_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m612995514_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m612995514_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m127944267_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m127944267_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m127944267_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m127944267_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m4159481738_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m4159481738_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m4159481738_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m4159481738_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m1333370475_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m1333370475_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1333370475_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1333370475_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m2770575792_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m2770575792_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2770575792_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2770575792_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m2027197727_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m2027197727_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2027197727_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2027197727_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m696292085_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m696292085_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m696292085_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m696292085_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindAll_m3649758798_gp_0_0_0_0_Types[] = { (&Array_FindAll_m3649758798_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindAll_m3649758798_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m3649758798_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Exists_m3309978611_gp_0_0_0_0_Types[] = { (&Array_Exists_m3309978611_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Exists_m3309978611_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m3309978611_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_AsReadOnly_m2407970135_gp_0_0_0_0_Types[] = { (&Array_AsReadOnly_m2407970135_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m2407970135_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m2407970135_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Find_m15451854_gp_0_0_0_0_Types[] = { (&Array_Find_m15451854_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Find_m15451854_gp_0_0_0_0 = { 1, GenInst_Array_Find_m15451854_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLast_m2951830467_gp_0_0_0_0_Types[] = { (&Array_FindLast_m2951830467_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLast_m2951830467_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m2951830467_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InternalEnumerator_1_t4167398077_gp_0_0_0_0_Types[] = { (&InternalEnumerator_1_t4167398077_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t4167398077_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t4167398077_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ArrayReadOnlyList_1_t3192552300_gp_0_0_0_0_Types[] = { (&ArrayReadOnlyList_1_t3192552300_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3192552300_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3192552300_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3188906990_gp_0_0_0_0_Types[] = { (&U3CGetEnumeratorU3Ec__Iterator0_t3188906990_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3188906990_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3188906990_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t74343611_gp_0_0_0_0_Types[] = { (&IList_1_t74343611_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t74343611_gp_0_0_0_0 = { 1, GenInst_IList_1_t74343611_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1964006621_gp_0_0_0_0_Types[] = { (&ICollection_1_t1964006621_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1964006621_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1964006621_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Nullable_1_t2192590836_gp_0_0_0_0_Types[] = { (&Nullable_1_t2192590836_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Nullable_1_t2192590836_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t2192590836_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Comparer_1_t117739795_gp_0_0_0_0_Types[] = { (&Comparer_1_t117739795_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Comparer_1_t117739795_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t117739795_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t2087338667_gp_0_0_0_0_Types[] = { (&DefaultComparer_t2087338667_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t2087338667_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t2087338667_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericComparer_1_t295359490_gp_0_0_0_0_Types[] = { (&GenericComparer_1_t295359490_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t295359490_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t295359490_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Types[] = { (&Dictionary_2_t3672574211_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3672574211_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_Types[] = { (&Dictionary_2_t3672574211_gp_0_0_0_0), (&Dictionary_2_t3672574211_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3732153717_0_0_0_Types[] = { (&KeyValuePair_2_t3732153717_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3732153717_0_0_0 = { 1, GenInst_KeyValuePair_2_t3732153717_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1998764678_gp_0_0_0_0_Types[] = { (&Dictionary_2_t3672574211_gp_0_0_0_0), (&Dictionary_2_t3672574211_gp_1_0_0_0), (&Dictionary_2_Do_CopyTo_m1998764678_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1998764678_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1998764678_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0_Types[] = { (&Dictionary_2_t3672574211_gp_0_0_0_0), (&Dictionary_2_t3672574211_gp_1_0_0_0), (&Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&Dictionary_2_t3672574211_gp_0_0_0_0), (&Dictionary_2_t3672574211_gp_1_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 3, GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_ShimEnumerator_t1214271919_gp_0_0_0_0_ShimEnumerator_t1214271919_gp_1_0_0_0_Types[] = { (&ShimEnumerator_t1214271919_gp_0_0_0_0), (&ShimEnumerator_t1214271919_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t1214271919_gp_0_0_0_0_ShimEnumerator_t1214271919_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t1214271919_gp_0_0_0_0_ShimEnumerator_t1214271919_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1564904491_gp_0_0_0_0_Enumerator_t1564904491_gp_1_0_0_0_Types[] = { (&Enumerator_t1564904491_gp_0_0_0_0), (&Enumerator_t1564904491_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1564904491_gp_0_0_0_0_Enumerator_t1564904491_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1564904491_gp_0_0_0_0_Enumerator_t1564904491_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3466079189_0_0_0_Types[] = { (&KeyValuePair_2_t3466079189_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3466079189_0_0_0 = { 1, GenInst_KeyValuePair_2_t3466079189_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t3865265530_gp_0_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0_Types[] = { (&ValueCollection_t3865265530_gp_0_0_0_0), (&ValueCollection_t3865265530_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t3865265530_gp_0_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3865265530_gp_0_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t3865265530_gp_1_0_0_0_Types[] = { (&ValueCollection_t3865265530_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t3865265530_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t3865265530_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2660683481_gp_0_0_0_0_Enumerator_t2660683481_gp_1_0_0_0_Types[] = { (&Enumerator_t2660683481_gp_0_0_0_0), (&Enumerator_t2660683481_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2660683481_gp_0_0_0_0_Enumerator_t2660683481_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2660683481_gp_0_0_0_0_Enumerator_t2660683481_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2660683481_gp_1_0_0_0_Types[] = { (&Enumerator_t2660683481_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2660683481_gp_1_0_0_0 = { 1, GenInst_Enumerator_t2660683481_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t3865265530_gp_0_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0_Types[] = { (&ValueCollection_t3865265530_gp_0_0_0_0), (&ValueCollection_t3865265530_gp_1_0_0_0), (&ValueCollection_t3865265530_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t3865265530_gp_0_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t3865265530_gp_0_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t3865265530_gp_1_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0_Types[] = { (&ValueCollection_t3865265530_gp_1_0_0_0), (&ValueCollection_t3865265530_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t3865265530_gp_1_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3865265530_gp_1_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t2929293879_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types[] = { (&DictionaryEntry_t2929293879_0_0_0), (&DictionaryEntry_t2929293879_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t2929293879_0_0_0_DictionaryEntry_t2929293879_0_0_0 = { 2, GenInst_DictionaryEntry_t2929293879_0_0_0_DictionaryEntry_t2929293879_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_KeyValuePair_2_t3732153717_0_0_0_Types[] = { (&Dictionary_2_t3672574211_gp_0_0_0_0), (&Dictionary_2_t3672574211_gp_1_0_0_0), (&KeyValuePair_2_t3732153717_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_KeyValuePair_2_t3732153717_0_0_0 = { 3, GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_KeyValuePair_2_t3732153717_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3732153717_0_0_0_KeyValuePair_2_t3732153717_0_0_0_Types[] = { (&KeyValuePair_2_t3732153717_0_0_0), (&KeyValuePair_2_t3732153717_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3732153717_0_0_0_KeyValuePair_2_t3732153717_0_0_0 = { 2, GenInst_KeyValuePair_2_t3732153717_0_0_0_KeyValuePair_2_t3732153717_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3672574211_gp_1_0_0_0_Types[] = { (&Dictionary_2_t3672574211_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3672574211_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t3672574211_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_EqualityComparer_1_t1389874609_gp_0_0_0_0_Types[] = { (&EqualityComparer_1_t1389874609_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t1389874609_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t1389874609_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t1445765274_gp_0_0_0_0_Types[] = { (&DefaultComparer_t1445765274_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1445765274_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1445765274_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericEqualityComparer_1_t2215272400_gp_0_0_0_0_Types[] = { (&GenericEqualityComparer_1_t2215272400_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2215272400_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2215272400_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t171571701_0_0_0_Types[] = { (&KeyValuePair_2_t171571701_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t171571701_0_0_0 = { 1, GenInst_KeyValuePair_2_t171571701_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t3746618595_gp_0_0_0_0_IDictionary_2_t3746618595_gp_1_0_0_0_Types[] = { (&IDictionary_2_t3746618595_gp_0_0_0_0), (&IDictionary_2_t3746618595_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3746618595_gp_0_0_0_0_IDictionary_2_t3746618595_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3746618595_gp_0_0_0_0_IDictionary_2_t3746618595_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3524528471_gp_0_0_0_0_KeyValuePair_2_t3524528471_gp_1_0_0_0_Types[] = { (&KeyValuePair_2_t3524528471_gp_0_0_0_0), (&KeyValuePair_2_t3524528471_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3524528471_gp_0_0_0_0_KeyValuePair_2_t3524528471_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t3524528471_gp_0_0_0_0_KeyValuePair_2_t3524528471_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2110594299_gp_0_0_0_0_Types[] = { (&List_1_t2110594299_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2110594299_gp_0_0_0_0 = { 1, GenInst_List_1_t2110594299_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1180177267_gp_0_0_0_0_Types[] = { (&Enumerator_t1180177267_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1180177267_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1180177267_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Collection_1_t2075259255_gp_0_0_0_0_Types[] = { (&Collection_1_t2075259255_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Collection_1_t2075259255_gp_0_0_0_0 = { 1, GenInst_Collection_1_t2075259255_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ReadOnlyCollection_1_t4123029339_gp_0_0_0_0_Types[] = { (&ReadOnlyCollection_1_t4123029339_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t4123029339_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t4123029339_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_GetterAdapterFrame_m1924011743_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1924011743_gp_1_0_0_0_Types[] = { (&MonoProperty_GetterAdapterFrame_m1924011743_gp_0_0_0_0), (&MonoProperty_GetterAdapterFrame_m1924011743_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m1924011743_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1924011743_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m1924011743_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1924011743_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1418720114_gp_0_0_0_0_Types[] = { (&MonoProperty_StaticGetterAdapterFrame_m1418720114_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1418720114_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1418720114_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Queue_1_t3547065007_gp_0_0_0_0_Types[] = { (&Queue_1_t3547065007_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Queue_1_t3547065007_gp_0_0_0_0 = { 1, GenInst_Queue_1_t3547065007_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3106742487_gp_0_0_0_0_Types[] = { (&Enumerator_t3106742487_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3106742487_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3106742487_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Stack_1_t452188541_gp_0_0_0_0_Types[] = { (&Stack_1_t452188541_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Stack_1_t452188541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t452188541_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1170309493_gp_0_0_0_0_Types[] = { (&Enumerator_t1170309493_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1170309493_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1170309493_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t772885429_gp_0_0_0_0_Types[] = { (&HashSet_1_t772885429_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t772885429_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t772885429_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2406876998_gp_0_0_0_0_Types[] = { (&Enumerator_t2406876998_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2406876998_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2406876998_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PrimeHelper_t3750465633_gp_0_0_0_0_Types[] = { (&PrimeHelper_t3750465633_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3750465633_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3750465633_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Any_m41080623_gp_0_0_0_0_Types[] = { (&Enumerable_Any_m41080623_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m41080623_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m41080623_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m2792816853_gp_0_0_0_0_Types[] = { (&Enumerable_Where_m2792816853_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2792816853_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2792816853_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m2792816853_gp_0_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&Enumerable_Where_m2792816853_gp_0_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2792816853_gp_0_0_0_0_Boolean_t988759797_0_0_0 = { 2, GenInst_Enumerable_Where_m2792816853_gp_0_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0_Boolean_t988759797_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0_Boolean_t988759797_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentInChildren_m3597157616_gp_0_0_0_0_Types[] = { (&Component_GetComponentInChildren_m3597157616_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m3597157616_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m3597157616_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m3020516785_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m3020516785_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3020516785_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3020516785_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m1275101576_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m1275101576_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1275101576_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1275101576_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m328034652_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m328034652_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m328034652_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m328034652_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m1749718494_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m1749718494_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1749718494_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1749718494_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m3436671515_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m3436671515_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3436671515_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3436671515_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m11573404_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m11573404_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m11573404_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m11573404_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m2073010651_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m2073010651_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2073010651_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2073010651_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m2703880170_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m2703880170_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2703880170_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2703880170_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m2817529289_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m2817529289_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2817529289_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2817529289_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentInChildren_m130752911_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentInChildren_m130752911_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m130752911_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m130752911_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponents_m1760858099_gp_0_0_0_0_Types[] = { (&GameObject_GetComponents_m1760858099_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m1760858099_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m1760858099_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInChildren_m560504938_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInChildren_m560504938_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m560504938_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m560504938_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInParent_m1658428519_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInParent_m1658428519_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m1658428519_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m1658428519_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_GetAllocArrayFromChannel_m2206468965_gp_0_0_0_0_Types[] = { (&Mesh_GetAllocArrayFromChannel_m2206468965_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m2206468965_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m2206468965_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SafeLength_m4141435376_gp_0_0_0_0_Types[] = { (&Mesh_SafeLength_m4141435376_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m4141435376_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m4141435376_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m523604312_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m523604312_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m523604312_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m523604312_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m1158588740_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m1158588740_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m1158588740_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m1158588740_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetUvsImpl_m157937555_gp_0_0_0_0_Types[] = { (&Mesh_SetUvsImpl_m157937555_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m157937555_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m157937555_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Object_Instantiate_m2704180360_gp_0_0_0_0_Types[] = { (&Object_Instantiate_m2704180360_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m2704180360_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m2704180360_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Object_FindObjectsOfType_m21843941_gp_0_0_0_0_Types[] = { (&Object_FindObjectsOfType_m21843941_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m21843941_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m21843941_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Playable_IsPlayableOfType_m1014032790_gp_0_0_0_0_Types[] = { (&Playable_IsPlayableOfType_m1014032790_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_IsPlayableOfType_m1014032790_gp_0_0_0_0 = { 1, GenInst_Playable_IsPlayableOfType_m1014032790_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_IsPlayableOutputOfType_m2401824904_gp_0_0_0_0_Types[] = { (&PlayableOutput_IsPlayableOutputOfType_m2401824904_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_IsPlayableOutputOfType_m2401824904_gp_0_0_0_0 = { 1, GenInst_PlayableOutput_IsPlayableOutputOfType_m2401824904_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_1_t608438805_gp_0_0_0_0_Types[] = { (&InvokableCall_1_t608438805_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t608438805_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t608438805_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_1_t1170212876_0_0_0_Types[] = { (&UnityAction_1_t1170212876_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_1_t1170212876_0_0_0 = { 1, GenInst_UnityAction_1_t1170212876_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t3393839338_gp_0_0_0_0_InvokableCall_2_t3393839338_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t3393839338_gp_0_0_0_0), (&InvokableCall_2_t3393839338_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3393839338_gp_0_0_0_0_InvokableCall_2_t3393839338_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t3393839338_gp_0_0_0_0_InvokableCall_2_t3393839338_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t3393839338_gp_0_0_0_0_Types[] = { (&InvokableCall_2_t3393839338_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3393839338_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t3393839338_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t3393839338_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t3393839338_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3393839338_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t3393839338_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3121392964_gp_0_0_0_0_InvokableCall_3_t3121392964_gp_1_0_0_0_InvokableCall_3_t3121392964_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t3121392964_gp_0_0_0_0), (&InvokableCall_3_t3121392964_gp_1_0_0_0), (&InvokableCall_3_t3121392964_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3121392964_gp_0_0_0_0_InvokableCall_3_t3121392964_gp_1_0_0_0_InvokableCall_3_t3121392964_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3121392964_gp_0_0_0_0_InvokableCall_3_t3121392964_gp_1_0_0_0_InvokableCall_3_t3121392964_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3121392964_gp_0_0_0_0_Types[] = { (&InvokableCall_3_t3121392964_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3121392964_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3121392964_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3121392964_gp_1_0_0_0_Types[] = { (&InvokableCall_3_t3121392964_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3121392964_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3121392964_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3121392964_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t3121392964_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3121392964_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3121392964_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t320322180_gp_0_0_0_0_InvokableCall_4_t320322180_gp_1_0_0_0_InvokableCall_4_t320322180_gp_2_0_0_0_InvokableCall_4_t320322180_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t320322180_gp_0_0_0_0), (&InvokableCall_4_t320322180_gp_1_0_0_0), (&InvokableCall_4_t320322180_gp_2_0_0_0), (&InvokableCall_4_t320322180_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t320322180_gp_0_0_0_0_InvokableCall_4_t320322180_gp_1_0_0_0_InvokableCall_4_t320322180_gp_2_0_0_0_InvokableCall_4_t320322180_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t320322180_gp_0_0_0_0_InvokableCall_4_t320322180_gp_1_0_0_0_InvokableCall_4_t320322180_gp_2_0_0_0_InvokableCall_4_t320322180_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t320322180_gp_0_0_0_0_Types[] = { (&InvokableCall_4_t320322180_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t320322180_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t320322180_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t320322180_gp_1_0_0_0_Types[] = { (&InvokableCall_4_t320322180_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t320322180_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t320322180_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t320322180_gp_2_0_0_0_Types[] = { (&InvokableCall_4_t320322180_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t320322180_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t320322180_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t320322180_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t320322180_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t320322180_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t320322180_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_CachedInvokableCall_1_t2373840750_gp_0_0_0_0_Types[] = { (&CachedInvokableCall_1_t2373840750_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t2373840750_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t2373840750_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_1_t3654565707_gp_0_0_0_0_Types[] = { (&UnityEvent_1_t3654565707_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t3654565707_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t3654565707_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_2_t4148778253_gp_0_0_0_0_UnityEvent_2_t4148778253_gp_1_0_0_0_Types[] = { (&UnityEvent_2_t4148778253_gp_0_0_0_0), (&UnityEvent_2_t4148778253_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4148778253_gp_0_0_0_0_UnityEvent_2_t4148778253_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4148778253_gp_0_0_0_0_UnityEvent_2_t4148778253_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_3_t2171759262_gp_0_0_0_0_UnityEvent_3_t2171759262_gp_1_0_0_0_UnityEvent_3_t2171759262_gp_2_0_0_0_Types[] = { (&UnityEvent_3_t2171759262_gp_0_0_0_0), (&UnityEvent_3_t2171759262_gp_1_0_0_0), (&UnityEvent_3_t2171759262_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t2171759262_gp_0_0_0_0_UnityEvent_3_t2171759262_gp_1_0_0_0_UnityEvent_3_t2171759262_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t2171759262_gp_0_0_0_0_UnityEvent_3_t2171759262_gp_1_0_0_0_UnityEvent_3_t2171759262_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_4_t401569282_gp_0_0_0_0_UnityEvent_4_t401569282_gp_1_0_0_0_UnityEvent_4_t401569282_gp_2_0_0_0_UnityEvent_4_t401569282_gp_3_0_0_0_Types[] = { (&UnityEvent_4_t401569282_gp_0_0_0_0), (&UnityEvent_4_t401569282_gp_1_0_0_0), (&UnityEvent_4_t401569282_gp_2_0_0_0), (&UnityEvent_4_t401569282_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t401569282_gp_0_0_0_0_UnityEvent_4_t401569282_gp_1_0_0_0_UnityEvent_4_t401569282_gp_2_0_0_0_UnityEvent_4_t401569282_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t401569282_gp_0_0_0_0_UnityEvent_4_t401569282_gp_1_0_0_0_UnityEvent_4_t401569282_gp_2_0_0_0_UnityEvent_4_t401569282_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_Execute_m1461001727_gp_0_0_0_0_Types[] = { (&ExecuteEvents_Execute_m1461001727_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m1461001727_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m1461001727_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_ExecuteHierarchy_m3588049849_gp_0_0_0_0_Types[] = { (&ExecuteEvents_ExecuteHierarchy_m3588049849_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m3588049849_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m3588049849_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventList_m1070087876_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventList_m1070087876_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m1070087876_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m1070087876_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_CanHandleEvent_m2083532058_gp_0_0_0_0_Types[] = { (&ExecuteEvents_CanHandleEvent_m2083532058_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m2083532058_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m2083532058_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventHandler_m472693225_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventHandler_m472693225_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m472693225_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m472693225_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TweenRunner_1_t1959029912_gp_0_0_0_0_Types[] = { (&TweenRunner_1_t1959029912_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t1959029912_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t1959029912_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_GetOrAddComponent_m2960389033_gp_0_0_0_0_Types[] = { (&Dropdown_GetOrAddComponent_m2960389033_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m2960389033_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m2960389033_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SetPropertyUtility_SetStruct_m71196877_gp_0_0_0_0_Types[] = { (&SetPropertyUtility_SetStruct_m71196877_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m71196877_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m71196877_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t767473291_gp_0_0_0_0_Types[] = { (&IndexedSet_1_t767473291_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t767473291_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t767473291_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t767473291_gp_0_0_0_0_Int32_t2803531614_0_0_0_Types[] = { (&IndexedSet_1_t767473291_gp_0_0_0_0), (&Int32_t2803531614_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t767473291_gp_0_0_0_0_Int32_t2803531614_0_0_0 = { 2, GenInst_IndexedSet_1_t767473291_gp_0_0_0_0_Int32_t2803531614_0_0_0_Types };
static const RuntimeType* GenInst_ListPool_1_t306790258_gp_0_0_0_0_Types[] = { (&ListPool_1_t306790258_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ListPool_1_t306790258_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t306790258_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1024040571_0_0_0_Types[] = { (&List_1_t1024040571_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1024040571_0_0_0 = { 1, GenInst_List_1_t1024040571_0_0_0_Types };
static const RuntimeType* GenInst_ObjectPool_1_t712677698_gp_0_0_0_0_Types[] = { (&ObjectPool_1_t712677698_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t712677698_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t712677698_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPlayableOutput_t3060749678_0_0_0_Types[] = { (&AnimationPlayableOutput_t3060749678_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPlayableOutput_t3060749678_0_0_0 = { 1, GenInst_AnimationPlayableOutput_t3060749678_0_0_0_Types };
static const RuntimeType* GenInst_DefaultExecutionOrder_t585808243_0_0_0_Types[] = { (&DefaultExecutionOrder_t585808243_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t585808243_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t585808243_0_0_0_Types };
static const RuntimeType* GenInst_AudioPlayableOutput_t1627733019_0_0_0_Types[] = { (&AudioPlayableOutput_t1627733019_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioPlayableOutput_t1627733019_0_0_0 = { 1, GenInst_AudioPlayableOutput_t1627733019_0_0_0_Types };
static const RuntimeType* GenInst_PlayerConnection_t944997452_0_0_0_Types[] = { (&PlayerConnection_t944997452_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayerConnection_t944997452_0_0_0 = { 1, GenInst_PlayerConnection_t944997452_0_0_0_Types };
static const RuntimeType* GenInst_ScriptPlayableOutput_t4185099258_0_0_0_Types[] = { (&ScriptPlayableOutput_t4185099258_0_0_0) };
extern const Il2CppGenericInst GenInst_ScriptPlayableOutput_t4185099258_0_0_0 = { 1, GenInst_ScriptPlayableOutput_t4185099258_0_0_0_Types };
static const RuntimeType* GenInst_GUILayer_t3764231201_0_0_0_Types[] = { (&GUILayer_t3764231201_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayer_t3764231201_0_0_0 = { 1, GenInst_GUILayer_t3764231201_0_0_0_Types };
static const RuntimeType* GenInst_EventSystem_t2634658781_0_0_0_Types[] = { (&EventSystem_t2634658781_0_0_0) };
extern const Il2CppGenericInst GenInst_EventSystem_t2634658781_0_0_0 = { 1, GenInst_EventSystem_t2634658781_0_0_0_Types };
static const RuntimeType* GenInst_AxisEventData_t1013140673_0_0_0_Types[] = { (&AxisEventData_t1013140673_0_0_0) };
extern const Il2CppGenericInst GenInst_AxisEventData_t1013140673_0_0_0 = { 1, GenInst_AxisEventData_t1013140673_0_0_0_Types };
static const RuntimeType* GenInst_SpriteRenderer_t3490489848_0_0_0_Types[] = { (&SpriteRenderer_t3490489848_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t3490489848_0_0_0 = { 1, GenInst_SpriteRenderer_t3490489848_0_0_0_Types };
static const RuntimeType* GenInst_Image_t1612257916_0_0_0_Types[] = { (&Image_t1612257916_0_0_0) };
extern const Il2CppGenericInst GenInst_Image_t1612257916_0_0_0 = { 1, GenInst_Image_t1612257916_0_0_0_Types };
static const RuntimeType* GenInst_Button_t1637722950_0_0_0_Types[] = { (&Button_t1637722950_0_0_0) };
extern const Il2CppGenericInst GenInst_Button_t1637722950_0_0_0 = { 1, GenInst_Button_t1637722950_0_0_0_Types };
static const RuntimeType* GenInst_RawImage_t4039118384_0_0_0_Types[] = { (&RawImage_t4039118384_0_0_0) };
extern const Il2CppGenericInst GenInst_RawImage_t4039118384_0_0_0 = { 1, GenInst_RawImage_t4039118384_0_0_0_Types };
static const RuntimeType* GenInst_Slider_t42002792_0_0_0_Types[] = { (&Slider_t42002792_0_0_0) };
extern const Il2CppGenericInst GenInst_Slider_t42002792_0_0_0 = { 1, GenInst_Slider_t42002792_0_0_0_Types };
static const RuntimeType* GenInst_Scrollbar_t3263882543_0_0_0_Types[] = { (&Scrollbar_t3263882543_0_0_0) };
extern const Il2CppGenericInst GenInst_Scrollbar_t3263882543_0_0_0 = { 1, GenInst_Scrollbar_t3263882543_0_0_0_Types };
static const RuntimeType* GenInst_InputField_t621374856_0_0_0_Types[] = { (&InputField_t621374856_0_0_0) };
extern const Il2CppGenericInst GenInst_InputField_t621374856_0_0_0 = { 1, GenInst_InputField_t621374856_0_0_0_Types };
static const RuntimeType* GenInst_ScrollRect_t2248909708_0_0_0_Types[] = { (&ScrollRect_t2248909708_0_0_0) };
extern const Il2CppGenericInst GenInst_ScrollRect_t2248909708_0_0_0 = { 1, GenInst_ScrollRect_t2248909708_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_t2100269451_0_0_0_Types[] = { (&Dropdown_t2100269451_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_t2100269451_0_0_0 = { 1, GenInst_Dropdown_t2100269451_0_0_0_Types };
static const RuntimeType* GenInst_GraphicRaycaster_t3508845319_0_0_0_Types[] = { (&GraphicRaycaster_t3508845319_0_0_0) };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t3508845319_0_0_0 = { 1, GenInst_GraphicRaycaster_t3508845319_0_0_0_Types };
static const RuntimeType* GenInst_CanvasRenderer_t1499400789_0_0_0_Types[] = { (&CanvasRenderer_t1499400789_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t1499400789_0_0_0 = { 1, GenInst_CanvasRenderer_t1499400789_0_0_0_Types };
static const RuntimeType* GenInst_Corner_t141238654_0_0_0_Types[] = { (&Corner_t141238654_0_0_0) };
extern const Il2CppGenericInst GenInst_Corner_t141238654_0_0_0 = { 1, GenInst_Corner_t141238654_0_0_0_Types };
static const RuntimeType* GenInst_Axis_t2562381249_0_0_0_Types[] = { (&Axis_t2562381249_0_0_0) };
extern const Il2CppGenericInst GenInst_Axis_t2562381249_0_0_0 = { 1, GenInst_Axis_t2562381249_0_0_0_Types };
static const RuntimeType* GenInst_Constraint_t508081870_0_0_0_Types[] = { (&Constraint_t508081870_0_0_0) };
extern const Il2CppGenericInst GenInst_Constraint_t508081870_0_0_0 = { 1, GenInst_Constraint_t508081870_0_0_0_Types };
static const RuntimeType* GenInst_SubmitEvent_t527990981_0_0_0_Types[] = { (&SubmitEvent_t527990981_0_0_0) };
extern const Il2CppGenericInst GenInst_SubmitEvent_t527990981_0_0_0 = { 1, GenInst_SubmitEvent_t527990981_0_0_0_Types };
static const RuntimeType* GenInst_OnChangeEvent_t3634356784_0_0_0_Types[] = { (&OnChangeEvent_t3634356784_0_0_0) };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t3634356784_0_0_0 = { 1, GenInst_OnChangeEvent_t3634356784_0_0_0_Types };
static const RuntimeType* GenInst_OnValidateInput_t3553388536_0_0_0_Types[] = { (&OnValidateInput_t3553388536_0_0_0) };
extern const Il2CppGenericInst GenInst_OnValidateInput_t3553388536_0_0_0 = { 1, GenInst_OnValidateInput_t3553388536_0_0_0_Types };
static const RuntimeType* GenInst_LayoutElement_t1499649617_0_0_0_Types[] = { (&LayoutElement_t1499649617_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutElement_t1499649617_0_0_0 = { 1, GenInst_LayoutElement_t1499649617_0_0_0_Types };
static const RuntimeType* GenInst_RectOffset_t1388178548_0_0_0_Types[] = { (&RectOffset_t1388178548_0_0_0) };
extern const Il2CppGenericInst GenInst_RectOffset_t1388178548_0_0_0 = { 1, GenInst_RectOffset_t1388178548_0_0_0_Types };
static const RuntimeType* GenInst_TextAnchor_t54660852_0_0_0_Types[] = { (&TextAnchor_t54660852_0_0_0) };
extern const Il2CppGenericInst GenInst_TextAnchor_t54660852_0_0_0 = { 1, GenInst_TextAnchor_t54660852_0_0_0_Types };
static const RuntimeType* GenInst_AnimationTriggers_t1051497479_0_0_0_Types[] = { (&AnimationTriggers_t1051497479_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t1051497479_0_0_0 = { 1, GenInst_AnimationTriggers_t1051497479_0_0_0_Types };
static const RuntimeType* GenInst_Animator_t2124055305_0_0_0_Types[] = { (&Animator_t2124055305_0_0_0) };
extern const Il2CppGenericInst GenInst_Animator_t2124055305_0_0_0 = { 1, GenInst_Animator_t2124055305_0_0_0_Types };
static const RuntimeType* GenInst_Paddle_t4050334188_0_0_0_Types[] = { (&Paddle_t4050334188_0_0_0) };
extern const Il2CppGenericInst GenInst_Paddle_t4050334188_0_0_0 = { 1, GenInst_Paddle_t4050334188_0_0_0_Types };
static const RuntimeType* GenInst_LevelManager_t3966866889_0_0_0_Types[] = { (&LevelManager_t3966866889_0_0_0) };
extern const Il2CppGenericInst GenInst_LevelManager_t3966866889_0_0_0 = { 1, GenInst_LevelManager_t3966866889_0_0_0_Types };
static const RuntimeType* GenInst_Ball_t3103655791_0_0_0_Types[] = { (&Ball_t3103655791_0_0_0) };
extern const Il2CppGenericInst GenInst_Ball_t3103655791_0_0_0 = { 1, GenInst_Ball_t3103655791_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2803531614_0_0_0_Int32_t2803531614_0_0_0_Types[] = { (&Int32_t2803531614_0_0_0), (&Int32_t2803531614_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2803531614_0_0_0_Int32_t2803531614_0_0_0 = { 2, GenInst_Int32_t2803531614_0_0_0_Int32_t2803531614_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t2815983049_0_0_0_CustomAttributeNamedArgument_t2815983049_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t2815983049_0_0_0), (&CustomAttributeNamedArgument_t2815983049_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t2815983049_0_0_0_CustomAttributeNamedArgument_t2815983049_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t2815983049_0_0_0_CustomAttributeNamedArgument_t2815983049_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t722066768_0_0_0_CustomAttributeTypedArgument_t722066768_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t722066768_0_0_0), (&CustomAttributeTypedArgument_t722066768_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t722066768_0_0_0_CustomAttributeTypedArgument_t722066768_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t722066768_0_0_0_CustomAttributeTypedArgument_t722066768_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorClipInfo_t506398021_0_0_0_AnimatorClipInfo_t506398021_0_0_0_Types[] = { (&AnimatorClipInfo_t506398021_0_0_0), (&AnimatorClipInfo_t506398021_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t506398021_0_0_0_AnimatorClipInfo_t506398021_0_0_0 = { 2, GenInst_AnimatorClipInfo_t506398021_0_0_0_AnimatorClipInfo_t506398021_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t338341170_0_0_0_Color32_t338341170_0_0_0_Types[] = { (&Color32_t338341170_0_0_0), (&Color32_t338341170_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t338341170_0_0_0_Color32_t338341170_0_0_0 = { 2, GenInst_Color32_t338341170_0_0_0_Color32_t338341170_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t791495392_0_0_0_RaycastResult_t791495392_0_0_0_Types[] = { (&RaycastResult_t791495392_0_0_0), (&RaycastResult_t791495392_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t791495392_0_0_0_RaycastResult_t791495392_0_0_0 = { 2, GenInst_RaycastResult_t791495392_0_0_0_RaycastResult_t791495392_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t1978200295_0_0_0_UICharInfo_t1978200295_0_0_0_Types[] = { (&UICharInfo_t1978200295_0_0_0), (&UICharInfo_t1978200295_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t1978200295_0_0_0_UICharInfo_t1978200295_0_0_0 = { 2, GenInst_UICharInfo_t1978200295_0_0_0_UICharInfo_t1978200295_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t202048947_0_0_0_UILineInfo_t202048947_0_0_0_Types[] = { (&UILineInfo_t202048947_0_0_0), (&UILineInfo_t202048947_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t202048947_0_0_0_UILineInfo_t202048947_0_0_0 = { 2, GenInst_UILineInfo_t202048947_0_0_0_UILineInfo_t202048947_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t2554352826_0_0_0_UIVertex_t2554352826_0_0_0_Types[] = { (&UIVertex_t2554352826_0_0_0), (&UIVertex_t2554352826_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t2554352826_0_0_0_UIVertex_t2554352826_0_0_0 = { 2, GenInst_UIVertex_t2554352826_0_0_0_UIVertex_t2554352826_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t2867110760_0_0_0_Vector2_t2867110760_0_0_0_Types[] = { (&Vector2_t2867110760_0_0_0), (&Vector2_t2867110760_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t2867110760_0_0_0_Vector2_t2867110760_0_0_0 = { 2, GenInst_Vector2_t2867110760_0_0_0_Vector2_t2867110760_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t174917947_0_0_0_Vector3_t174917947_0_0_0_Types[] = { (&Vector3_t174917947_0_0_0), (&Vector3_t174917947_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t174917947_0_0_0_Vector3_t174917947_0_0_0 = { 2, GenInst_Vector3_t174917947_0_0_0_Vector3_t174917947_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t181764249_0_0_0_Vector4_t181764249_0_0_0_Types[] = { (&Vector4_t181764249_0_0_0), (&Vector4_t181764249_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t181764249_0_0_0_Vector4_t181764249_0_0_0 = { 2, GenInst_Vector4_t181764249_0_0_0_Vector4_t181764249_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1520347806_0_0_0_KeyValuePair_2_t1520347806_0_0_0_Types[] = { (&KeyValuePair_2_t1520347806_0_0_0), (&KeyValuePair_2_t1520347806_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1520347806_0_0_0_KeyValuePair_2_t1520347806_0_0_0 = { 2, GenInst_KeyValuePair_2_t1520347806_0_0_0_KeyValuePair_2_t1520347806_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1520347806_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1520347806_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1520347806_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1520347806_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t153286347_0_0_0_KeyValuePair_2_t153286347_0_0_0_Types[] = { (&KeyValuePair_2_t153286347_0_0_0), (&KeyValuePair_2_t153286347_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t153286347_0_0_0_KeyValuePair_2_t153286347_0_0_0 = { 2, GenInst_KeyValuePair_2_t153286347_0_0_0_KeyValuePair_2_t153286347_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t153286347_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t153286347_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t153286347_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t153286347_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t988759797_0_0_0_Boolean_t988759797_0_0_0_Types[] = { (&Boolean_t988759797_0_0_0), (&Boolean_t988759797_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t988759797_0_0_0_Boolean_t988759797_0_0_0 = { 2, GenInst_Boolean_t988759797_0_0_0_Boolean_t988759797_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1817943545_0_0_0_KeyValuePair_2_t1817943545_0_0_0_Types[] = { (&KeyValuePair_2_t1817943545_0_0_0), (&KeyValuePair_2_t1817943545_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1817943545_0_0_0_KeyValuePair_2_t1817943545_0_0_0 = { 2, GenInst_KeyValuePair_2_t1817943545_0_0_0_KeyValuePair_2_t1817943545_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1817943545_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1817943545_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1817943545_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1817943545_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3632715362_0_0_0_KeyValuePair_2_t3632715362_0_0_0_Types[] = { (&KeyValuePair_2_t3632715362_0_0_0), (&KeyValuePair_2_t3632715362_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3632715362_0_0_0_KeyValuePair_2_t3632715362_0_0_0 = { 2, GenInst_KeyValuePair_2_t3632715362_0_0_0_KeyValuePair_2_t3632715362_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3632715362_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3632715362_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3632715362_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3632715362_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2818848908_0_0_0_KeyValuePair_2_t2818848908_0_0_0_Types[] = { (&KeyValuePair_2_t2818848908_0_0_0), (&KeyValuePair_2_t2818848908_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2818848908_0_0_0_KeyValuePair_2_t2818848908_0_0_0 = { 2, GenInst_KeyValuePair_2_t2818848908_0_0_0_KeyValuePair_2_t2818848908_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2818848908_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2818848908_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2818848908_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2818848908_0_0_0_RuntimeObject_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[563] = 
{
	&GenInst_RuntimeObject_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0,
	&GenInst_Char_t1334203673_0_0_0,
	&GenInst_Int64_t338563217_0_0_0,
	&GenInst_UInt32_t413574147_0_0_0,
	&GenInst_UInt64_t921683320_0_0_0,
	&GenInst_Byte_t2822626181_0_0_0,
	&GenInst_SByte_t1604185109_0_0_0,
	&GenInst_Int16_t1178792569_0_0_0,
	&GenInst_UInt16_t2269804182_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t2316153507_0_0_0,
	&GenInst_IComparable_t353233170_0_0_0,
	&GenInst_IEnumerable_t863395423_0_0_0,
	&GenInst_ICloneable_t3398578658_0_0_0,
	&GenInst_IComparable_1_t2439511737_0_0_0,
	&GenInst_IEquatable_1_t1780045377_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t4232730689_0_0_0,
	&GenInst__Type_t2861529173_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t173947886_0_0_0,
	&GenInst__MemberInfo_t1846860436_0_0_0,
	&GenInst_Double_t2276348901_0_0_0,
	&GenInst_Single_t473017394_0_0_0,
	&GenInst_Decimal_t1717466512_0_0_0,
	&GenInst_Boolean_t988759797_0_0_0,
	&GenInst_Delegate_t3172540527_0_0_0,
	&GenInst_ISerializable_t1943426027_0_0_0,
	&GenInst_ParameterInfo_t3977181738_0_0_0,
	&GenInst__ParameterInfo_t4262047638_0_0_0,
	&GenInst_ParameterModifier_t2173190739_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t619352020_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t119590885_0_0_0,
	&GenInst_MethodBase_t3482457506_0_0_0,
	&GenInst__MethodBase_t3230368487_0_0_0,
	&GenInst_ConstructorInfo_t1090520116_0_0_0,
	&GenInst__ConstructorInfo_t2766353297_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t1615225767_0_0_0,
	&GenInst_TailoringInfo_t4005436652_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0,
	&GenInst_KeyValuePair_2_t3632715362_0_0_0,
	&GenInst_Link_t3811480030_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_Int32_t2803531614_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3632715362_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3357489110_0_0_0,
	&GenInst_KeyValuePair_2_t3357489110_0_0_0,
	&GenInst_Contraction_t691510939_0_0_0,
	&GenInst_Level2Map_t1316326550_0_0_0,
	&GenInst_BigInteger_t1041289570_0_0_0,
	&GenInst_KeySizes_t2289235637_0_0_0,
	&GenInst_KeyValuePair_2_t2818848908_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2818848908_0_0_0,
	&GenInst_Slot_t334802244_0_0_0,
	&GenInst_Slot_t3383151216_0_0_0,
	&GenInst_StackFrame_t1229074178_0_0_0,
	&GenInst_Calendar_t1129585511_0_0_0,
	&GenInst_ModuleBuilder_t3841773814_0_0_0,
	&GenInst__ModuleBuilder_t1161624706_0_0_0,
	&GenInst_Module_t2180181500_0_0_0,
	&GenInst__Module_t2704852488_0_0_0,
	&GenInst_ParameterBuilder_t3288171829_0_0_0,
	&GenInst__ParameterBuilder_t134282708_0_0_0,
	&GenInst_TypeU5BU5D_t60035734_0_0_0,
	&GenInst_RuntimeArray_0_0_0,
	&GenInst_ICollection_t2097114642_0_0_0,
	&GenInst_IList_t2084693791_0_0_0,
	&GenInst_IList_1_t2599697879_0_0_0,
	&GenInst_ICollection_1_t1936994217_0_0_0,
	&GenInst_IEnumerable_1_t4060944939_0_0_0,
	&GenInst_IList_1_t1761661641_0_0_0,
	&GenInst_ICollection_1_t1098957979_0_0_0,
	&GenInst_IEnumerable_1_t3222908701_0_0_0,
	&GenInst_IList_1_t390460125_0_0_0,
	&GenInst_ICollection_1_t4022723759_0_0_0,
	&GenInst_IEnumerable_1_t1851707185_0_0_0,
	&GenInst_IList_1_t3242441985_0_0_0,
	&GenInst_ICollection_1_t2579738323_0_0_0,
	&GenInst_IEnumerable_1_t408721749_0_0_0,
	&GenInst_IList_1_t1997846134_0_0_0,
	&GenInst_ICollection_1_t1335142472_0_0_0,
	&GenInst_IEnumerable_1_t3459093194_0_0_0,
	&GenInst_IList_1_t3670758684_0_0_0,
	&GenInst_ICollection_1_t3008055022_0_0_0,
	&GenInst_IEnumerable_1_t837038448_0_0_0,
	&GenInst_IList_1_t3813563408_0_0_0,
	&GenInst_ICollection_1_t3150859746_0_0_0,
	&GenInst_IEnumerable_1_t979843172_0_0_0,
	&GenInst_ILTokenInfo_t1872913708_0_0_0,
	&GenInst_LabelData_t1543367224_0_0_0,
	&GenInst_LabelFixup_t340982400_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t2722992877_0_0_0,
	&GenInst_TypeBuilder_t3421641907_0_0_0,
	&GenInst__TypeBuilder_t2650947519_0_0_0,
	&GenInst_MethodBuilder_t473061604_0_0_0,
	&GenInst__MethodBuilder_t1927917387_0_0_0,
	&GenInst_ConstructorBuilder_t2542337473_0_0_0,
	&GenInst__ConstructorBuilder_t812717629_0_0_0,
	&GenInst_FieldBuilder_t1185537865_0_0_0,
	&GenInst__FieldBuilder_t726804693_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t3021247535_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t722066768_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t2815983049_0_0_0,
	&GenInst_CustomAttributeData_t2504078644_0_0_0,
	&GenInst_ResourceInfo_t3425727170_0_0_0,
	&GenInst_ResourceCacheItem_t3158443302_0_0_0,
	&GenInst_IContextProperty_t3681610108_0_0_0,
	&GenInst_Header_t1031972396_0_0_0,
	&GenInst_ITrackingHandler_t3502059095_0_0_0,
	&GenInst_IContextAttribute_t1394016211_0_0_0,
	&GenInst_DateTime_t173522653_0_0_0,
	&GenInst_TimeSpan_t384326017_0_0_0,
	&GenInst_TypeTag_t3240722718_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2336798789_0_0_0,
	&GenInst_IBuiltInEvidence_t101773638_0_0_0,
	&GenInst_IIdentityPermissionFactory_t2821289363_0_0_0,
	&GenInst_DateTimeOffset_t2358022680_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t618301286_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_KeyValuePair_2_t1817943545_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t988759797_0_0_0_KeyValuePair_2_t1817943545_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t988759797_0_0_0_KeyValuePair_2_t1542717293_0_0_0,
	&GenInst_KeyValuePair_2_t1542717293_0_0_0,
	&GenInst_X509Certificate_t3436509184_0_0_0,
	&GenInst_IDeserializationCallback_t306402524_0_0_0,
	&GenInst_X509ChainStatus_t703314169_0_0_0,
	&GenInst_Capture_t2938944230_0_0_0,
	&GenInst_Group_t2651207689_0_0_0,
	&GenInst_Mark_t3695721384_0_0_0,
	&GenInst_UriScheme_t2057891215_0_0_0,
	&GenInst_BigInteger_t1041289571_0_0_0,
	&GenInst_ByteU5BU5D_t4256625864_0_0_0,
	&GenInst_IList_1_t351557133_0_0_0,
	&GenInst_ICollection_1_t3983820767_0_0_0,
	&GenInst_IEnumerable_1_t1812804193_0_0_0,
	&GenInst_ClientCertificateType_t1318574781_0_0_0,
	&GenInst_Link_t2244904633_0_0_0,
	&GenInst_Object_t2399819029_0_0_0,
	&GenInst_Camera_t4214422582_0_0_0,
	&GenInst_Behaviour_t1869462422_0_0_0,
	&GenInst_Component_t1209726582_0_0_0,
	&GenInst_Display_t964176615_0_0_0,
	&GenInst_Keyframe_t1034491164_0_0_0,
	&GenInst_Vector3_t174917947_0_0_0,
	&GenInst_Vector4_t181764249_0_0_0,
	&GenInst_Vector2_t2867110760_0_0_0,
	&GenInst_Color32_t338341170_0_0_0,
	&GenInst_Playable_t882738958_0_0_0,
	&GenInst_PlayableOutput_t2283729907_0_0_0,
	&GenInst_Scene_t1653993680_0_0_0_LoadSceneMode_t3425002508_0_0_0,
	&GenInst_Scene_t1653993680_0_0_0,
	&GenInst_Scene_t1653993680_0_0_0_Scene_t1653993680_0_0_0,
	&GenInst_SpriteAtlas_t2865821040_0_0_0,
	&GenInst_Rigidbody2D_t1937802487_0_0_0,
	&GenInst_RaycastHit2D_t2595119295_0_0_0,
	&GenInst_ContactPoint2D_t4070334835_0_0_0,
	&GenInst_AudioClipPlayable_t2750948543_0_0_0,
	&GenInst_AudioMixerPlayable_t2521349572_0_0_0,
	&GenInst_AnimationClipPlayable_t2199148327_0_0_0,
	&GenInst_AnimationLayerMixerPlayable_t3950440205_0_0_0,
	&GenInst_AnimationMixerPlayable_t635383928_0_0_0,
	&GenInst_AnimationOffsetPlayable_t1025003618_0_0_0,
	&GenInst_AnimatorControllerPlayable_t421975501_0_0_0,
	&GenInst_AnimatorClipInfo_t506398021_0_0_0,
	&GenInst_AnimatorControllerParameter_t923037798_0_0_0,
	&GenInst_UIVertex_t2554352826_0_0_0,
	&GenInst_UICharInfo_t1978200295_0_0_0,
	&GenInst_UILineInfo_t202048947_0_0_0,
	&GenInst_Font_t3423958016_0_0_0,
	&GenInst_GUILayoutOption_t1026689508_0_0_0,
	&GenInst_GUILayoutEntry_t3218050312_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1520347806_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1520347806_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_LayoutCache_t3250357553_0_0_0_KeyValuePair_2_t2781040199_0_0_0,
	&GenInst_KeyValuePair_2_t2781040199_0_0_0,
	&GenInst_GUIStyle_t236231435_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_KeyValuePair_2_t790188931_0_0_0,
	&GenInst_KeyValuePair_2_t790188931_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t236231435_0_0_0_GUIStyle_t236231435_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_IntPtr_t_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_Exception_t4239825407_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_UserProfile_t700863329_0_0_0,
	&GenInst_IUserProfile_t2763119316_0_0_0,
	&GenInst_Boolean_t988759797_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t988759797_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AchievementDescription_t1494495164_0_0_0,
	&GenInst_IAchievementDescription_t452714252_0_0_0,
	&GenInst_GcLeaderboard_t436677311_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t2589288261_0_0_0,
	&GenInst_IAchievementU5BU5D_t2444041797_0_0_0,
	&GenInst_IAchievement_t190526476_0_0_0,
	&GenInst_GcAchievementData_t3124305070_0_0_0,
	&GenInst_Achievement_t1395733712_0_0_0,
	&GenInst_IScoreU5BU5D_t3632149934_0_0_0,
	&GenInst_IScore_t1344207895_0_0_0,
	&GenInst_GcScoreData_t104171333_0_0_0,
	&GenInst_Score_t939050586_0_0_0,
	&GenInst_IUserProfileU5BU5D_t1403542365_0_0_0,
	&GenInst_DisallowMultipleComponent_t2116310945_0_0_0,
	&GenInst_Attribute_t3232323375_0_0_0,
	&GenInst__Attribute_t3061038810_0_0_0,
	&GenInst_ExecuteInEditMode_t2112836633_0_0_0,
	&GenInst_RequireComponent_t709491642_0_0_0,
	&GenInst_HitInfo_t808200858_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_PersistentCall_t1665637431_0_0_0,
	&GenInst_BaseInvokableCall_t185741398_0_0_0,
	&GenInst_WorkRequest_t1285289635_0_0_0,
	&GenInst_PlayableBinding_t198519643_0_0_0,
	&GenInst_MessageTypeSubscribers_t2455387773_0_0_0,
	&GenInst_MessageTypeSubscribers_t2455387773_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_MessageEventArgs_t1676961500_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t153286347_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t153286347_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t3337740345_0_0_0_KeyValuePair_2_t1501361532_0_0_0,
	&GenInst_KeyValuePair_2_t1501361532_0_0_0,
	&GenInst_BaseInputModule_t3534238441_0_0_0,
	&GenInst_RaycastResult_t791495392_0_0_0,
	&GenInst_IDeselectHandler_t1821424147_0_0_0,
	&GenInst_IEventSystemHandler_t550700969_0_0_0,
	&GenInst_List_1_t620318833_0_0_0,
	&GenInst_List_1_t2059283024_0_0_0,
	&GenInst_List_1_t1279344446_0_0_0,
	&GenInst_ISelectHandler_t3646584355_0_0_0,
	&GenInst_BaseRaycaster_t2834558618_0_0_0,
	&GenInst_Entry_t1721113290_0_0_0,
	&GenInst_BaseEventData_t2239799759_0_0_0,
	&GenInst_IPointerEnterHandler_t3359589364_0_0_0,
	&GenInst_IPointerExitHandler_t1194585081_0_0_0,
	&GenInst_IPointerDownHandler_t1934540052_0_0_0,
	&GenInst_IPointerUpHandler_t1595114040_0_0_0,
	&GenInst_IPointerClickHandler_t3898132831_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t133502056_0_0_0,
	&GenInst_IBeginDragHandler_t3066996802_0_0_0,
	&GenInst_IDragHandler_t1430837312_0_0_0,
	&GenInst_IEndDragHandler_t1502010433_0_0_0,
	&GenInst_IDropHandler_t4289830363_0_0_0,
	&GenInst_IScrollHandler_t2947048734_0_0_0,
	&GenInst_IUpdateSelectedHandler_t2533267811_0_0_0,
	&GenInst_IMoveHandler_t2306254927_0_0_0,
	&GenInst_ISubmitHandler_t1593443308_0_0_0,
	&GenInst_ICancelHandler_t1896074317_0_0_0,
	&GenInst_Transform_t2451218446_0_0_0,
	&GenInst_GameObject_t2629728357_0_0_0,
	&GenInst_BaseInput_t718558484_0_0_0,
	&GenInst_UIBehaviour_t4190033142_0_0_0,
	&GenInst_MonoBehaviour_t3163037868_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_KeyValuePair_2_t2429394435_0_0_0,
	&GenInst_KeyValuePair_2_t2429394435_0_0_0,
	&GenInst_PointerEventData_t2898711789_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_PointerEventData_t2898711789_0_0_0_PointerEventData_t2898711789_0_0_0,
	&GenInst_ButtonState_t4280795307_0_0_0,
	&GenInst_RaycastHit_t3326614521_0_0_0,
	&GenInst_Color_t1894673040_0_0_0,
	&GenInst_ICanvasElement_t3347193002_0_0_0,
	&GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0,
	&GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_ColorBlock_t3790099472_0_0_0,
	&GenInst_OptionData_t309155721_0_0_0,
	&GenInst_DropdownItem_t1339148254_0_0_0,
	&GenInst_FloatTween_t820617968_0_0_0,
	&GenInst_Sprite_t3431485579_0_0_0,
	&GenInst_Canvas_t2785269944_0_0_0,
	&GenInst_List_1_t2854887808_0_0_0,
	&GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0,
	&GenInst_Text_t4164675009_0_0_0,
	&GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_Font_t3423958016_0_0_0_HashSet_1_t1006781235_0_0_0_KeyValuePair_2_t800773855_0_0_0,
	&GenInst_KeyValuePair_2_t800773855_0_0_0,
	&GenInst_ColorTween_t3542451946_0_0_0,
	&GenInst_Graphic_t3593681171_0_0_0,
	&GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0,
	&GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0,
	&GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_Canvas_t2785269944_0_0_0_IndexedSet_1_t3015234795_0_0_0_KeyValuePair_2_t1842217471_0_0_0,
	&GenInst_KeyValuePair_2_t1842217471_0_0_0,
	&GenInst_Graphic_t3593681171_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t2710483019_0_0_0,
	&GenInst_KeyValuePair_2_t2710483019_0_0_0,
	&GenInst_ICanvasElement_t3347193002_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t3246846072_0_0_0,
	&GenInst_KeyValuePair_2_t3246846072_0_0_0,
	&GenInst_Type_t2268913483_0_0_0,
	&GenInst_FillMethod_t1193514586_0_0_0,
	&GenInst_ContentType_t2862244902_0_0_0,
	&GenInst_LineType_t2260719266_0_0_0,
	&GenInst_InputType_t2301555234_0_0_0,
	&GenInst_TouchScreenKeyboardType_t3631231902_0_0_0,
	&GenInst_CharacterValidation_t1479726893_0_0_0,
	&GenInst_Mask_t1584318481_0_0_0,
	&GenInst_List_1_t1653936345_0_0_0,
	&GenInst_RectMask2D_t3187556863_0_0_0,
	&GenInst_List_1_t3257174727_0_0_0,
	&GenInst_Navigation_t643916199_0_0_0,
	&GenInst_IClippable_t3710278876_0_0_0,
	&GenInst_Direction_t1518645464_0_0_0,
	&GenInst_Selectable_t3045992923_0_0_0,
	&GenInst_Transition_t2847464031_0_0_0,
	&GenInst_SpriteState_t582609410_0_0_0,
	&GenInst_CanvasGroup_t3466240198_0_0_0,
	&GenInst_Direction_t1638109238_0_0_0,
	&GenInst_MatEntry_t767401855_0_0_0,
	&GenInst_Toggle_t510632154_0_0_0,
	&GenInst_Toggle_t510632154_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_IClipper_t1019235875_0_0_0,
	&GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0,
	&GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_IClipper_t1019235875_0_0_0_Int32_t2803531614_0_0_0_KeyValuePair_2_t4113814779_0_0_0,
	&GenInst_KeyValuePair_2_t4113814779_0_0_0,
	&GenInst_AspectMode_t2018207616_0_0_0,
	&GenInst_FitMode_t1416694921_0_0_0,
	&GenInst_RectTransform_t3890948561_0_0_0,
	&GenInst_LayoutRebuilder_t9522069_0_0_0,
	&GenInst_ILayoutElement_t2893856145_0_0_0_Single_t473017394_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Single_t473017394_0_0_0,
	&GenInst_List_1_t244535811_0_0_0,
	&GenInst_List_1_t407959034_0_0_0,
	&GenInst_List_1_t2936728624_0_0_0,
	&GenInst_List_1_t251382113_0_0_0,
	&GenInst_List_1_t2873149478_0_0_0,
	&GenInst_List_1_t2623970690_0_0_0,
	&GenInst_IEnumerable_1_t2416028975_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m1287759456_gp_0_0_0_0,
	&GenInst_Array_Sort_m2282796150_gp_0_0_0_0_Array_Sort_m2282796150_gp_0_0_0_0,
	&GenInst_Array_Sort_m3892888058_gp_0_0_0_0_Array_Sort_m3892888058_gp_1_0_0_0,
	&GenInst_Array_Sort_m551987201_gp_0_0_0_0,
	&GenInst_Array_Sort_m551987201_gp_0_0_0_0_Array_Sort_m551987201_gp_0_0_0_0,
	&GenInst_Array_Sort_m1923995114_gp_0_0_0_0,
	&GenInst_Array_Sort_m1923995114_gp_0_0_0_0_Array_Sort_m1923995114_gp_1_0_0_0,
	&GenInst_Array_Sort_m2177995984_gp_0_0_0_0_Array_Sort_m2177995984_gp_0_0_0_0,
	&GenInst_Array_Sort_m1851492001_gp_0_0_0_0_Array_Sort_m1851492001_gp_1_0_0_0,
	&GenInst_Array_Sort_m444933298_gp_0_0_0_0,
	&GenInst_Array_Sort_m444933298_gp_0_0_0_0_Array_Sort_m444933298_gp_0_0_0_0,
	&GenInst_Array_Sort_m2428568558_gp_0_0_0_0,
	&GenInst_Array_Sort_m2428568558_gp_1_0_0_0,
	&GenInst_Array_Sort_m2428568558_gp_0_0_0_0_Array_Sort_m2428568558_gp_1_0_0_0,
	&GenInst_Array_Sort_m211883471_gp_0_0_0_0,
	&GenInst_Array_Sort_m2013146379_gp_0_0_0_0,
	&GenInst_Array_qsort_m425782624_gp_0_0_0_0,
	&GenInst_Array_qsort_m425782624_gp_0_0_0_0_Array_qsort_m425782624_gp_1_0_0_0,
	&GenInst_Array_compare_m1097872614_gp_0_0_0_0,
	&GenInst_Array_qsort_m2192871689_gp_0_0_0_0,
	&GenInst_Array_Resize_m3427076075_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m833996309_gp_0_0_0_0,
	&GenInst_Array_ForEach_m3624317836_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m941549552_gp_0_0_0_0_Array_ConvertAll_m941549552_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m1134336487_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3644051563_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m2327043970_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m2887368280_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3008983722_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m464982704_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1764795623_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m890810051_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m2999506427_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m612995514_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m127944267_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m4159481738_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1333370475_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2770575792_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2027197727_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m696292085_gp_0_0_0_0,
	&GenInst_Array_FindAll_m3649758798_gp_0_0_0_0,
	&GenInst_Array_Exists_m3309978611_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m2407970135_gp_0_0_0_0,
	&GenInst_Array_Find_m15451854_gp_0_0_0_0,
	&GenInst_Array_FindLast_m2951830467_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t4167398077_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3192552300_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3188906990_gp_0_0_0_0,
	&GenInst_IList_1_t74343611_gp_0_0_0_0,
	&GenInst_ICollection_1_t1964006621_gp_0_0_0_0,
	&GenInst_Nullable_1_t2192590836_gp_0_0_0_0,
	&GenInst_Comparer_1_t117739795_gp_0_0_0_0,
	&GenInst_DefaultComparer_t2087338667_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t295359490_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3672574211_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3732153717_0_0_0,
	&GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1998764678_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3784654591_gp_0_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_ShimEnumerator_t1214271919_gp_0_0_0_0_ShimEnumerator_t1214271919_gp_1_0_0_0,
	&GenInst_Enumerator_t1564904491_gp_0_0_0_0_Enumerator_t1564904491_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3466079189_0_0_0,
	&GenInst_ValueCollection_t3865265530_gp_0_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0,
	&GenInst_ValueCollection_t3865265530_gp_1_0_0_0,
	&GenInst_Enumerator_t2660683481_gp_0_0_0_0_Enumerator_t2660683481_gp_1_0_0_0,
	&GenInst_Enumerator_t2660683481_gp_1_0_0_0,
	&GenInst_ValueCollection_t3865265530_gp_0_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0,
	&GenInst_ValueCollection_t3865265530_gp_1_0_0_0_ValueCollection_t3865265530_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t2929293879_0_0_0_DictionaryEntry_t2929293879_0_0_0,
	&GenInst_Dictionary_2_t3672574211_gp_0_0_0_0_Dictionary_2_t3672574211_gp_1_0_0_0_KeyValuePair_2_t3732153717_0_0_0,
	&GenInst_KeyValuePair_2_t3732153717_0_0_0_KeyValuePair_2_t3732153717_0_0_0,
	&GenInst_Dictionary_2_t3672574211_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t1389874609_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1445765274_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2215272400_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t171571701_0_0_0,
	&GenInst_IDictionary_2_t3746618595_gp_0_0_0_0_IDictionary_2_t3746618595_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3524528471_gp_0_0_0_0_KeyValuePair_2_t3524528471_gp_1_0_0_0,
	&GenInst_List_1_t2110594299_gp_0_0_0_0,
	&GenInst_Enumerator_t1180177267_gp_0_0_0_0,
	&GenInst_Collection_1_t2075259255_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t4123029339_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m1924011743_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1924011743_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1418720114_gp_0_0_0_0,
	&GenInst_Queue_1_t3547065007_gp_0_0_0_0,
	&GenInst_Enumerator_t3106742487_gp_0_0_0_0,
	&GenInst_Stack_1_t452188541_gp_0_0_0_0,
	&GenInst_Enumerator_t1170309493_gp_0_0_0_0,
	&GenInst_HashSet_1_t772885429_gp_0_0_0_0,
	&GenInst_Enumerator_t2406876998_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3750465633_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m41080623_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2792816853_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2792816853_gp_0_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m1021596597_gp_0_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2336744521_gp_0_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_Component_GetComponentInChildren_m3597157616_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3020516785_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1275101576_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m328034652_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1749718494_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3436671515_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m11573404_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2073010651_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2703880170_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2817529289_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m130752911_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m1760858099_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m560504938_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m1658428519_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m2206468965_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m4141435376_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m523604312_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m1158588740_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m157937555_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m2704180360_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m21843941_gp_0_0_0_0,
	&GenInst_Playable_IsPlayableOfType_m1014032790_gp_0_0_0_0,
	&GenInst_PlayableOutput_IsPlayableOutputOfType_m2401824904_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t608438805_gp_0_0_0_0,
	&GenInst_UnityAction_1_t1170212876_0_0_0,
	&GenInst_InvokableCall_2_t3393839338_gp_0_0_0_0_InvokableCall_2_t3393839338_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t3393839338_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3393839338_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3121392964_gp_0_0_0_0_InvokableCall_3_t3121392964_gp_1_0_0_0_InvokableCall_3_t3121392964_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3121392964_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3121392964_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3121392964_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t320322180_gp_0_0_0_0_InvokableCall_4_t320322180_gp_1_0_0_0_InvokableCall_4_t320322180_gp_2_0_0_0_InvokableCall_4_t320322180_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t320322180_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t320322180_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t320322180_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t320322180_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t2373840750_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t3654565707_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4148778253_gp_0_0_0_0_UnityEvent_2_t4148778253_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t2171759262_gp_0_0_0_0_UnityEvent_3_t2171759262_gp_1_0_0_0_UnityEvent_3_t2171759262_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t401569282_gp_0_0_0_0_UnityEvent_4_t401569282_gp_1_0_0_0_UnityEvent_4_t401569282_gp_2_0_0_0_UnityEvent_4_t401569282_gp_3_0_0_0,
	&GenInst_ExecuteEvents_Execute_m1461001727_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m3588049849_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m1070087876_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m2083532058_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m472693225_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t1959029912_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m2960389033_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m71196877_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t767473291_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t767473291_gp_0_0_0_0_Int32_t2803531614_0_0_0,
	&GenInst_ListPool_1_t306790258_gp_0_0_0_0,
	&GenInst_List_1_t1024040571_0_0_0,
	&GenInst_ObjectPool_1_t712677698_gp_0_0_0_0,
	&GenInst_AnimationPlayableOutput_t3060749678_0_0_0,
	&GenInst_DefaultExecutionOrder_t585808243_0_0_0,
	&GenInst_AudioPlayableOutput_t1627733019_0_0_0,
	&GenInst_PlayerConnection_t944997452_0_0_0,
	&GenInst_ScriptPlayableOutput_t4185099258_0_0_0,
	&GenInst_GUILayer_t3764231201_0_0_0,
	&GenInst_EventSystem_t2634658781_0_0_0,
	&GenInst_AxisEventData_t1013140673_0_0_0,
	&GenInst_SpriteRenderer_t3490489848_0_0_0,
	&GenInst_Image_t1612257916_0_0_0,
	&GenInst_Button_t1637722950_0_0_0,
	&GenInst_RawImage_t4039118384_0_0_0,
	&GenInst_Slider_t42002792_0_0_0,
	&GenInst_Scrollbar_t3263882543_0_0_0,
	&GenInst_InputField_t621374856_0_0_0,
	&GenInst_ScrollRect_t2248909708_0_0_0,
	&GenInst_Dropdown_t2100269451_0_0_0,
	&GenInst_GraphicRaycaster_t3508845319_0_0_0,
	&GenInst_CanvasRenderer_t1499400789_0_0_0,
	&GenInst_Corner_t141238654_0_0_0,
	&GenInst_Axis_t2562381249_0_0_0,
	&GenInst_Constraint_t508081870_0_0_0,
	&GenInst_SubmitEvent_t527990981_0_0_0,
	&GenInst_OnChangeEvent_t3634356784_0_0_0,
	&GenInst_OnValidateInput_t3553388536_0_0_0,
	&GenInst_LayoutElement_t1499649617_0_0_0,
	&GenInst_RectOffset_t1388178548_0_0_0,
	&GenInst_TextAnchor_t54660852_0_0_0,
	&GenInst_AnimationTriggers_t1051497479_0_0_0,
	&GenInst_Animator_t2124055305_0_0_0,
	&GenInst_Paddle_t4050334188_0_0_0,
	&GenInst_LevelManager_t3966866889_0_0_0,
	&GenInst_Ball_t3103655791_0_0_0,
	&GenInst_Int32_t2803531614_0_0_0_Int32_t2803531614_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t2815983049_0_0_0_CustomAttributeNamedArgument_t2815983049_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t722066768_0_0_0_CustomAttributeTypedArgument_t722066768_0_0_0,
	&GenInst_AnimatorClipInfo_t506398021_0_0_0_AnimatorClipInfo_t506398021_0_0_0,
	&GenInst_Color32_t338341170_0_0_0_Color32_t338341170_0_0_0,
	&GenInst_RaycastResult_t791495392_0_0_0_RaycastResult_t791495392_0_0_0,
	&GenInst_UICharInfo_t1978200295_0_0_0_UICharInfo_t1978200295_0_0_0,
	&GenInst_UILineInfo_t202048947_0_0_0_UILineInfo_t202048947_0_0_0,
	&GenInst_UIVertex_t2554352826_0_0_0_UIVertex_t2554352826_0_0_0,
	&GenInst_Vector2_t2867110760_0_0_0_Vector2_t2867110760_0_0_0,
	&GenInst_Vector3_t174917947_0_0_0_Vector3_t174917947_0_0_0,
	&GenInst_Vector4_t181764249_0_0_0_Vector4_t181764249_0_0_0,
	&GenInst_KeyValuePair_2_t1520347806_0_0_0_KeyValuePair_2_t1520347806_0_0_0,
	&GenInst_KeyValuePair_2_t1520347806_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t153286347_0_0_0_KeyValuePair_2_t153286347_0_0_0,
	&GenInst_KeyValuePair_2_t153286347_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Boolean_t988759797_0_0_0_Boolean_t988759797_0_0_0,
	&GenInst_KeyValuePair_2_t1817943545_0_0_0_KeyValuePair_2_t1817943545_0_0_0,
	&GenInst_KeyValuePair_2_t1817943545_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3632715362_0_0_0_KeyValuePair_2_t3632715362_0_0_0,
	&GenInst_KeyValuePair_2_t3632715362_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2818848908_0_0_0_KeyValuePair_2_t2818848908_0_0_0,
	&GenInst_KeyValuePair_2_t2818848908_0_0_0_RuntimeObject_0_0_0,
};
