﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Ball
struct Ball_t3103655791;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3163037868;
// Paddle
struct Paddle_t4050334188;
// UnityEngine.Component
struct Component_t1209726582;
// UnityEngine.Transform
struct Transform_t2451218446;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1937802487;
// UnityEngine.Collision2D
struct Collision2D_t793992104;
// Brick
struct Brick_t2699191618;
// LevelManager
struct LevelManager_t3966866889;
// System.String
struct String_t;
// UnityEngine.AudioClip
struct AudioClip_t1857853486;
// UnityEngine.GameObject
struct GameObject_t2629728357;
// UnityEngine.Object
struct Object_t2399819029;
// UnityEngine.Sprite
struct Sprite_t3431485579;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3490489848;
// LooseCollider
struct LooseCollider_t1202412928;
// UnityEngine.Collider2D
struct Collider2D_t1625801269;
// MusicPlayer
struct MusicPlayer_t106117946;
// System.Char[]
struct CharU5BU5D_t3761151588;
// System.Void
struct Void_t1466654215;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t1069556578;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1756185772;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t3088484030;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t391306474;

extern RuntimeClass* Object_t2399819029_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t174917947_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisPaddle_t4050334188_m3653398745_RuntimeMethod_var;
extern const uint32_t Ball_Start_m2680331571_MetadataUsageId;
extern RuntimeClass* Input_t994511223_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody2D_t1937802487_m281710606_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2964999764;
extern const uint32_t Ball_Update_m3396477688_MetadataUsageId;
extern RuntimeClass* Vector2_t2867110760_il2cpp_TypeInfo_var;
extern const uint32_t Ball_OnCollisionEnter2D_m1747953246_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Brick_t2699191618_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisLevelManager_t3966866889_m1228751567_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2018735470;
extern const uint32_t Brick_Start_m1151123751_MetadataUsageId;
extern const uint32_t Brick_HandleHits_m988256236_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3490489848_m2023546735_RuntimeMethod_var;
extern const uint32_t Brick_LoadSprites_m2032612688_MetadataUsageId;
extern const uint32_t Brick__cctor_m3957544948_MetadataUsageId;
extern RuntimeClass* Debug_t4198707019_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral455862463;
extern const uint32_t LevelManager_LoadLevel_m3078441651_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1886205610;
extern const uint32_t LevelManager_QuitRequest_m3310784474_MetadataUsageId;
extern const uint32_t LevelManager_BrickDestroyed_m1089361922_MetadataUsageId;
extern const uint32_t LooseCollider_Start_m2253205700_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2998540065;
extern const uint32_t LooseCollider_OnCollisionEnter2D_m855342786_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3139056792;
extern Il2CppCodeGenString* _stringLiteral429903067;
extern const uint32_t LooseCollider_OnTriggerEnter2D_m3445383954_MetadataUsageId;
extern RuntimeClass* Int32_t2803531614_il2cpp_TypeInfo_var;
extern RuntimeClass* MusicPlayer_t106117946_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1649544885;
extern Il2CppCodeGenString* _stringLiteral3017841416;
extern const uint32_t MusicPlayer_Awake_m2605367453_MetadataUsageId;
extern const uint32_t MusicPlayer__cctor_m355724533_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisBall_t3103655791_m872147958_RuntimeMethod_var;
extern const uint32_t Paddle_Start_m191850713_MetadataUsageId;
extern RuntimeClass* Paddle_t4050334188_il2cpp_TypeInfo_var;
extern const uint32_t Paddle_Update_m1856140125_MetadataUsageId;
extern RuntimeClass* Mathf_t805486531_il2cpp_TypeInfo_var;
extern const uint32_t Paddle_MoveWithMouse_m4028705907_MetadataUsageId;
extern const uint32_t Paddle_MoveWithAccelerometer_m547288149_MetadataUsageId;
extern const uint32_t Paddle_Autoplay_m3657795759_MetadataUsageId;
extern const uint32_t Paddle__cctor_m672128667_MetadataUsageId;
struct ContactPoint2D_t4070334835 ;

struct SpriteU5BU5D_t391306474;


#ifndef U3CMODULEU3E_T509202523_H
#define U3CMODULEU3E_T509202523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t509202523 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T509202523_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T380474154_H
#define VALUETYPE_T380474154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t380474154  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t380474154_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t380474154_marshaled_com
{
};
#endif // VALUETYPE_T380474154_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3761151588* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3761151588* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3761151588** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3761151588* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T473017394_H
#define SINGLE_T473017394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t473017394 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t473017394, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T473017394_H
#ifndef INT32_T2803531614_H
#define INT32_T2803531614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2803531614 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2803531614, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2803531614_H
#ifndef VECTOR2_T2867110760_H
#define VECTOR2_T2867110760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2867110760 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2867110760, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2867110760, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2867110760_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2867110760  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2867110760  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2867110760  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2867110760  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2867110760  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2867110760  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2867110760  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2867110760  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2867110760_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2867110760  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2867110760 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2867110760  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2867110760_StaticFields, ___oneVector_3)); }
	inline Vector2_t2867110760  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2867110760 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2867110760  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2867110760_StaticFields, ___upVector_4)); }
	inline Vector2_t2867110760  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2867110760 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2867110760  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2867110760_StaticFields, ___downVector_5)); }
	inline Vector2_t2867110760  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2867110760 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2867110760  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2867110760_StaticFields, ___leftVector_6)); }
	inline Vector2_t2867110760  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2867110760 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2867110760  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2867110760_StaticFields, ___rightVector_7)); }
	inline Vector2_t2867110760  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2867110760 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2867110760  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2867110760_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2867110760  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2867110760 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2867110760  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2867110760_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2867110760  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2867110760 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2867110760  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2867110760_H
#ifndef ENUM_T256357748_H
#define ENUM_T256357748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t256357748  : public ValueType_t380474154
{
public:

public:
};

struct Enum_t256357748_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3761151588* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t256357748_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3761151588* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3761151588** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3761151588* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t256357748_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t256357748_marshaled_com
{
};
#endif // ENUM_T256357748_H
#ifndef VECTOR3_T174917947_H
#define VECTOR3_T174917947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t174917947 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t174917947, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t174917947, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t174917947, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t174917947_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t174917947  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t174917947  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t174917947  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t174917947  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t174917947  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t174917947  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t174917947  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t174917947  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t174917947  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t174917947  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t174917947_StaticFields, ___zeroVector_4)); }
	inline Vector3_t174917947  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t174917947 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t174917947  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t174917947_StaticFields, ___oneVector_5)); }
	inline Vector3_t174917947  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t174917947 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t174917947  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t174917947_StaticFields, ___upVector_6)); }
	inline Vector3_t174917947  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t174917947 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t174917947  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t174917947_StaticFields, ___downVector_7)); }
	inline Vector3_t174917947  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t174917947 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t174917947  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t174917947_StaticFields, ___leftVector_8)); }
	inline Vector3_t174917947  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t174917947 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t174917947  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t174917947_StaticFields, ___rightVector_9)); }
	inline Vector3_t174917947  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t174917947 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t174917947  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t174917947_StaticFields, ___forwardVector_10)); }
	inline Vector3_t174917947  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t174917947 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t174917947  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t174917947_StaticFields, ___backVector_11)); }
	inline Vector3_t174917947  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t174917947 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t174917947  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t174917947_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t174917947  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t174917947 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t174917947  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t174917947_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t174917947  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t174917947 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t174917947  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T174917947_H
#ifndef VOID_T1466654215_H
#define VOID_T1466654215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1466654215 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1466654215_H
#ifndef BOOLEAN_T988759797_H
#define BOOLEAN_T988759797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t988759797 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t988759797, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t988759797_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t988759797_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t988759797_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T988759797_H
#ifndef COLLISION2D_T793992104_H
#define COLLISION2D_T793992104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision2D
struct  Collision2D_t793992104  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Collision2D::m_Collider
	int32_t ___m_Collider_0;
	// System.Int32 UnityEngine.Collision2D::m_OtherCollider
	int32_t ___m_OtherCollider_1;
	// System.Int32 UnityEngine.Collision2D::m_Rigidbody
	int32_t ___m_Rigidbody_2;
	// System.Int32 UnityEngine.Collision2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_3;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_Contacts
	ContactPoint2DU5BU5D_t1069556578* ___m_Contacts_4;
	// UnityEngine.Vector2 UnityEngine.Collision2D::m_RelativeVelocity
	Vector2_t2867110760  ___m_RelativeVelocity_5;
	// System.Int32 UnityEngine.Collision2D::m_Enabled
	int32_t ___m_Enabled_6;

public:
	inline static int32_t get_offset_of_m_Collider_0() { return static_cast<int32_t>(offsetof(Collision2D_t793992104, ___m_Collider_0)); }
	inline int32_t get_m_Collider_0() const { return ___m_Collider_0; }
	inline int32_t* get_address_of_m_Collider_0() { return &___m_Collider_0; }
	inline void set_m_Collider_0(int32_t value)
	{
		___m_Collider_0 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_1() { return static_cast<int32_t>(offsetof(Collision2D_t793992104, ___m_OtherCollider_1)); }
	inline int32_t get_m_OtherCollider_1() const { return ___m_OtherCollider_1; }
	inline int32_t* get_address_of_m_OtherCollider_1() { return &___m_OtherCollider_1; }
	inline void set_m_OtherCollider_1(int32_t value)
	{
		___m_OtherCollider_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision2D_t793992104, ___m_Rigidbody_2)); }
	inline int32_t get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline int32_t* get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(int32_t value)
	{
		___m_Rigidbody_2 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_3() { return static_cast<int32_t>(offsetof(Collision2D_t793992104, ___m_OtherRigidbody_3)); }
	inline int32_t get_m_OtherRigidbody_3() const { return ___m_OtherRigidbody_3; }
	inline int32_t* get_address_of_m_OtherRigidbody_3() { return &___m_OtherRigidbody_3; }
	inline void set_m_OtherRigidbody_3(int32_t value)
	{
		___m_OtherRigidbody_3 = value;
	}

	inline static int32_t get_offset_of_m_Contacts_4() { return static_cast<int32_t>(offsetof(Collision2D_t793992104, ___m_Contacts_4)); }
	inline ContactPoint2DU5BU5D_t1069556578* get_m_Contacts_4() const { return ___m_Contacts_4; }
	inline ContactPoint2DU5BU5D_t1069556578** get_address_of_m_Contacts_4() { return &___m_Contacts_4; }
	inline void set_m_Contacts_4(ContactPoint2DU5BU5D_t1069556578* value)
	{
		___m_Contacts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Contacts_4), value);
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_5() { return static_cast<int32_t>(offsetof(Collision2D_t793992104, ___m_RelativeVelocity_5)); }
	inline Vector2_t2867110760  get_m_RelativeVelocity_5() const { return ___m_RelativeVelocity_5; }
	inline Vector2_t2867110760 * get_address_of_m_RelativeVelocity_5() { return &___m_RelativeVelocity_5; }
	inline void set_m_RelativeVelocity_5(Vector2_t2867110760  value)
	{
		___m_RelativeVelocity_5 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_6() { return static_cast<int32_t>(offsetof(Collision2D_t793992104, ___m_Enabled_6)); }
	inline int32_t get_m_Enabled_6() const { return ___m_Enabled_6; }
	inline int32_t* get_address_of_m_Enabled_6() { return &___m_Enabled_6; }
	inline void set_m_Enabled_6(int32_t value)
	{
		___m_Enabled_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision2D
struct Collision2D_t793992104_marshaled_pinvoke
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	ContactPoint2D_t4070334835 * ___m_Contacts_4;
	Vector2_t2867110760  ___m_RelativeVelocity_5;
	int32_t ___m_Enabled_6;
};
// Native definition for COM marshalling of UnityEngine.Collision2D
struct Collision2D_t793992104_marshaled_com
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	ContactPoint2D_t4070334835 * ___m_Contacts_4;
	Vector2_t2867110760  ___m_RelativeVelocity_5;
	int32_t ___m_Enabled_6;
};
#endif // COLLISION2D_T793992104_H
#ifndef TOUCHTYPE_T1585855840_H
#define TOUCHTYPE_T1585855840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t1585855840 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t1585855840, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T1585855840_H
#ifndef TOUCHPHASE_T1837085710_H
#define TOUCHPHASE_T1837085710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t1837085710 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t1837085710, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T1837085710_H
#ifndef OBJECT_T2399819029_H
#define OBJECT_T2399819029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t2399819029  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t2399819029, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t2399819029_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t2399819029_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t2399819029_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t2399819029_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T2399819029_H
#ifndef GAMEOBJECT_T2629728357_H
#define GAMEOBJECT_T2629728357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t2629728357  : public Object_t2399819029
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T2629728357_H
#ifndef AUDIOCLIP_T1857853486_H
#define AUDIOCLIP_T1857853486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip
struct  AudioClip_t1857853486  : public Object_t2399819029
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t1756185772 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t3088484030 * ___m_PCMSetPositionCallback_3;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_2() { return static_cast<int32_t>(offsetof(AudioClip_t1857853486, ___m_PCMReaderCallback_2)); }
	inline PCMReaderCallback_t1756185772 * get_m_PCMReaderCallback_2() const { return ___m_PCMReaderCallback_2; }
	inline PCMReaderCallback_t1756185772 ** get_address_of_m_PCMReaderCallback_2() { return &___m_PCMReaderCallback_2; }
	inline void set_m_PCMReaderCallback_2(PCMReaderCallback_t1756185772 * value)
	{
		___m_PCMReaderCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMReaderCallback_2), value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_3() { return static_cast<int32_t>(offsetof(AudioClip_t1857853486, ___m_PCMSetPositionCallback_3)); }
	inline PCMSetPositionCallback_t3088484030 * get_m_PCMSetPositionCallback_3() const { return ___m_PCMSetPositionCallback_3; }
	inline PCMSetPositionCallback_t3088484030 ** get_address_of_m_PCMSetPositionCallback_3() { return &___m_PCMSetPositionCallback_3; }
	inline void set_m_PCMSetPositionCallback_3(PCMSetPositionCallback_t3088484030 * value)
	{
		___m_PCMSetPositionCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMSetPositionCallback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIP_T1857853486_H
#ifndef SPRITE_T3431485579_H
#define SPRITE_T3431485579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t3431485579  : public Object_t2399819029
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T3431485579_H
#ifndef COMPONENT_T1209726582_H
#define COMPONENT_T1209726582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1209726582  : public Object_t2399819029
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1209726582_H
#ifndef TOUCH_T2565156694_H
#define TOUCH_T2565156694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t2565156694 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2867110760  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2867110760  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2867110760  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_Position_1)); }
	inline Vector2_t2867110760  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2867110760 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2867110760  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_RawPosition_2)); }
	inline Vector2_t2867110760  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2867110760 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2867110760  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_PositionDelta_3)); }
	inline Vector2_t2867110760  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2867110760 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2867110760  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t2565156694, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T2565156694_H
#ifndef RENDERER_T845195620_H
#define RENDERER_T845195620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t845195620  : public Component_t1209726582
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T845195620_H
#ifndef BEHAVIOUR_T1869462422_H
#define BEHAVIOUR_T1869462422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1869462422  : public Component_t1209726582
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1869462422_H
#ifndef TRANSFORM_T2451218446_H
#define TRANSFORM_T2451218446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t2451218446  : public Component_t1209726582
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T2451218446_H
#ifndef RIGIDBODY2D_T1937802487_H
#define RIGIDBODY2D_T1937802487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t1937802487  : public Component_t1209726582
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T1937802487_H
#ifndef COLLIDER2D_T1625801269_H
#define COLLIDER2D_T1625801269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t1625801269  : public Behaviour_t1869462422
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T1625801269_H
#ifndef SPRITERENDERER_T3490489848_H
#define SPRITERENDERER_T3490489848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3490489848  : public Renderer_t845195620
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3490489848_H
#ifndef MONOBEHAVIOUR_T3163037868_H
#define MONOBEHAVIOUR_T3163037868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3163037868  : public Behaviour_t1869462422
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3163037868_H
#ifndef MUSICPLAYER_T106117946_H
#define MUSICPLAYER_T106117946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MusicPlayer
struct  MusicPlayer_t106117946  : public MonoBehaviour_t3163037868
{
public:

public:
};

struct MusicPlayer_t106117946_StaticFields
{
public:
	// MusicPlayer MusicPlayer::instance
	MusicPlayer_t106117946 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(MusicPlayer_t106117946_StaticFields, ___instance_2)); }
	inline MusicPlayer_t106117946 * get_instance_2() const { return ___instance_2; }
	inline MusicPlayer_t106117946 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(MusicPlayer_t106117946 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUSICPLAYER_T106117946_H
#ifndef LEVELMANAGER_T3966866889_H
#define LEVELMANAGER_T3966866889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelManager
struct  LevelManager_t3966866889  : public MonoBehaviour_t3163037868
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELMANAGER_T3966866889_H
#ifndef BALL_T3103655791_H
#define BALL_T3103655791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball
struct  Ball_t3103655791  : public MonoBehaviour_t3163037868
{
public:
	// Paddle Ball::paddle
	Paddle_t4050334188 * ___paddle_2;
	// UnityEngine.Vector3 Ball::paddleToBallVector
	Vector3_t174917947  ___paddleToBallVector_3;
	// System.Boolean Ball::hasStarted
	bool ___hasStarted_4;

public:
	inline static int32_t get_offset_of_paddle_2() { return static_cast<int32_t>(offsetof(Ball_t3103655791, ___paddle_2)); }
	inline Paddle_t4050334188 * get_paddle_2() const { return ___paddle_2; }
	inline Paddle_t4050334188 ** get_address_of_paddle_2() { return &___paddle_2; }
	inline void set_paddle_2(Paddle_t4050334188 * value)
	{
		___paddle_2 = value;
		Il2CppCodeGenWriteBarrier((&___paddle_2), value);
	}

	inline static int32_t get_offset_of_paddleToBallVector_3() { return static_cast<int32_t>(offsetof(Ball_t3103655791, ___paddleToBallVector_3)); }
	inline Vector3_t174917947  get_paddleToBallVector_3() const { return ___paddleToBallVector_3; }
	inline Vector3_t174917947 * get_address_of_paddleToBallVector_3() { return &___paddleToBallVector_3; }
	inline void set_paddleToBallVector_3(Vector3_t174917947  value)
	{
		___paddleToBallVector_3 = value;
	}

	inline static int32_t get_offset_of_hasStarted_4() { return static_cast<int32_t>(offsetof(Ball_t3103655791, ___hasStarted_4)); }
	inline bool get_hasStarted_4() const { return ___hasStarted_4; }
	inline bool* get_address_of_hasStarted_4() { return &___hasStarted_4; }
	inline void set_hasStarted_4(bool value)
	{
		___hasStarted_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALL_T3103655791_H
#ifndef PADDLE_T4050334188_H
#define PADDLE_T4050334188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Paddle
struct  Paddle_t4050334188  : public MonoBehaviour_t3163037868
{
public:
	// System.Single Paddle::mousePosInBlocks
	float ___mousePosInBlocks_2;
	// Ball Paddle::ball
	Ball_t3103655791 * ___ball_4;

public:
	inline static int32_t get_offset_of_mousePosInBlocks_2() { return static_cast<int32_t>(offsetof(Paddle_t4050334188, ___mousePosInBlocks_2)); }
	inline float get_mousePosInBlocks_2() const { return ___mousePosInBlocks_2; }
	inline float* get_address_of_mousePosInBlocks_2() { return &___mousePosInBlocks_2; }
	inline void set_mousePosInBlocks_2(float value)
	{
		___mousePosInBlocks_2 = value;
	}

	inline static int32_t get_offset_of_ball_4() { return static_cast<int32_t>(offsetof(Paddle_t4050334188, ___ball_4)); }
	inline Ball_t3103655791 * get_ball_4() const { return ___ball_4; }
	inline Ball_t3103655791 ** get_address_of_ball_4() { return &___ball_4; }
	inline void set_ball_4(Ball_t3103655791 * value)
	{
		___ball_4 = value;
		Il2CppCodeGenWriteBarrier((&___ball_4), value);
	}
};

struct Paddle_t4050334188_StaticFields
{
public:
	// System.Boolean Paddle::autoplay
	bool ___autoplay_3;

public:
	inline static int32_t get_offset_of_autoplay_3() { return static_cast<int32_t>(offsetof(Paddle_t4050334188_StaticFields, ___autoplay_3)); }
	inline bool get_autoplay_3() const { return ___autoplay_3; }
	inline bool* get_address_of_autoplay_3() { return &___autoplay_3; }
	inline void set_autoplay_3(bool value)
	{
		___autoplay_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDLE_T4050334188_H
#ifndef LOOSECOLLIDER_T1202412928_H
#define LOOSECOLLIDER_T1202412928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LooseCollider
struct  LooseCollider_t1202412928  : public MonoBehaviour_t3163037868
{
public:
	// LevelManager LooseCollider::levelManager
	LevelManager_t3966866889 * ___levelManager_2;

public:
	inline static int32_t get_offset_of_levelManager_2() { return static_cast<int32_t>(offsetof(LooseCollider_t1202412928, ___levelManager_2)); }
	inline LevelManager_t3966866889 * get_levelManager_2() const { return ___levelManager_2; }
	inline LevelManager_t3966866889 ** get_address_of_levelManager_2() { return &___levelManager_2; }
	inline void set_levelManager_2(LevelManager_t3966866889 * value)
	{
		___levelManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelManager_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOSECOLLIDER_T1202412928_H
#ifndef BRICK_T2699191618_H
#define BRICK_T2699191618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Brick
struct  Brick_t2699191618  : public MonoBehaviour_t3163037868
{
public:
	// System.Int32 Brick::maxHits
	int32_t ___maxHits_2;
	// System.Int32 Brick::timesHits
	int32_t ___timesHits_3;
	// LevelManager Brick::levelmanager
	LevelManager_t3966866889 * ___levelmanager_4;
	// System.Boolean Brick::isBreakable
	bool ___isBreakable_5;
	// UnityEngine.Sprite[] Brick::hitSprites
	SpriteU5BU5D_t391306474* ___hitSprites_6;
	// UnityEngine.AudioClip Brick::crack
	AudioClip_t1857853486 * ___crack_8;

public:
	inline static int32_t get_offset_of_maxHits_2() { return static_cast<int32_t>(offsetof(Brick_t2699191618, ___maxHits_2)); }
	inline int32_t get_maxHits_2() const { return ___maxHits_2; }
	inline int32_t* get_address_of_maxHits_2() { return &___maxHits_2; }
	inline void set_maxHits_2(int32_t value)
	{
		___maxHits_2 = value;
	}

	inline static int32_t get_offset_of_timesHits_3() { return static_cast<int32_t>(offsetof(Brick_t2699191618, ___timesHits_3)); }
	inline int32_t get_timesHits_3() const { return ___timesHits_3; }
	inline int32_t* get_address_of_timesHits_3() { return &___timesHits_3; }
	inline void set_timesHits_3(int32_t value)
	{
		___timesHits_3 = value;
	}

	inline static int32_t get_offset_of_levelmanager_4() { return static_cast<int32_t>(offsetof(Brick_t2699191618, ___levelmanager_4)); }
	inline LevelManager_t3966866889 * get_levelmanager_4() const { return ___levelmanager_4; }
	inline LevelManager_t3966866889 ** get_address_of_levelmanager_4() { return &___levelmanager_4; }
	inline void set_levelmanager_4(LevelManager_t3966866889 * value)
	{
		___levelmanager_4 = value;
		Il2CppCodeGenWriteBarrier((&___levelmanager_4), value);
	}

	inline static int32_t get_offset_of_isBreakable_5() { return static_cast<int32_t>(offsetof(Brick_t2699191618, ___isBreakable_5)); }
	inline bool get_isBreakable_5() const { return ___isBreakable_5; }
	inline bool* get_address_of_isBreakable_5() { return &___isBreakable_5; }
	inline void set_isBreakable_5(bool value)
	{
		___isBreakable_5 = value;
	}

	inline static int32_t get_offset_of_hitSprites_6() { return static_cast<int32_t>(offsetof(Brick_t2699191618, ___hitSprites_6)); }
	inline SpriteU5BU5D_t391306474* get_hitSprites_6() const { return ___hitSprites_6; }
	inline SpriteU5BU5D_t391306474** get_address_of_hitSprites_6() { return &___hitSprites_6; }
	inline void set_hitSprites_6(SpriteU5BU5D_t391306474* value)
	{
		___hitSprites_6 = value;
		Il2CppCodeGenWriteBarrier((&___hitSprites_6), value);
	}

	inline static int32_t get_offset_of_crack_8() { return static_cast<int32_t>(offsetof(Brick_t2699191618, ___crack_8)); }
	inline AudioClip_t1857853486 * get_crack_8() const { return ___crack_8; }
	inline AudioClip_t1857853486 ** get_address_of_crack_8() { return &___crack_8; }
	inline void set_crack_8(AudioClip_t1857853486 * value)
	{
		___crack_8 = value;
		Il2CppCodeGenWriteBarrier((&___crack_8), value);
	}
};

struct Brick_t2699191618_StaticFields
{
public:
	// System.Int32 Brick::breakAbleCount
	int32_t ___breakAbleCount_7;

public:
	inline static int32_t get_offset_of_breakAbleCount_7() { return static_cast<int32_t>(offsetof(Brick_t2699191618_StaticFields, ___breakAbleCount_7)); }
	inline int32_t get_breakAbleCount_7() const { return ___breakAbleCount_7; }
	inline int32_t* get_address_of_breakAbleCount_7() { return &___breakAbleCount_7; }
	inline void set_breakAbleCount_7(int32_t value)
	{
		___breakAbleCount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRICK_T2699191618_H
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t391306474  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Sprite_t3431485579 * m_Items[1];

public:
	inline Sprite_t3431485579 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_t3431485579 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_t3431485579 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Sprite_t3431485579 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_t3431485579 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_t3431485579 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m2565303548_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m446886295_gshared (Component_t1209726582 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2116648366 (MonoBehaviour_t3163037868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<Paddle>()
#define Object_FindObjectOfType_TisPaddle_t4050334188_m3653398745(__this /* static, unused */, method) ((  Paddle_t4050334188 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2565303548_gshared)(__this /* static, unused */, method)
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t2451218446 * Component_get_transform_m1207608846 (Component_t1209726582 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t174917947  Transform_get_position_m1510843579 (Transform_t2451218446 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t174917947  Vector3_op_Subtraction_m81386658 (RuntimeObject * __this /* static, unused */, Vector3_t174917947  p0, Vector3_t174917947  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t174917947  Vector3_op_Addition_m2962925730 (RuntimeObject * __this /* static, unused */, Vector3_t174917947  p0, Vector3_t174917947  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m392221443 (Transform_t2451218446 * __this, Vector3_t174917947  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m2985809163 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C"  void MonoBehaviour_print_m3750541100 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t1937802487_m281710606(__this, method) ((  Rigidbody2D_t1937802487 * (*) (Component_t1209726582 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m446886295_gshared)(__this, method)
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3647646985 (Vector2_t2867110760 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m1410274315 (Rigidbody2D_t1937802487 * __this, Vector2_t2867110760  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2620753083 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C"  Vector2_t2867110760  Rigidbody2D_get_velocity_m2231795149 (Rigidbody2D_t1937802487 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2867110760  Vector2_op_Addition_m2468413754 (RuntimeObject * __this /* static, unused */, Vector2_t2867110760  p0, Vector2_t2867110760  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<LevelManager>()
#define Object_FindObjectOfType_TisLevelManager_t3966866889_m1228751567(__this /* static, unused */, method) ((  LevelManager_t3966866889 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2565303548_gshared)(__this /* static, unused */, method)
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m4284092406 (Component_t1209726582 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1796912180 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::PlayClipAtPoint(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern "C"  void AudioSource_PlayClipAtPoint_m250016131 (RuntimeObject * __this /* static, unused */, AudioClip_t1857853486 * p0, Vector3_t174917947  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Brick::HandleHits()
extern "C"  void Brick_HandleHits_m988256236 (Brick_t2699191618 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LevelManager::BrickDestroyed()
extern "C"  void LevelManager_BrickDestroyed_m1089361922 (LevelManager_t3966866889 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t2629728357 * Component_get_gameObject_m2600232034 (Component_t1209726582 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m3381369623 (RuntimeObject * __this /* static, unused */, Object_t2399819029 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Brick::LoadSprites()
extern "C"  void Brick_LoadSprites_m2032612688 (Brick_t2699191618 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LevelManager::LoadNextLevel()
extern "C"  void LevelManager_LoadNextLevel_m369139724 (LevelManager_t3966866889 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m935347368 (RuntimeObject * __this /* static, unused */, Object_t2399819029 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t3490489848_m2023546735(__this, method) ((  SpriteRenderer_t3490489848 * (*) (Component_t1209726582 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m446886295_gshared)(__this, method)
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_set_sprite_m2681530958 (SpriteRenderer_t3490489848 * __this, Sprite_t3431485579 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3962401956 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m1797711598 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::LoadLevel(System.String)
extern "C"  void Application_LoadLevel_m3355730539 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m931736579 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Application::get_loadedLevel()
extern "C"  int32_t Application_get_loadedLevel_m2125514012 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::LoadLevel(System.Int32)
extern "C"  void Application_LoadLevel_m1823350987 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LevelManager::LoadLevel(System.String)
extern "C"  void LevelManager_LoadLevel_m3078441651 (LevelManager_t3966866889 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m1089513739 (Object_t2399819029 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m2597834834 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m3435325212 (RuntimeObject * __this /* static, unused */, Object_t2399819029 * p0, Object_t2399819029 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m1520202240 (RuntimeObject * __this /* static, unused */, Object_t2399819029 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<Ball>()
#define Object_FindObjectOfType_TisBall_t3103655791_m872147958(__this /* static, unused */, method) ((  Ball_t3103655791 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2565303548_gshared)(__this /* static, unused */, method)
// System.Void Paddle::Autoplay()
extern "C"  void Paddle_Autoplay_m3657795759 (Paddle_t4050334188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Paddle::MoveWithAccelerometer()
extern "C"  void Paddle_MoveWithAccelerometer_m547288149 (Paddle_t4050334188 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3225092 (Vector3_t174917947 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m2004741560 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t2565156694  Input_GetTouch_m2851433137 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m2016633669 (Touch_t2565156694 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C"  Vector2_t2867110760  Touch_get_deltaPosition_m2557167563 (Touch_t2565156694 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_acceleration()
extern "C"  Vector3_t174917947  Input_get_acceleration_m38260823 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single)
extern "C"  void Transform_Translate_m3536088981 (Transform_t2451218446 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t174917947  Input_get_mousePosition_m780518479 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m2828759700 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Ball::.ctor()
extern "C"  void Ball__ctor_m3278299632 (Ball_t3103655791 * __this, const RuntimeMethod* method)
{
	{
		// private bool hasStarted=false;
		__this->set_hasStarted_4((bool)0);
		MonoBehaviour__ctor_m2116648366(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Ball::Start()
extern "C"  void Ball_Start_m2680331571 (Ball_t3103655791 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_Start_m2680331571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// paddle = GameObject.FindObjectOfType<Paddle>();
		// paddle = GameObject.FindObjectOfType<Paddle>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2399819029_il2cpp_TypeInfo_var);
		Paddle_t4050334188 * L_0 = Object_FindObjectOfType_TisPaddle_t4050334188_m3653398745(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisPaddle_t4050334188_m3653398745_RuntimeMethod_var);
		__this->set_paddle_2(L_0);
		// paddleToBallVector = this.transform.position - paddle.transform.position;
		// paddleToBallVector = this.transform.position - paddle.transform.position;
		Transform_t2451218446 * L_1 = Component_get_transform_m1207608846(__this, /*hidden argument*/NULL);
		// paddleToBallVector = this.transform.position - paddle.transform.position;
		NullCheck(L_1);
		Vector3_t174917947  L_2 = Transform_get_position_m1510843579(L_1, /*hidden argument*/NULL);
		Paddle_t4050334188 * L_3 = __this->get_paddle_2();
		// paddleToBallVector = this.transform.position - paddle.transform.position;
		NullCheck(L_3);
		Transform_t2451218446 * L_4 = Component_get_transform_m1207608846(L_3, /*hidden argument*/NULL);
		// paddleToBallVector = this.transform.position - paddle.transform.position;
		NullCheck(L_4);
		Vector3_t174917947  L_5 = Transform_get_position_m1510843579(L_4, /*hidden argument*/NULL);
		// paddleToBallVector = this.transform.position - paddle.transform.position;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t174917947_il2cpp_TypeInfo_var);
		Vector3_t174917947  L_6 = Vector3_op_Subtraction_m81386658(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		__this->set_paddleToBallVector_3(L_6);
		// }
		return;
	}
}
// System.Void Ball::Update()
extern "C"  void Ball_Update_m3396477688 (Ball_t3103655791 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_Update_m3396477688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(!hasStarted)
		bool L_0 = __this->get_hasStarted_4();
		if (L_0)
		{
			goto IL_006c;
		}
	}
	{
		// this.transform.position = paddle.transform.position + paddleToBallVector;
		// this.transform.position = paddle.transform.position + paddleToBallVector;
		Transform_t2451218446 * L_1 = Component_get_transform_m1207608846(__this, /*hidden argument*/NULL);
		Paddle_t4050334188 * L_2 = __this->get_paddle_2();
		// this.transform.position = paddle.transform.position + paddleToBallVector;
		NullCheck(L_2);
		Transform_t2451218446 * L_3 = Component_get_transform_m1207608846(L_2, /*hidden argument*/NULL);
		// this.transform.position = paddle.transform.position + paddleToBallVector;
		NullCheck(L_3);
		Vector3_t174917947  L_4 = Transform_get_position_m1510843579(L_3, /*hidden argument*/NULL);
		Vector3_t174917947  L_5 = __this->get_paddleToBallVector_3();
		// this.transform.position = paddle.transform.position + paddleToBallVector;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t174917947_il2cpp_TypeInfo_var);
		Vector3_t174917947  L_6 = Vector3_op_Addition_m2962925730(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		// this.transform.position = paddle.transform.position + paddleToBallVector;
		NullCheck(L_1);
		Transform_set_position_m392221443(L_1, L_6, /*hidden argument*/NULL);
		// if(Input.GetMouseButtonDown(0))
		// if(Input.GetMouseButtonDown(0))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t994511223_il2cpp_TypeInfo_var);
		bool L_7 = Input_GetMouseButtonDown_m2985809163(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006b;
		}
	}
	{
		// print ("Mouse Clicked!");
		// print ("Mouse Clicked!");
		MonoBehaviour_print_m3750541100(NULL /*static, unused*/, _stringLiteral2964999764, /*hidden argument*/NULL);
		// hasStarted = true;
		__this->set_hasStarted_4((bool)1);
		// GetComponent<Rigidbody2D>().velocity = new Vector2(2f,10f);
		// GetComponent<Rigidbody2D>().velocity = new Vector2(2f,10f);
		Rigidbody2D_t1937802487 * L_8 = Component_GetComponent_TisRigidbody2D_t1937802487_m281710606(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t1937802487_m281710606_RuntimeMethod_var);
		// GetComponent<Rigidbody2D>().velocity = new Vector2(2f,10f);
		Vector2_t2867110760  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector2__ctor_m3647646985((&L_9), (2.0f), (10.0f), /*hidden argument*/NULL);
		// GetComponent<Rigidbody2D>().velocity = new Vector2(2f,10f);
		NullCheck(L_8);
		Rigidbody2D_set_velocity_m1410274315(L_8, L_9, /*hidden argument*/NULL);
	}

IL_006b:
	{
	}

IL_006c:
	{
		// }
		return;
	}
}
// System.Void Ball::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void Ball_OnCollisionEnter2D_m1747953246 (Ball_t3103655791 * __this, Collision2D_t793992104 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_OnCollisionEnter2D_m1747953246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2867110760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// Vector2 tweak = new Vector2(Random.Range (0f,0.2f),Random.Range (0f,0.2f));
		// Vector2 tweak = new Vector2(Random.Range (0f,0.2f),Random.Range (0f,0.2f));
		float L_0 = Random_Range_m2620753083(NULL /*static, unused*/, (0.0f), (0.2f), /*hidden argument*/NULL);
		// Vector2 tweak = new Vector2(Random.Range (0f,0.2f),Random.Range (0f,0.2f));
		float L_1 = Random_Range_m2620753083(NULL /*static, unused*/, (0.0f), (0.2f), /*hidden argument*/NULL);
		// Vector2 tweak = new Vector2(Random.Range (0f,0.2f),Random.Range (0f,0.2f));
		Vector2__ctor_m3647646985((&V_0), L_0, L_1, /*hidden argument*/NULL);
		// if(hasStarted)
		bool L_2 = __this->get_hasStarted_4();
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		// GetComponent<Rigidbody2D>().velocity += tweak;
		// GetComponent<Rigidbody2D>().velocity += tweak;
		Rigidbody2D_t1937802487 * L_3 = Component_GetComponent_TisRigidbody2D_t1937802487_m281710606(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t1937802487_m281710606_RuntimeMethod_var);
		Rigidbody2D_t1937802487 * L_4 = L_3;
		// GetComponent<Rigidbody2D>().velocity += tweak;
		NullCheck(L_4);
		Vector2_t2867110760  L_5 = Rigidbody2D_get_velocity_m2231795149(L_4, /*hidden argument*/NULL);
		Vector2_t2867110760  L_6 = V_0;
		// GetComponent<Rigidbody2D>().velocity += tweak;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2867110760_il2cpp_TypeInfo_var);
		Vector2_t2867110760  L_7 = Vector2_op_Addition_m2468413754(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		// GetComponent<Rigidbody2D>().velocity += tweak;
		NullCheck(L_4);
		Rigidbody2D_set_velocity_m1410274315(L_4, L_7, /*hidden argument*/NULL);
	}

IL_004a:
	{
		// }
		return;
	}
}
// System.Void Brick::.ctor()
extern "C"  void Brick__ctor_m2170009358 (Brick_t2699191618 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2116648366(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Brick::Start()
extern "C"  void Brick_Start_m1151123751 (Brick_t2699191618 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Brick_Start_m1151123751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// timesHits = 0;
		__this->set_timesHits_3(0);
		// levelmanager = GameObject.FindObjectOfType<LevelManager>();
		// levelmanager = GameObject.FindObjectOfType<LevelManager>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2399819029_il2cpp_TypeInfo_var);
		LevelManager_t3966866889 * L_0 = Object_FindObjectOfType_TisLevelManager_t3966866889_m1228751567(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisLevelManager_t3966866889_m1228751567_RuntimeMethod_var);
		__this->set_levelmanager_4(L_0);
		// isBreakable = (this.tag == "Breakable");
		// isBreakable = (this.tag == "Breakable");
		String_t* L_1 = Component_get_tag_m4284092406(__this, /*hidden argument*/NULL);
		// isBreakable = (this.tag == "Breakable");
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1796912180(NULL /*static, unused*/, L_1, _stringLiteral2018735470, /*hidden argument*/NULL);
		__this->set_isBreakable_5(L_2);
		// if(isBreakable)
		bool L_3 = __this->get_isBreakable_5();
		if (!L_3)
		{
			goto IL_0042;
		}
	}
	{
		// breakAbleCount++;
		IL2CPP_RUNTIME_CLASS_INIT(Brick_t2699191618_il2cpp_TypeInfo_var);
		int32_t L_4 = ((Brick_t2699191618_StaticFields*)il2cpp_codegen_static_fields_for(Brick_t2699191618_il2cpp_TypeInfo_var))->get_breakAbleCount_7();
		((Brick_t2699191618_StaticFields*)il2cpp_codegen_static_fields_for(Brick_t2699191618_il2cpp_TypeInfo_var))->set_breakAbleCount_7(((int32_t)((int32_t)L_4+(int32_t)1)));
	}

IL_0042:
	{
		// }
		return;
	}
}
// System.Void Brick::Update()
extern "C"  void Brick_Update_m49490809 (Brick_t2699191618 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Brick::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void Brick_OnCollisionEnter2D_m2151670483 (Brick_t2699191618 * __this, Collision2D_t793992104 * ___collision0, const RuntimeMethod* method)
{
	{
		// AudioSource.PlayClipAtPoint(crack,transform.position,0.7f);
		AudioClip_t1857853486 * L_0 = __this->get_crack_8();
		// AudioSource.PlayClipAtPoint(crack,transform.position,0.7f);
		Transform_t2451218446 * L_1 = Component_get_transform_m1207608846(__this, /*hidden argument*/NULL);
		// AudioSource.PlayClipAtPoint(crack,transform.position,0.7f);
		NullCheck(L_1);
		Vector3_t174917947  L_2 = Transform_get_position_m1510843579(L_1, /*hidden argument*/NULL);
		// AudioSource.PlayClipAtPoint(crack,transform.position,0.7f);
		AudioSource_PlayClipAtPoint_m250016131(NULL /*static, unused*/, L_0, L_2, (0.7f), /*hidden argument*/NULL);
		// if(isBreakable)
		bool L_3 = __this->get_isBreakable_5();
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		// HandleHits ();
		// HandleHits ();
		Brick_HandleHits_m988256236(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Void Brick::HandleHits()
extern "C"  void Brick_HandleHits_m988256236 (Brick_t2699191618 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Brick_HandleHits_m988256236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// timesHits++;
		int32_t L_0 = __this->get_timesHits_3();
		__this->set_timesHits_3(((int32_t)((int32_t)L_0+(int32_t)1)));
		// maxHits = hitSprites.Length + 1;
		SpriteU5BU5D_t391306474* L_1 = __this->get_hitSprites_6();
		NullCheck(L_1);
		__this->set_maxHits_2(((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length))))+(int32_t)1)));
		// if(timesHits >= maxHits)
		int32_t L_2 = __this->get_timesHits_3();
		int32_t L_3 = __this->get_maxHits_2();
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_0059;
		}
	}
	{
		// breakAbleCount--;
		IL2CPP_RUNTIME_CLASS_INIT(Brick_t2699191618_il2cpp_TypeInfo_var);
		int32_t L_4 = ((Brick_t2699191618_StaticFields*)il2cpp_codegen_static_fields_for(Brick_t2699191618_il2cpp_TypeInfo_var))->get_breakAbleCount_7();
		((Brick_t2699191618_StaticFields*)il2cpp_codegen_static_fields_for(Brick_t2699191618_il2cpp_TypeInfo_var))->set_breakAbleCount_7(((int32_t)((int32_t)L_4-(int32_t)1)));
		// levelmanager.BrickDestroyed();
		LevelManager_t3966866889 * L_5 = __this->get_levelmanager_4();
		// levelmanager.BrickDestroyed();
		NullCheck(L_5);
		LevelManager_BrickDestroyed_m1089361922(L_5, /*hidden argument*/NULL);
		// Destroy(gameObject);
		// Destroy(gameObject);
		GameObject_t2629728357 * L_6 = Component_get_gameObject_m2600232034(__this, /*hidden argument*/NULL);
		// Destroy(gameObject);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2399819029_il2cpp_TypeInfo_var);
		Object_Destroy_m3381369623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_0061;
	}

IL_0059:
	{
		// LoadSprites();
		// LoadSprites();
		Brick_LoadSprites_m2032612688(__this, /*hidden argument*/NULL);
	}

IL_0061:
	{
		// }
		return;
	}
}
// System.Void Brick::SimulateWin()
extern "C"  void Brick_SimulateWin_m740050196 (Brick_t2699191618 * __this, const RuntimeMethod* method)
{
	{
		// levelmanager.LoadNextLevel();
		LevelManager_t3966866889 * L_0 = __this->get_levelmanager_4();
		// levelmanager.LoadNextLevel();
		NullCheck(L_0);
		LevelManager_LoadNextLevel_m369139724(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Brick::LoadSprites()
extern "C"  void Brick_LoadSprites_m2032612688 (Brick_t2699191618 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Brick_LoadSprites_m2032612688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// int spriteIndex = timesHits - 1;
		int32_t L_0 = __this->get_timesHits_3();
		V_0 = ((int32_t)((int32_t)L_0-(int32_t)1));
		// if(hitSprites[spriteIndex])
		SpriteU5BU5D_t391306474* L_1 = __this->get_hitSprites_6();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Sprite_t3431485579 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		// if(hitSprites[spriteIndex])
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2399819029_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m935347368(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		// this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
		// this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
		SpriteRenderer_t3490489848 * L_6 = Component_GetComponent_TisSpriteRenderer_t3490489848_m2023546735(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3490489848_m2023546735_RuntimeMethod_var);
		SpriteU5BU5D_t391306474* L_7 = __this->get_hitSprites_6();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Sprite_t3431485579 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		// this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
		NullCheck(L_6);
		SpriteRenderer_set_sprite_m2681530958(L_6, L_10, /*hidden argument*/NULL);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Void Brick::.cctor()
extern "C"  void Brick__cctor_m3957544948 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Brick__cctor_m3957544948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static int breakAbleCount = 0;
		((Brick_t2699191618_StaticFields*)il2cpp_codegen_static_fields_for(Brick_t2699191618_il2cpp_TypeInfo_var))->set_breakAbleCount_7(0);
		return;
	}
}
// System.Void LevelManager::.ctor()
extern "C"  void LevelManager__ctor_m2790620663 (LevelManager_t3966866889 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2116648366(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LevelManager::LoadLevel(System.String)
extern "C"  void LevelManager_LoadLevel_m3078441651 (LevelManager_t3966866889 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelManager_LoadLevel_m3078441651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log ("New Level load: " + name);
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3962401956(NULL /*static, unused*/, _stringLiteral455862463, L_0, /*hidden argument*/NULL);
		// Debug.Log ("New Level load: " + name);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4198707019_il2cpp_TypeInfo_var);
		Debug_Log_m1797711598(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// Application.LoadLevel (name);
		String_t* L_2 = ___name0;
		// Application.LoadLevel (name);
		Application_LoadLevel_m3355730539(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::QuitRequest()
extern "C"  void LevelManager_QuitRequest_m3310784474 (LevelManager_t3966866889 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelManager_QuitRequest_m3310784474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log ("Quit requested");
		// Debug.Log ("Quit requested");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4198707019_il2cpp_TypeInfo_var);
		Debug_Log_m1797711598(NULL /*static, unused*/, _stringLiteral1886205610, /*hidden argument*/NULL);
		// Application.Quit ();
		Application_Quit_m931736579(NULL /*static, unused*/, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::LoadNextLevel()
extern "C"  void LevelManager_LoadNextLevel_m369139724 (LevelManager_t3966866889 * __this, const RuntimeMethod* method)
{
	{
		// Application.LoadLevel(Application.loadedLevel + 1);
		int32_t L_0 = Application_get_loadedLevel_m2125514012(NULL /*static, unused*/, /*hidden argument*/NULL);
		// Application.LoadLevel(Application.loadedLevel + 1);
		Application_LoadLevel_m1823350987(NULL /*static, unused*/, ((int32_t)((int32_t)L_0+(int32_t)1)), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelManager::BrickDestroyed()
extern "C"  void LevelManager_BrickDestroyed_m1089361922 (LevelManager_t3966866889 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelManager_BrickDestroyed_m1089361922_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Brick.breakAbleCount <= 0)
		IL2CPP_RUNTIME_CLASS_INIT(Brick_t2699191618_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Brick_t2699191618_StaticFields*)il2cpp_codegen_static_fields_for(Brick_t2699191618_il2cpp_TypeInfo_var))->get_breakAbleCount_7();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		// LoadNextLevel();
		// LoadNextLevel();
		LevelManager_LoadNextLevel_m369139724(__this, /*hidden argument*/NULL);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void LooseCollider::.ctor()
extern "C"  void LooseCollider__ctor_m832984768 (LooseCollider_t1202412928 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2116648366(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LooseCollider::Start()
extern "C"  void LooseCollider_Start_m2253205700 (LooseCollider_t1202412928 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LooseCollider_Start_m2253205700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// levelManager = GameObject.FindObjectOfType<LevelManager>();
		// levelManager = GameObject.FindObjectOfType<LevelManager>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2399819029_il2cpp_TypeInfo_var);
		LevelManager_t3966866889 * L_0 = Object_FindObjectOfType_TisLevelManager_t3966866889_m1228751567(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisLevelManager_t3966866889_m1228751567_RuntimeMethod_var);
		__this->set_levelManager_2(L_0);
		// }
		return;
	}
}
// System.Void LooseCollider::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void LooseCollider_OnCollisionEnter2D_m855342786 (LooseCollider_t1202412928 * __this, Collision2D_t793992104 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LooseCollider_OnCollisionEnter2D_m855342786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print ("Collision");
		// print ("Collision");
		MonoBehaviour_print_m3750541100(NULL /*static, unused*/, _stringLiteral2998540065, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LooseCollider::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void LooseCollider_OnTriggerEnter2D_m3445383954 (LooseCollider_t1202412928 * __this, Collider2D_t1625801269 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LooseCollider_OnTriggerEnter2D_m3445383954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print ("Trigger");
		// print ("Trigger");
		MonoBehaviour_print_m3750541100(NULL /*static, unused*/, _stringLiteral3139056792, /*hidden argument*/NULL);
		// levelManager.LoadLevel("Lose Screen");
		LevelManager_t3966866889 * L_0 = __this->get_levelManager_2();
		// levelManager.LoadLevel("Lose Screen");
		NullCheck(L_0);
		LevelManager_LoadLevel_m3078441651(L_0, _stringLiteral429903067, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MusicPlayer::.ctor()
extern "C"  void MusicPlayer__ctor_m1577721769 (MusicPlayer_t106117946 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2116648366(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MusicPlayer::Awake()
extern "C"  void MusicPlayer_Awake_m2605367453 (MusicPlayer_t106117946 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MusicPlayer_Awake_m2605367453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Music player awake:"+GetInstanceID());
		// Debug.Log("Music player awake:"+GetInstanceID());
		int32_t L_0 = Object_GetInstanceID_m1089513739(__this, /*hidden argument*/NULL);
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_t2803531614_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2597834834(NULL /*static, unused*/, _stringLiteral1649544885, L_2, /*hidden argument*/NULL);
		// Debug.Log("Music player awake:"+GetInstanceID());
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4198707019_il2cpp_TypeInfo_var);
		Debug_Log_m1797711598(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// if(instance!=null)
		IL2CPP_RUNTIME_CLASS_INIT(MusicPlayer_t106117946_il2cpp_TypeInfo_var);
		MusicPlayer_t106117946 * L_4 = ((MusicPlayer_t106117946_StaticFields*)il2cpp_codegen_static_fields_for(MusicPlayer_t106117946_il2cpp_TypeInfo_var))->get_instance_2();
		// if(instance!=null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2399819029_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m3435325212(NULL /*static, unused*/, L_4, (Object_t2399819029 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0047;
		}
	}
	{
		// Destroy (gameObject);
		// Destroy (gameObject);
		GameObject_t2629728357 * L_6 = Component_get_gameObject_m2600232034(__this, /*hidden argument*/NULL);
		// Destroy (gameObject);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2399819029_il2cpp_TypeInfo_var);
		Object_Destroy_m3381369623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		// print ("Deplicate music player self destruction");
		// print ("Deplicate music player self destruction");
		MonoBehaviour_print_m3750541100(NULL /*static, unused*/, _stringLiteral3017841416, /*hidden argument*/NULL);
		goto IL_005a;
	}

IL_0047:
	{
		// instance = this;
		IL2CPP_RUNTIME_CLASS_INIT(MusicPlayer_t106117946_il2cpp_TypeInfo_var);
		((MusicPlayer_t106117946_StaticFields*)il2cpp_codegen_static_fields_for(MusicPlayer_t106117946_il2cpp_TypeInfo_var))->set_instance_2(__this);
		// GameObject.DontDestroyOnLoad(gameObject);
		// GameObject.DontDestroyOnLoad(gameObject);
		GameObject_t2629728357 * L_7 = Component_get_gameObject_m2600232034(__this, /*hidden argument*/NULL);
		// GameObject.DontDestroyOnLoad(gameObject);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2399819029_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m1520202240(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_005a:
	{
		// }
		return;
	}
}
// System.Void MusicPlayer::.cctor()
extern "C"  void MusicPlayer__cctor_m355724533 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MusicPlayer__cctor_m355724533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static MusicPlayer instance = null;
		((MusicPlayer_t106117946_StaticFields*)il2cpp_codegen_static_fields_for(MusicPlayer_t106117946_il2cpp_TypeInfo_var))->set_instance_2((MusicPlayer_t106117946 *)NULL);
		return;
	}
}
// System.Void Paddle::.ctor()
extern "C"  void Paddle__ctor_m4272994362 (Paddle_t4050334188 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2116648366(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Paddle::Start()
extern "C"  void Paddle_Start_m191850713 (Paddle_t4050334188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Paddle_Start_m191850713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ball = Object.FindObjectOfType<Ball>();
		// ball = Object.FindObjectOfType<Ball>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t2399819029_il2cpp_TypeInfo_var);
		Ball_t3103655791 * L_0 = Object_FindObjectOfType_TisBall_t3103655791_m872147958(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisBall_t3103655791_m872147958_RuntimeMethod_var);
		__this->set_ball_4(L_0);
		// }
		return;
	}
}
// System.Void Paddle::Update()
extern "C"  void Paddle_Update_m1856140125 (Paddle_t4050334188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Paddle_Update_m1856140125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(autoplay)
		IL2CPP_RUNTIME_CLASS_INIT(Paddle_t4050334188_il2cpp_TypeInfo_var);
		bool L_0 = ((Paddle_t4050334188_StaticFields*)il2cpp_codegen_static_fields_for(Paddle_t4050334188_il2cpp_TypeInfo_var))->get_autoplay_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		// Autoplay();
		// Autoplay();
		Paddle_Autoplay_m3657795759(__this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		// MoveWithAccelerometer();
		// MoveWithAccelerometer();
		Paddle_MoveWithAccelerometer_m547288149(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		// }
		return;
	}
}
// System.Void Paddle::MoveWithMouse()
extern "C"  void Paddle_MoveWithMouse_m4028705907 (Paddle_t4050334188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Paddle_MoveWithMouse_m4028705907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t174917947  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t174917947  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t174917947  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Touch_t2565156694  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2867110760  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Touch_t2565156694  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t174917947  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t174917947  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		// Vector3 paddlePos = new Vector3(0.5f,this.transform.position.y,0f);
		// Vector3 paddlePos = new Vector3(0.5f,this.transform.position.y,0f);
		Transform_t2451218446 * L_0 = Component_get_transform_m1207608846(__this, /*hidden argument*/NULL);
		// Vector3 paddlePos = new Vector3(0.5f,this.transform.position.y,0f);
		NullCheck(L_0);
		Vector3_t174917947  L_1 = Transform_get_position_m1510843579(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_y_2();
		// Vector3 paddlePos = new Vector3(0.5f,this.transform.position.y,0f);
		Vector3__ctor_m3225092((&V_0), (0.5f), L_2, (0.0f), /*hidden argument*/NULL);
		// Vector3 ballPos = ball.transform.position;
		Ball_t3103655791 * L_3 = __this->get_ball_4();
		// Vector3 ballPos = ball.transform.position;
		NullCheck(L_3);
		Transform_t2451218446 * L_4 = Component_get_transform_m1207608846(L_3, /*hidden argument*/NULL);
		// Vector3 ballPos = ball.transform.position;
		NullCheck(L_4);
		Vector3_t174917947  L_5 = Transform_get_position_m1510843579(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		// paddlePos.x = Mathf.Clamp(ballPos.x,0.5f,15.5f);
		float L_6 = (&V_2)->get_x_1();
		// paddlePos.x = Mathf.Clamp(ballPos.x,0.5f,15.5f);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t805486531_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Clamp_m2004741560(NULL /*static, unused*/, L_6, (0.5f), (15.5f), /*hidden argument*/NULL);
		(&V_0)->set_x_1(L_7);
		// this.transform.position = paddlePos;
		// this.transform.position = paddlePos;
		Transform_t2451218446 * L_8 = Component_get_transform_m1207608846(__this, /*hidden argument*/NULL);
		Vector3_t174917947  L_9 = V_0;
		// this.transform.position = paddlePos;
		NullCheck(L_8);
		Transform_set_position_m392221443(L_8, L_9, /*hidden argument*/NULL);
		// if(Input.GetTouch(0).phase == TouchPhase.Moved)
		// if(Input.GetTouch(0).phase == TouchPhase.Moved)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t994511223_il2cpp_TypeInfo_var);
		Touch_t2565156694  L_10 = Input_GetTouch_m2851433137(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_3 = L_10;
		// if(Input.GetTouch(0).phase == TouchPhase.Moved)
		int32_t L_11 = Touch_get_phase_m2016633669((&V_3), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)1))))
		{
			goto IL_00ba;
		}
	}
	{
		// Vector2 TouchDeltaPosition = Input.GetTouch(0).deltaPosition;
		// Vector2 TouchDeltaPosition = Input.GetTouch(0).deltaPosition;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t994511223_il2cpp_TypeInfo_var);
		Touch_t2565156694  L_12 = Input_GetTouch_m2851433137(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_5 = L_12;
		// Vector2 TouchDeltaPosition = Input.GetTouch(0).deltaPosition;
		Vector2_t2867110760  L_13 = Touch_get_deltaPosition_m2557167563((&V_5), /*hidden argument*/NULL);
		V_4 = L_13;
		// Vector3 paddlePosition = new Vector3(TouchDeltaPosition.x,this.transform.position.y,0f);
		float L_14 = (&V_4)->get_x_0();
		// Vector3 paddlePosition = new Vector3(TouchDeltaPosition.x,this.transform.position.y,0f);
		Transform_t2451218446 * L_15 = Component_get_transform_m1207608846(__this, /*hidden argument*/NULL);
		// Vector3 paddlePosition = new Vector3(TouchDeltaPosition.x,this.transform.position.y,0f);
		NullCheck(L_15);
		Vector3_t174917947  L_16 = Transform_get_position_m1510843579(L_15, /*hidden argument*/NULL);
		V_7 = L_16;
		float L_17 = (&V_7)->get_y_2();
		// Vector3 paddlePosition = new Vector3(TouchDeltaPosition.x,this.transform.position.y,0f);
		Vector3__ctor_m3225092((&V_6), L_14, L_17, (0.0f), /*hidden argument*/NULL);
		// this.transform.position = paddlePosition;
		// this.transform.position = paddlePosition;
		Transform_t2451218446 * L_18 = Component_get_transform_m1207608846(__this, /*hidden argument*/NULL);
		Vector3_t174917947  L_19 = V_6;
		// this.transform.position = paddlePosition;
		NullCheck(L_18);
		Transform_set_position_m392221443(L_18, L_19, /*hidden argument*/NULL);
	}

IL_00ba:
	{
		// }
		return;
	}
}
// System.Void Paddle::MoveWithAccelerometer()
extern "C"  void Paddle_MoveWithAccelerometer_m547288149 (Paddle_t4050334188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Paddle_MoveWithAccelerometer_m547288149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t174917947  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		// this.transform.Translate(Input.acceleration.x,0,0);
		// this.transform.Translate(Input.acceleration.x,0,0);
		Transform_t2451218446 * L_0 = Component_get_transform_m1207608846(__this, /*hidden argument*/NULL);
		// this.transform.Translate(Input.acceleration.x,0,0);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t994511223_il2cpp_TypeInfo_var);
		Vector3_t174917947  L_1 = Input_get_acceleration_m38260823(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		// this.transform.Translate(Input.acceleration.x,0,0);
		NullCheck(L_0);
		Transform_Translate_m3536088981(L_0, L_2, (0.0f), (0.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Paddle::Autoplay()
extern "C"  void Paddle_Autoplay_m3657795759 (Paddle_t4050334188 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Paddle_Autoplay_m3657795759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t174917947  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t174917947  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t174917947  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		// Vector3 paddlePos = new Vector3(0.5f,this.transform.position.y,0f);
		// Vector3 paddlePos = new Vector3(0.5f,this.transform.position.y,0f);
		Transform_t2451218446 * L_0 = Component_get_transform_m1207608846(__this, /*hidden argument*/NULL);
		// Vector3 paddlePos = new Vector3(0.5f,this.transform.position.y,0f);
		NullCheck(L_0);
		Vector3_t174917947  L_1 = Transform_get_position_m1510843579(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_y_2();
		// Vector3 paddlePos = new Vector3(0.5f,this.transform.position.y,0f);
		Vector3__ctor_m3225092((&V_0), (0.5f), L_2, (0.0f), /*hidden argument*/NULL);
		// mousePosInBlocks = Input.mousePosition.x/Screen.width*16;
		// mousePosInBlocks = Input.mousePosition.x/Screen.width*16;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t994511223_il2cpp_TypeInfo_var);
		Vector3_t174917947  L_3 = Input_get_mousePosition_m780518479(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_3;
		float L_4 = (&V_2)->get_x_1();
		// mousePosInBlocks = Input.mousePosition.x/Screen.width*16;
		int32_t L_5 = Screen_get_width_m2828759700(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mousePosInBlocks_2(((float)((float)((float)((float)L_4/(float)(((float)((float)L_5)))))*(float)(16.0f))));
		// paddlePos.x = Mathf.Clamp(mousePosInBlocks,0.5f,15.5f);
		float L_6 = __this->get_mousePosInBlocks_2();
		// paddlePos.x = Mathf.Clamp(mousePosInBlocks,0.5f,15.5f);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t805486531_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Clamp_m2004741560(NULL /*static, unused*/, L_6, (0.5f), (15.5f), /*hidden argument*/NULL);
		(&V_0)->set_x_1(L_7);
		// this.transform.position = paddlePos;
		// this.transform.position = paddlePos;
		Transform_t2451218446 * L_8 = Component_get_transform_m1207608846(__this, /*hidden argument*/NULL);
		Vector3_t174917947  L_9 = V_0;
		// this.transform.position = paddlePos;
		NullCheck(L_8);
		Transform_set_position_m392221443(L_8, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Paddle::.cctor()
extern "C"  void Paddle__cctor_m672128667 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Paddle__cctor_m672128667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static public bool autoplay = true;
		((Paddle_t4050334188_StaticFields*)il2cpp_codegen_static_fields_for(Paddle_t4050334188_il2cpp_TypeInfo_var))->set_autoplay_3((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
