﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void Context_t2346500799_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t2346500799_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t2346500799_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t2346500799_0_0_0;
extern "C" void Escape_t4124895566_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t4124895566_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t4124895566_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t4124895566_0_0_0;
extern "C" void PreviousInfo_t3019508765_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t3019508765_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t3019508765_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t3019508765_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t3995437672();
extern const RuntimeType AppDomainInitializer_t3995437672_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t996062500();
extern const RuntimeType Swapper_t996062500_0_0_0;
extern "C" void DictionaryEntry_t2929293879_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t2929293879_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t2929293879_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t2929293879_0_0_0;
extern "C" void Slot_t334802244_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t334802244_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t334802244_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t334802244_0_0_0;
extern "C" void Slot_t3383151216_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t3383151216_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t3383151216_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t3383151216_0_0_0;
extern "C" void Enum_t256357748_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t256357748_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t256357748_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t256357748_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t4200844067();
extern const RuntimeType ReadDelegate_t4200844067_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t3186463108();
extern const RuntimeType WriteDelegate_t3186463108_0_0_0;
extern "C" void MonoIOStat_t3838903243_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t3838903243_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t3838903243_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t3838903243_0_0_0;
extern "C" void MonoEnumInfo_t3954751260_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t3954751260_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t3954751260_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t3954751260_0_0_0;
extern "C" void CustomAttributeNamedArgument_t2815983049_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t2815983049_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t2815983049_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t2815983049_0_0_0;
extern "C" void CustomAttributeTypedArgument_t722066768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t722066768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t722066768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t722066768_0_0_0;
extern "C" void ILTokenInfo_t1872913708_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t1872913708_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t1872913708_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t1872913708_0_0_0;
extern "C" void MonoEventInfo_t2188823178_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t2188823178_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t2188823178_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t2188823178_0_0_0;
extern "C" void MonoMethodInfo_t568995672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t568995672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t568995672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t568995672_0_0_0;
extern "C" void MonoPropertyInfo_t3238641924_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t3238641924_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t3238641924_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t3238641924_0_0_0;
extern "C" void ParameterModifier_t2173190739_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t2173190739_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t2173190739_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t2173190739_0_0_0;
extern "C" void ResourceCacheItem_t3158443302_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t3158443302_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t3158443302_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t3158443302_0_0_0;
extern "C" void ResourceInfo_t3425727170_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t3425727170_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t3425727170_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t3425727170_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t2959902747();
extern const RuntimeType CrossContextDelegate_t2959902747_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t1106384809();
extern const RuntimeType CallbackHandler_t1106384809_0_0_0;
extern "C" void SerializationEntry_t1792512110_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t1792512110_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t1792512110_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t1792512110_0_0_0;
extern "C" void StreamingContext_t34689150_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t34689150_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t34689150_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t34689150_0_0_0;
extern "C" void DSAParameters_t772912804_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t772912804_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t772912804_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t772912804_0_0_0;
extern "C" void RSAParameters_t2455568150_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t2455568150_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t2455568150_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t2455568150_0_0_0;
extern "C" void SecurityFrame_t3021208141_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t3021208141_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t3021208141_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t3021208141_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t435203469();
extern const RuntimeType ThreadStart_t435203469_0_0_0;
extern "C" void ValueType_t380474154_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t380474154_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t380474154_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t380474154_0_0_0;
extern "C" void X509ChainStatus_t703314169_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t703314169_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t703314169_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t703314169_0_0_0;
extern "C" void IntStack_t3738399117_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t3738399117_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t3738399117_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t3738399117_0_0_0;
extern "C" void Interval_t3607969178_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t3607969178_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t3607969178_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t3607969178_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t762442512();
extern const RuntimeType CostDelegate_t762442512_0_0_0;
extern "C" void UriScheme_t2057891215_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t2057891215_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t2057891215_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t2057891215_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t2570934392();
extern const RuntimeType Action_t2570934392_0_0_0;
extern "C" void AnimationCurve_t3501457430_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3501457430_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3501457430_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t3501457430_0_0_0;
extern "C" void AnimationEvent_t1008067075_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t1008067075_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t1008067075_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t1008067075_0_0_0;
extern "C" void AnimatorTransitionInfo_t4265592529_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t4265592529_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t4265592529_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t4265592529_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t4245252394();
extern const RuntimeType LogCallback_t4245252394_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t1460030936();
extern const RuntimeType LowMemoryCallback_t1460030936_0_0_0;
extern "C" void AssetBundleRequest_t2499071767_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t2499071767_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t2499071767_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t2499071767_0_0_0;
extern "C" void AsyncOperation_t1519908885_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t1519908885_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t1519908885_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t1519908885_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t1756185772();
extern const RuntimeType PCMReaderCallback_t1756185772_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t3088484030();
extern const RuntimeType PCMSetPositionCallback_t3088484030_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1899955275();
extern const RuntimeType AudioConfigurationChangeHandler_t1899955275_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t4054855609();
extern const RuntimeType WillRenderCanvases_t4054855609_0_0_0;
extern "C" void Collision2D_t793992104_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision2D_t793992104_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision2D_t793992104_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision2D_t793992104_0_0_0;
extern "C" void ContactFilter2D_t2902820850_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t2902820850_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t2902820850_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ContactFilter2D_t2902820850_0_0_0;
extern "C" void Coroutine_t2786737299_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t2786737299_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t2786737299_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t2786737299_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t1355779179();
extern const RuntimeType CSSMeasureFunc_t1355779179_0_0_0;
extern "C" void CullingGroup_t2948492783_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t2948492783_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t2948492783_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t2948492783_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t246193582();
extern const RuntimeType StateChanged_t246193582_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t2695213547();
extern const RuntimeType DisplaysUpdatedDelegate_t2695213547_0_0_0;
extern "C" void Event_t749512867_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t749512867_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t749512867_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t749512867_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t1010957147();
extern const RuntimeType UnityAction_t1010957147_0_0_0;
extern "C" void FailedToLoadScriptObject_t2196350297_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t2196350297_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t2196350297_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t2196350297_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t4145331972();
extern const RuntimeType FontTextureRebuildCallback_t4145331972_0_0_0;
extern "C" void Gradient_t1025661461_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t1025661461_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t1025661461_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t1025661461_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t4140737879();
extern const RuntimeType WindowFunction_t4140737879_0_0_0;
extern "C" void GUIContent_t1912066559_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t1912066559_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t1912066559_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t1912066559_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3347500958();
extern const RuntimeType SkinChangedDelegate_t3347500958_0_0_0;
extern "C" void GUIStyle_t236231435_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t236231435_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t236231435_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t236231435_0_0_0;
extern "C" void GUIStyleState_t864127664_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t864127664_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t864127664_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t864127664_0_0_0;
extern "C" void HostData_t2993975190_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HostData_t2993975190_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HostData_t2993975190_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HostData_t2993975190_0_0_0;
extern "C" void HumanBone_t4186201749_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t4186201749_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t4186201749_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t4186201749_0_0_0;
extern "C" void Object_t2399819029_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t2399819029_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t2399819029_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t2399819029_0_0_0;
extern "C" void PlayableBinding_t198519643_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t198519643_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t198519643_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t198519643_0_0_0;
extern "C" void RaycastHit_t3326614521_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t3326614521_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t3326614521_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t3326614521_0_0_0;
extern "C" void RaycastHit2D_t2595119295_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t2595119295_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t2595119295_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t2595119295_0_0_0;
extern "C" void RectOffset_t1388178548_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t1388178548_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t1388178548_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t1388178548_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t3664142111();
extern const RuntimeType UpdatedEventHandler_t3664142111_0_0_0;
extern "C" void ResourceRequest_t3558272522_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t3558272522_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t3558272522_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t3558272522_0_0_0;
extern "C" void ScriptableObject_t1699968602_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t1699968602_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t1699968602_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t1699968602_0_0_0;
extern "C" void HitInfo_t808200858_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t808200858_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t808200858_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t808200858_0_0_0;
extern "C" void SkeletonBone_t1640054518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t1640054518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t1640054518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t1640054518_0_0_0;
extern "C" void GcAchievementData_t3124305070_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t3124305070_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t3124305070_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t3124305070_0_0_0;
extern "C" void GcAchievementDescriptionData_t2362401440_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t2362401440_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t2362401440_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t2362401440_0_0_0;
extern "C" void GcLeaderboard_t436677311_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t436677311_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t436677311_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t436677311_0_0_0;
extern "C" void GcScoreData_t104171333_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t104171333_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t104171333_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t104171333_0_0_0;
extern "C" void GcUserProfileData_t783094146_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t783094146_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t783094146_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t783094146_0_0_0;
extern "C" void TextGenerationSettings_t4026143269_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t4026143269_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t4026143269_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t4026143269_0_0_0;
extern "C" void TextGenerator_t4192863258_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t4192863258_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t4192863258_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t4192863258_0_0_0;
extern "C" void TrackedReference_t3096623824_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t3096623824_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t3096623824_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t3096623824_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t3729546682();
extern const RuntimeType RequestAtlasCallback_t3729546682_0_0_0;
extern "C" void WorkRequest_t1285289635_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t1285289635_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t1285289635_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t1285289635_0_0_0;
extern "C" void WaitForSeconds_t2111244568_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t2111244568_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t2111244568_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t2111244568_0_0_0;
extern "C" void YieldInstruction_t1821362878_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t1821362878_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t1821362878_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t1821362878_0_0_0;
extern "C" void RaycastResult_t791495392_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t791495392_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t791495392_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t791495392_0_0_0;
extern "C" void ColorTween_t3542451946_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t3542451946_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t3542451946_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t3542451946_0_0_0;
extern "C" void FloatTween_t820617968_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t820617968_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t820617968_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t820617968_0_0_0;
extern "C" void Resources_t2401827842_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t2401827842_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t2401827842_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t2401827842_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t3553388536();
extern const RuntimeType OnValidateInput_t3553388536_0_0_0;
extern "C" void Navigation_t643916199_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t643916199_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t643916199_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t643916199_0_0_0;
extern "C" void SpriteState_t582609410_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t582609410_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t582609410_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t582609410_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[97] = 
{
	{ NULL, Context_t2346500799_marshal_pinvoke, Context_t2346500799_marshal_pinvoke_back, Context_t2346500799_marshal_pinvoke_cleanup, NULL, NULL, &Context_t2346500799_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t4124895566_marshal_pinvoke, Escape_t4124895566_marshal_pinvoke_back, Escape_t4124895566_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t4124895566_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t3019508765_marshal_pinvoke, PreviousInfo_t3019508765_marshal_pinvoke_back, PreviousInfo_t3019508765_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t3019508765_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t3995437672, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t3995437672_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t996062500, NULL, NULL, NULL, NULL, NULL, &Swapper_t996062500_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t2929293879_marshal_pinvoke, DictionaryEntry_t2929293879_marshal_pinvoke_back, DictionaryEntry_t2929293879_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t2929293879_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t334802244_marshal_pinvoke, Slot_t334802244_marshal_pinvoke_back, Slot_t334802244_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t334802244_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t3383151216_marshal_pinvoke, Slot_t3383151216_marshal_pinvoke_back, Slot_t3383151216_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t3383151216_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t256357748_marshal_pinvoke, Enum_t256357748_marshal_pinvoke_back, Enum_t256357748_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t256357748_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t4200844067, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t4200844067_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t3186463108, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t3186463108_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t3838903243_marshal_pinvoke, MonoIOStat_t3838903243_marshal_pinvoke_back, MonoIOStat_t3838903243_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t3838903243_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t3954751260_marshal_pinvoke, MonoEnumInfo_t3954751260_marshal_pinvoke_back, MonoEnumInfo_t3954751260_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t3954751260_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t2815983049_marshal_pinvoke, CustomAttributeNamedArgument_t2815983049_marshal_pinvoke_back, CustomAttributeNamedArgument_t2815983049_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t2815983049_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t722066768_marshal_pinvoke, CustomAttributeTypedArgument_t722066768_marshal_pinvoke_back, CustomAttributeTypedArgument_t722066768_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t722066768_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t1872913708_marshal_pinvoke, ILTokenInfo_t1872913708_marshal_pinvoke_back, ILTokenInfo_t1872913708_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t1872913708_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoEventInfo_t2188823178_marshal_pinvoke, MonoEventInfo_t2188823178_marshal_pinvoke_back, MonoEventInfo_t2188823178_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t2188823178_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t568995672_marshal_pinvoke, MonoMethodInfo_t568995672_marshal_pinvoke_back, MonoMethodInfo_t568995672_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t568995672_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t3238641924_marshal_pinvoke, MonoPropertyInfo_t3238641924_marshal_pinvoke_back, MonoPropertyInfo_t3238641924_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t3238641924_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t2173190739_marshal_pinvoke, ParameterModifier_t2173190739_marshal_pinvoke_back, ParameterModifier_t2173190739_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t2173190739_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t3158443302_marshal_pinvoke, ResourceCacheItem_t3158443302_marshal_pinvoke_back, ResourceCacheItem_t3158443302_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t3158443302_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t3425727170_marshal_pinvoke, ResourceInfo_t3425727170_marshal_pinvoke_back, ResourceInfo_t3425727170_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t3425727170_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t2959902747, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t2959902747_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t1106384809, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t1106384809_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t1792512110_marshal_pinvoke, SerializationEntry_t1792512110_marshal_pinvoke_back, SerializationEntry_t1792512110_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t1792512110_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t34689150_marshal_pinvoke, StreamingContext_t34689150_marshal_pinvoke_back, StreamingContext_t34689150_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t34689150_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t772912804_marshal_pinvoke, DSAParameters_t772912804_marshal_pinvoke_back, DSAParameters_t772912804_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t772912804_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t2455568150_marshal_pinvoke, RSAParameters_t2455568150_marshal_pinvoke_back, RSAParameters_t2455568150_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t2455568150_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t3021208141_marshal_pinvoke, SecurityFrame_t3021208141_marshal_pinvoke_back, SecurityFrame_t3021208141_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t3021208141_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t435203469, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t435203469_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t380474154_marshal_pinvoke, ValueType_t380474154_marshal_pinvoke_back, ValueType_t380474154_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t380474154_0_0_0 } /* System.ValueType */,
	{ NULL, X509ChainStatus_t703314169_marshal_pinvoke, X509ChainStatus_t703314169_marshal_pinvoke_back, X509ChainStatus_t703314169_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t703314169_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t3738399117_marshal_pinvoke, IntStack_t3738399117_marshal_pinvoke_back, IntStack_t3738399117_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t3738399117_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t3607969178_marshal_pinvoke, Interval_t3607969178_marshal_pinvoke_back, Interval_t3607969178_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t3607969178_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t762442512, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t762442512_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t2057891215_marshal_pinvoke, UriScheme_t2057891215_marshal_pinvoke_back, UriScheme_t2057891215_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t2057891215_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t2570934392, NULL, NULL, NULL, NULL, NULL, &Action_t2570934392_0_0_0 } /* System.Action */,
	{ NULL, AnimationCurve_t3501457430_marshal_pinvoke, AnimationCurve_t3501457430_marshal_pinvoke_back, AnimationCurve_t3501457430_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3501457430_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ NULL, AnimationEvent_t1008067075_marshal_pinvoke, AnimationEvent_t1008067075_marshal_pinvoke_back, AnimationEvent_t1008067075_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t1008067075_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t4265592529_marshal_pinvoke, AnimatorTransitionInfo_t4265592529_marshal_pinvoke_back, AnimatorTransitionInfo_t4265592529_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t4265592529_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ DelegatePInvokeWrapper_LogCallback_t4245252394, NULL, NULL, NULL, NULL, NULL, &LogCallback_t4245252394_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t1460030936, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t1460030936_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleRequest_t2499071767_marshal_pinvoke, AssetBundleRequest_t2499071767_marshal_pinvoke_back, AssetBundleRequest_t2499071767_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t2499071767_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t1519908885_marshal_pinvoke, AsyncOperation_t1519908885_marshal_pinvoke_back, AsyncOperation_t1519908885_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t1519908885_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t1756185772, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t1756185772_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t3088484030, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t3088484030_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t1899955275, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t1899955275_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t4054855609, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t4054855609_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ NULL, Collision2D_t793992104_marshal_pinvoke, Collision2D_t793992104_marshal_pinvoke_back, Collision2D_t793992104_marshal_pinvoke_cleanup, NULL, NULL, &Collision2D_t793992104_0_0_0 } /* UnityEngine.Collision2D */,
	{ NULL, ContactFilter2D_t2902820850_marshal_pinvoke, ContactFilter2D_t2902820850_marshal_pinvoke_back, ContactFilter2D_t2902820850_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t2902820850_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, Coroutine_t2786737299_marshal_pinvoke, Coroutine_t2786737299_marshal_pinvoke_back, Coroutine_t2786737299_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t2786737299_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t1355779179, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t1355779179_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t2948492783_marshal_pinvoke, CullingGroup_t2948492783_marshal_pinvoke_back, CullingGroup_t2948492783_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t2948492783_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t246193582, NULL, NULL, NULL, NULL, NULL, &StateChanged_t246193582_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t2695213547, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t2695213547_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ NULL, Event_t749512867_marshal_pinvoke, Event_t749512867_marshal_pinvoke_back, Event_t749512867_marshal_pinvoke_cleanup, NULL, NULL, &Event_t749512867_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_UnityAction_t1010957147, NULL, NULL, NULL, NULL, NULL, &UnityAction_t1010957147_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t2196350297_marshal_pinvoke, FailedToLoadScriptObject_t2196350297_marshal_pinvoke_back, FailedToLoadScriptObject_t2196350297_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t2196350297_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t4145331972, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t4145331972_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, Gradient_t1025661461_marshal_pinvoke, Gradient_t1025661461_marshal_pinvoke_back, Gradient_t1025661461_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t1025661461_0_0_0 } /* UnityEngine.Gradient */,
	{ DelegatePInvokeWrapper_WindowFunction_t4140737879, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t4140737879_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t1912066559_marshal_pinvoke, GUIContent_t1912066559_marshal_pinvoke_back, GUIContent_t1912066559_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t1912066559_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t3347500958, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t3347500958_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t236231435_marshal_pinvoke, GUIStyle_t236231435_marshal_pinvoke_back, GUIStyle_t236231435_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t236231435_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t864127664_marshal_pinvoke, GUIStyleState_t864127664_marshal_pinvoke_back, GUIStyleState_t864127664_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t864127664_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, HostData_t2993975190_marshal_pinvoke, HostData_t2993975190_marshal_pinvoke_back, HostData_t2993975190_marshal_pinvoke_cleanup, NULL, NULL, &HostData_t2993975190_0_0_0 } /* UnityEngine.HostData */,
	{ NULL, HumanBone_t4186201749_marshal_pinvoke, HumanBone_t4186201749_marshal_pinvoke_back, HumanBone_t4186201749_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t4186201749_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, Object_t2399819029_marshal_pinvoke, Object_t2399819029_marshal_pinvoke_back, Object_t2399819029_marshal_pinvoke_cleanup, NULL, NULL, &Object_t2399819029_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t198519643_marshal_pinvoke, PlayableBinding_t198519643_marshal_pinvoke_back, PlayableBinding_t198519643_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t198519643_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RaycastHit_t3326614521_marshal_pinvoke, RaycastHit_t3326614521_marshal_pinvoke_back, RaycastHit_t3326614521_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t3326614521_0_0_0 } /* UnityEngine.RaycastHit */,
	{ NULL, RaycastHit2D_t2595119295_marshal_pinvoke, RaycastHit2D_t2595119295_marshal_pinvoke_back, RaycastHit2D_t2595119295_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t2595119295_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, RectOffset_t1388178548_marshal_pinvoke, RectOffset_t1388178548_marshal_pinvoke_back, RectOffset_t1388178548_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t1388178548_0_0_0 } /* UnityEngine.RectOffset */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t3664142111, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t3664142111_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, ResourceRequest_t3558272522_marshal_pinvoke, ResourceRequest_t3558272522_marshal_pinvoke_back, ResourceRequest_t3558272522_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t3558272522_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t1699968602_marshal_pinvoke, ScriptableObject_t1699968602_marshal_pinvoke_back, ScriptableObject_t1699968602_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t1699968602_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t808200858_marshal_pinvoke, HitInfo_t808200858_marshal_pinvoke_back, HitInfo_t808200858_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t808200858_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, SkeletonBone_t1640054518_marshal_pinvoke, SkeletonBone_t1640054518_marshal_pinvoke_back, SkeletonBone_t1640054518_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t1640054518_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t3124305070_marshal_pinvoke, GcAchievementData_t3124305070_marshal_pinvoke_back, GcAchievementData_t3124305070_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t3124305070_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t2362401440_marshal_pinvoke, GcAchievementDescriptionData_t2362401440_marshal_pinvoke_back, GcAchievementDescriptionData_t2362401440_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t2362401440_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t436677311_marshal_pinvoke, GcLeaderboard_t436677311_marshal_pinvoke_back, GcLeaderboard_t436677311_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t436677311_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t104171333_marshal_pinvoke, GcScoreData_t104171333_marshal_pinvoke_back, GcScoreData_t104171333_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t104171333_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t783094146_marshal_pinvoke, GcUserProfileData_t783094146_marshal_pinvoke_back, GcUserProfileData_t783094146_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t783094146_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, TextGenerationSettings_t4026143269_marshal_pinvoke, TextGenerationSettings_t4026143269_marshal_pinvoke_back, TextGenerationSettings_t4026143269_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t4026143269_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t4192863258_marshal_pinvoke, TextGenerator_t4192863258_marshal_pinvoke_back, TextGenerator_t4192863258_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t4192863258_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, TrackedReference_t3096623824_marshal_pinvoke, TrackedReference_t3096623824_marshal_pinvoke_back, TrackedReference_t3096623824_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t3096623824_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t3729546682, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t3729546682_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t1285289635_marshal_pinvoke, WorkRequest_t1285289635_marshal_pinvoke_back, WorkRequest_t1285289635_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t1285289635_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t2111244568_marshal_pinvoke, WaitForSeconds_t2111244568_marshal_pinvoke_back, WaitForSeconds_t2111244568_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t2111244568_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t1821362878_marshal_pinvoke, YieldInstruction_t1821362878_marshal_pinvoke_back, YieldInstruction_t1821362878_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t1821362878_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ NULL, RaycastResult_t791495392_marshal_pinvoke, RaycastResult_t791495392_marshal_pinvoke_back, RaycastResult_t791495392_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t791495392_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t3542451946_marshal_pinvoke, ColorTween_t3542451946_marshal_pinvoke_back, ColorTween_t3542451946_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t3542451946_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t820617968_marshal_pinvoke, FloatTween_t820617968_marshal_pinvoke_back, FloatTween_t820617968_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t820617968_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t2401827842_marshal_pinvoke, Resources_t2401827842_marshal_pinvoke_back, Resources_t2401827842_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t2401827842_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t3553388536, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t3553388536_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t643916199_marshal_pinvoke, Navigation_t643916199_marshal_pinvoke_back, Navigation_t643916199_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t643916199_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t582609410_marshal_pinvoke, SpriteState_t582609410_marshal_pinvoke_back, SpriteState_t582609410_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t582609410_0_0_0 } /* UnityEngine.UI.SpriteState */,
	NULL,
};
